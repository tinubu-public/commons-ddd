package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.ValidatingObject.validatingObject;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isTrue;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.startsWith;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.UrnRules.isUrn;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.UrnRules.nidIsEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.UriRules.UrnRules.nss;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static java.util.Objects.isNull;

import java.net.URI;
import java.net.URISyntaxException;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.ParameterValue;

/**
 * A default {@link SimpleId} implementation.
 * The implementation support both persistence style for ids :
 * <ul>
 *    <li>{@link #AbstractSimpleId(T)} : initialized (requesting next id from ORM)</li>
 *    <li>{@link #AbstractSimpleId()} : uninitialized (after ORM's save). In this case many operations are restricted</li>
 * </ul>
 * <p>
 *
 * @param <T> id raw value type
 *
 * @implNote Implementation is immutable.
 * @see AbstractBiCompositeId for composite ids.
 */
public abstract class AbstractSimpleId<T extends Comparable<? super T>> extends AbstractValue
      implements SimpleId<T> {

   protected T value;
   protected final boolean newObject;

   /**
    * Builds {@link AbstractSimpleId} from specified value.
    *
    * @param value optional raw id value
    * @param newObject informs whether the entity object is a new entity. If value is uninitialized
    *       ({@code null}), newObject must be set to {@code true}
    */
   protected AbstractSimpleId(T value, boolean newObject) {
      this.value = value;
      this.newObject = newObject;
   }

   /**
    * Builds {@link AbstractSimpleId} from specified value for object reconstitution.
    * {@code newObject} is defaulted to {@code false}.
    *
    * @param value optional id raw value
    */
   protected AbstractSimpleId(T value) {
      this(value, false);
   }

   /**
    * Builds {@link AbstractSimpleId} with uninitialized value.
    * {@code newObject} is defaulted to {@code true}.
    */
   protected AbstractSimpleId() {
      this(null, true);
   }

   /**
    * Overridable domain fields definitions.
    *
    * @return domain fields
    */
   @Override
   protected Fields<? extends SimpleId<T>> defineDomainFields() {
      return Fields
            .<AbstractSimpleId<T>>builder()
            .field("value", v -> v.value)
            .field("newObject",
                   v -> v.newObject,
                   isTrue("'%1$s' must be true if value is uninitialized",
                          validatingObjectName()).ifIsSatisfied(() -> isNull(value)))
            .build();
   }

   @Override
   public boolean newObject() {
      return newObject;
   }

   public T value() {
      return value;
   }

   /**
    * Returns new id instance with same fields and initialized value.
    *
    * @param value id value to initialize
    *
    * @return new id with initialized value
    */
   public abstract SimpleId<T> withValue(T value);

   /**
    * Compares {@link Id} by value.
    * Only comparison of exactly the same id classes is supported.
    * Only comparison of initialized ids is supported.
    *
    * @param value the id to be compared.
    */
   @Override
   @SuppressWarnings("unchecked")
   public int compareTo(Id value) {
      if (!(this.getClass() == value.getClass())) {
         throw new IllegalStateException(String.format("'%s' can't be compared with '%s'",
                                                       this.getClass().getName(),
                                                       value.getClass().getName()));
      }
      if (this.value == null || ((AbstractSimpleId<?>) value).value == null) {
         throw new IllegalStateException("Can't compare uninitialized ids");
      }
      return this.value.compareTo(((AbstractSimpleId<T>) value).value);
   }

   /**
    * {@inheritDoc}
    * Always return {@code false} if any id is uninitialized.
    */
   @Override
   public boolean sameValueAs(Value value) {
      if (this.value == null) {
         return false;
      } else {
         return super.sameValueAs(value);
      }
   }

   /**
    * Generates a custom URN using format {@code urn:id:<id-type>:<id-value>}.
    *
    * @param idType Custom URN id type identifier, should be in kebab-case
    *
    * @return value URN representation, as a valid {@link URI}. Can be {@code null} if id value is uninitialized
    */
   protected URI idUrn(String idType) {
      notBlank(idType, "idType");

      if (value() == null) {
         return null;
      }

      try {
         return new URI("urn", "id:" + idType.toLowerCase() + ":" + stringValue(), null);
      } catch (URISyntaxException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Parses id URN with format {@code urn:id:<id-type>:<id-value>}.
    *
    * @param urn id URN
    * @param idType id type to check
    *
    * @return id URN value part
    *
    * @throws InvariantValidationException if URN syntax is invalid
    */
   protected static String idUrnValue(URI urn, String idType) {
      validatingObject(urn, "urn")
            .validate(isValidIdUrnSyntax(idType))
            .and(validatingObject(idType, "idType").validate(isNotBlank()))
            .orThrow();

      return urn.getSchemeSpecificPart().split(":", 3)[2];
   }

   protected static InvariantRule<URI> isValidIdUrnSyntax(String idType) {
      return isUrn().andValue(nidIsEqualTo(ParameterValue.value("id")).andValue(nss(startsWith(ParameterValue.value(
            idType + ":")))));
   }

}
