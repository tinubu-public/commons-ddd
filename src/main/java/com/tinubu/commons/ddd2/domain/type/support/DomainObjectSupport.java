package com.tinubu.commons.ddd2.domain.type.support;

import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Supplier;

import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Fields.Field;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

/**
 * Operations supporting {@link DomainObject}.
 */
public final class DomainObjectSupport {

   /**
    * Public global static flag to change {@link DomainObjectSupport#entityToString(Entity)} behavior.
    * Display only the entity id if set to {@code true}.
    */
   public static boolean ENTITY_TO_STRING_IDENTITY_ONLY = false;

   private static final String ENTITY_DEFAULT_ID_FIELD_NAME = "id";

   private DomainObjectSupport() {
   }

   /**
    * General equality function for {@link Value value}.
    * Specified value and object are equals if one or both are {@code null} or value and object have
    * the {@link Value#sameValueAs(Value)}.
    * <p>
    * If {@link Object} is not a value, the function returns {@code false} without {@link ClassCastException}.
    * <p>
    * You should use this function if you use {@link #valueHashCode(Value)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    * <p>
    * It is not mandatory for {@link Value values} to implement {@link Object#equals(Object)} like this, it's
    * the user choice, but it is a good default.
    *
    * @param value first object of type {@link Value}
    * @param object second object
    * @param <T> value type
    *
    * @return {@code true} if one or both of value and object are {@code null}, or have the same values.
    *
    * @implSpec Technical fields are not taken into account for sameValueAs/equals/hashCode to be
    *       consistent
    */
   @SuppressWarnings("unchecked")
   public static <T extends Value> boolean valueEquals(T value, Object object) {
      if (value == object) return true;
      if (value == null || object == null || value.getClass() != object.getClass()) return false;
      return value.sameValueAs((T) object);
   }

   /**
    * General hashcode function for {@link Value value} using {@link DomainObject#domainFields()}.
    * <p>
    * You should use this function if you use {@link #valueEquals(Value, Object)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    *
    * @param value value to hash
    * @param <T> value type
    *
    * @return value hashcode
    *
    * @implSpec Technical fields are not taken into account for sameValueAs/equals/hashCode to be
    *       consistent
    */
   @SuppressWarnings("unchecked")
   public static <T extends Value> int valueHashCode(T value) {
      notNull(value, "value");

      return Objects.hash(((Fields<DomainObject>) value.domainFields())
                                .values()
                                .stream()
                                .filter(f -> !f.technical())
                                .map(f -> f.value().apply(value))
                                .toArray());
   }

   public static <T extends Value> String valueToString(T value) {
      notNull(value, "value");

      return valueToString(value, Fields.<T>builder().empty());
   }

   @SuppressWarnings("unchecked")
   public static <T extends Value> String valueToString(T value, Fields<T> extraFields) {
      notNull(value, "value");
      notNull(extraFields, "extraFields");

      StringJoiner toStringBuilder = new StringJoiner(",", value.getClass().getSimpleName() + "[", "]");
      ((Fields<DomainObject>) value.domainFields())
            .values()
            .forEach(field -> toStringBuilder.add(field.name() + "=" + formatToStringValue(field
                                                                                                 .displayValue()
                                                                                                 .apply(value))));
      extraFields.values().forEach(field -> toStringBuilder.add(field.name() + "=" + formatToStringValue(field
                                                                                                 .displayValue()
                                                                                                 .apply(value))));

      return toStringBuilder.toString();
   }

   /**
    * General equality function for {@link Entity entity}.
    * Specified entity and object are equals if one or both are {@code null} or entity and object have
    * the {@link Entity#sameIdentityAs(Entity)}.
    * <p>
    * You should use this function if you use {@link #entityHashCode(Entity)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    * <p>
    * If {@link Object} is not a value, the function returns {@code false} without {@link ClassCastException}.
    * <p>
    * It is not mandatory for {@link Entity entities} to implement {@link Object#equals(Object)} like this,
    * alternatively the user will prefer to compare all fields with a classic implementation.
    *
    * @param entity first object of type {@link Entity}
    * @param object second object
    * @param <T> entity type
    *
    * @return {@code true} if one or both of entity and object are {@code null}, or have the same identities.
    */
   @SuppressWarnings("unchecked")
   public static <T extends Entity<ID>, ID extends Id> boolean entityEquals(T entity, Object object) {
      if (entity == object) return true;
      if (entity == null || object == null || entity.getClass() != object.getClass()) return false;
      return entity.sameIdentityAs((T) object);
   }

   /**
    * General hashcode function for {@link Entity entity}.
    * <p>
    * You should use this function if you use {@link #entityEquals(Entity, Object)} as {@link
    * Object#equals(Object)} and {@link Object#hashCode()} must be consistent.
    * <p>
    * It is not mandatory for {@link Entity entities} to implement {@link Object#hashCode()} like this,
    * alternatively the user will prefer to hash all fields with a classic implementation.
    *
    * @param <T> entity type
    * @param entity entity to hash
    *
    * @return entity hashcode
    */
   public static <T extends Entity<?>> int entityHashCode(T entity) {
      notNull(entity, "entity");

      return Objects.hash(entity.id());
   }

   @SuppressWarnings("unchecked")
   public static <T extends Entity<?>> String entityToString(T entity,
                                                             boolean identityOnly,
                                                             Fields<T> extraFields) {
      notNull(entity, "entity");
      notNull(extraFields, "extraFields");

      StringJoiner toStringBuilder = new StringJoiner(",", entity.getClass().getSimpleName() + "[", "]");

      if (identityOnly || entity.domainFields().isEmpty()) {
         toStringBuilder.add(ENTITY_DEFAULT_ID_FIELD_NAME + "=" + entity.id());
      } else {
         for (Field<DomainObject, ?> field : ((Fields<DomainObject>) entity.domainFields()).values()) {
            toStringBuilder.add(field.name() + "=" + formatToStringValue(field.displayValue().apply(entity)));
         }
      }
      for (Field<T, ?> field : extraFields.values()) {
         toStringBuilder.add(field.name() + "=" + formatToStringValue(field.displayValue().apply(entity)));
      }
      return toStringBuilder.toString();
   }

   public static <T extends Entity<?>> String entityToString(T entity, boolean identityOnly) {
      notNull(entity, "entity");

      return entityToString(entity, identityOnly, Fields.<T>builder().empty());
   }

   public static <T extends Entity<?>> String entityToString(T entity) {
      notNull(entity, "entity");

      return entityToString(entity, ENTITY_TO_STRING_IDENTITY_ONLY);
   }

   public static <T extends Entity<?>> String entityToString(T entity, Fields<T> extraFields) {
      notNull(entity, "entity");
      notNull(extraFields, "extraFields");

      return entityToString(entity, ENTITY_TO_STRING_IDENTITY_ONLY, extraFields);
   }

   private static String formatToStringValue(Object value) {
      if (value == null) {
         return "<null>";
      } else {
         return value.toString();
      }
   }

   /**
    * Returns {@code true} if this value is the same that any listed {@code values}.
    *
    * @param value value to compare
    * @param inValues list of values to compare value to
    * @param <T> values type
    *
    * @return {@code true} if this value is the same that any listed values
    */
   @SafeVarargs
   public static <T extends Value> boolean valueIn(T value, T... inValues) {
      notNull(value, "value");
      noNullElements(inValues, "inValues");

      return stream(inValues).anyMatch(value::sameValueAs);
   }

   /**
    * Returns {@code true} if this value is the same that any listed {@code values}.
    *
    * @param value value to compare
    * @param inValues list of values to compare value to
    * @param <T> values type
    *
    * @return {@code true} if this value is the same that any listed values
    */
   public static <T extends Value> boolean valueIn(T value, Collection<T> inValues) {
      notNull(value, "value");
      noNullElements(inValues, "inValues");

      return stream(inValues).anyMatch(value::sameValueAs);
   }

   /**
    * Returns {@code true} if this entity has the same identity that any listed {@code inEntities}.
    *
    * @param entity entity to compare
    * @param inEntities list of entities to compare entity to
    * @param <T> entities type
    * @param <ID> Identity type
    *
    * @return {@code true} if this entity has the same identity that any listed identities
    */
   @SafeVarargs
   public static <T extends Entity<ID>, ID extends Id> boolean identityIn(T entity, T... inEntities) {
      notNull(entity, "entity");
      noNullElements(inEntities, "inEntities");

      return stream(inEntities).anyMatch(entity::sameIdentityAs);
   }

   /**
    * Returns {@code true} if this entity has the same identity that any listed {@code inEntities}.
    *
    * @param entity entity to compare
    * @param inEntities list of entities to compare entity to
    * @param <T> entities type
    * @param <ID> Identity type
    *
    * @return {@code true} if this entity has the same identity that any listed identities
    */
   public static <T extends Entity<ID>, ID extends Id> boolean identityIn(T entity,
                                                                          Collection<T> inEntities) {
      notNull(entity, "entity");
      noNullElements(inEntities, "inEntities");

      return stream(inEntities).anyMatch(entity::sameIdentityAs);
   }

   /**
    * Checks domain object invariants and throw exception in case of failure.
    *
    * @param domainObject domainObject to check
    * @param groups validation groups
    * @param <T> domain object type
    *
    * @return domain object
    *
    * @throws InvariantValidationException if invariant validation fails
    */
   public static <T extends DomainObject> T checkInvariants(T domainObject, List<String> groups) {
      notNull(domainObject, "domainObject");
      noNullElements(groups, "groups");

      domainObject.validateInvariants(groups).orThrow();

      return domainObject;
   }

   /**
    * Checks domain object invariants and throw exception in case of failure.
    *
    * @param domainObject domainObject to check
    * @param groups optional validation groups
    * @param <T> domain object type
    *
    * @return domain object
    *
    * @throws InvariantValidationException if invariant validation fails
    */
   public static <T extends DomainObject> T checkInvariants(T domainObject, String... groups) {
      notNull(domainObject, "domainObject");

      domainObject.validateInvariants(groups).orThrow();

      return domainObject;
   }

   /**
    * Builds a domain object applying all post operations :
    * <ul>
    *    <li>{@link DomainObject#validateInvariants(String...)}</li>
    *    <li>{@link DomainObject#postConstruct()}</li>
    *    <li>{@link DomainObject#postValidate()}</li>
    * </ul>
    *
    * @param domainObject raw domain object instance builder
    * @param groups optional validation groups
    * @param <T> domain object type
    *
    * @return fully built domain object
    */
   public static <T extends DomainObject> T build(Supplier<T> domainObject, String... groups) {
      return checkInvariants(domainObject.get().postConstruct(), groups).postValidate();
   }

}
