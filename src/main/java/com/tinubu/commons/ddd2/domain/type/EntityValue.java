package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import com.tinubu.commons.lang.beans.Getter;

/**
 * Entity value holder with associated entity identifier. This class is useful to manipulate one of entity's
 * value without losing the associated entity id, for example when manipulating collections.
 * This class can be subclassed for a given entity or used as-is.
 */
public class EntityValue<ID extends Id, T extends Value> extends AbstractValue {
   private final ID id;
   private final T value;

   protected EntityValue(ID id, T value) {
      this.id = id;
      this.value = value;
   }

   public static <ID extends Id, T extends Value> EntityValue<ID, T> newInstance(ID id, T value) {
      return checkInvariants(new EntityValue<>(id, value));
   }

   protected Fields<? extends EntityValue<ID, T>> defineDomainFields() {
      return Fields
            .<EntityValue<ID, T>>builder()
            .field("id", v -> v.id, isNotNull())
            .field("value", v -> v.value)
            .build();
   }

   /**
    * Entity identifier associated to this value.
    *
    * @return entity identifier associated to this value
    */
   @Getter
   public ID id() {
      return id;
   }

   /**
    * Hold value. Value can be {@code null}.
    *
    * @return hold value or {@code null}
    */
   @Getter
   public T value() {
      return value;
   }
}
