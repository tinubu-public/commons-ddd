package com.tinubu.commons.ddd2.domain.type;

/**
 * A DDD event object.
 * The event identity may be explicit (i.e. is an entity), or derived from data (i.e. is a value) : you should
 * extend your subclass with {@link Entity} or {@link Value} accordingly.
 *
 * @deprecated Use {@link com.tinubu.commons.ddd2.domain.event.DomainEvent} instead
 */
@Deprecated
public interface DomainEvent {

   /**
    * Events comparison operation.
    * Compared events classes must be equals.
    *
    * @param event The domain event to compare to.
    *
    * @return {@code true} if the given domain event and this event are regarded as being the same event.
    *
    * @implSpec Default implementation compares {@link Value} event classes using
    *       {@link Value#sameValueAs(Value)} and {@link Entity} event classes using
    *       {@link Entity#sameIdentityAs(Entity)}.
    */
   @SuppressWarnings({ "unchecked", "rawtypes" })
   default boolean sameEventAs(DomainEvent event) {
      if (event != null && this.getClass().equals(event.getClass())) {
         if (this instanceof Value) {
            return event instanceof Value && ((Value) this).sameValueAs((Value) event);
         }

         if (this instanceof Entity) {
            return event instanceof Entity && ((Entity) this).sameIdentityAs((Entity) event);
         }

         return true;
      }

      return false;
   }
}
