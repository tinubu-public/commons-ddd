/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

/**
 * A DDD default {@link Version} implementation.
 *
 * @implNote Implementation is immutable.
 */
public abstract class AbstractVersion<T extends Comparable<? super T>> extends AbstractValue
      implements Version<T> {
   protected T value;

   /**
    * Constructor for explicit version value.
    *
    * @param value raw version value
    */
   public AbstractVersion(T value) {
      this.value = value;
   }

   /**
    * Overridable domain fields definitions.
    *
    * @return domain fields
    */
   @Override
   protected Fields<? extends Version<T>> defineDomainFields() {
      return Fields
            .<AbstractVersion<T>>builder()
            .field("value", v -> v.value, isNotNull())
            .build();
   }

   public T value() {
      return value;
   }

   @Override
   public int compareTo(Version<T> version) {
      if (!(this.getClass() == version.getClass())) {
         throw new IllegalStateException(String.format("'%s' can't be compared with '%s'",
                                                       this.getClass().getName(),
                                                       version.getClass().getName()));
      }

      return value.compareTo(((AbstractVersion<T>) version).value);
   }

}
