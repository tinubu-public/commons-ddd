package com.tinubu.commons.ddd2.domain.specification;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.stream.Stream;

/**
 * Composite specification interface.
 * A composite specification is also a {@link Specification} and can be evaluated.
 * A composite specification maintains the original composition tree, so that it can be {@link #accept
 * visited} without information loss.
 */
public interface CompositeSpecification<T> extends Specification<T> {

   /**
    * Returns a {@link CompositeSpecification} from a {@link Specification}.
    * If specified specification is an instance of {@link CompositeSpecification}, value is simply cast and
    * returned.
    *
    * @param specification specification to transform to composite specification
    *
    * @return composite specification
    */
   static <T> CompositeSpecification<T> of(Specification<T> specification) {
      notNull(specification, "specification");

      if (specification instanceof CompositeSpecification) {
         return (CompositeSpecification<T>) specification;
      } else {
         return specification::satisfiedBy;
      }
   }

   /**
    * Creates a new specification that is the 'and' operation of this specification and another
    * specification.
    *
    * @param specifications Specifications to 'and'.
    *
    * @return A new specification.
    *
    * @apiNote 'and' associativity is used to simplify composition.
    */
   default CompositeSpecification<T> and(final List<? extends Specification<T>> specifications) {
      noNullElements(specifications, "specifications");

      Stream<Specification<T>> associativeSpecifications;

      if (this instanceof AndSpecification) {
         associativeSpecifications = stream(((AndSpecification<T>) this).specifications());
      } else {
         associativeSpecifications = stream(this);
      }

      return new AndSpecification<>(listConcat(associativeSpecifications, stream(specifications)));
   }

   /**
    * Creates a new specification that is the 'and' operation of this specification and another
    * specification.
    *
    * @param specification Specification to 'and'.
    *
    * @return A new specification.
    *
    * @apiNote 'and' associativity is used to simplify composition.
    */
   default CompositeSpecification<T> and(final Specification<T> specification) {
      return and(list(specification));
   }

   /**
    * Creates a new specification that is the 'or' operation of this specification and another
    * specification.
    *
    * @param specifications Specifications to 'or'.
    *
    * @return A new specification.
    *
    * @apiNote 'or' associativity is used to simplify composition.
    */
   default CompositeSpecification<T> or(final List<? extends Specification<T>> specifications) {
      noNullElements(specifications, "specifications");

      Stream<Specification<T>> associativeSpecifications;

      if (this instanceof OrSpecification) {
         associativeSpecifications = stream(((OrSpecification<T>) this).specifications());
      } else {
         associativeSpecifications = stream(this);
      }

      return new OrSpecification<>(listConcat(associativeSpecifications, stream(specifications)));

   }

   /**
    * Creates a new specification that is the 'or' operation of this specification and another
    * specification.
    *
    * @param specification Specification to 'or'.
    *
    * @return A new specification.
    *
    * @apiNote 'or' associativity is used to simplify composition.
    */
   default CompositeSpecification<T> or(final Specification<T> specification) {
      return or(list(specification));
   }

   /**
    * Creates a new specification that is the 'not' operation of this specification.
    *
    * @return A new specification.
    */
   default CompositeSpecification<T> not() {
      return new NotSpecification<>(this);
   }

   /**
    * Determines if an entire composite tree is "pure", it means that all its leafs are instance of the
    * specified class (not necessarily equals).
    *
    * @param specificationClass specification class that must implement all tree leafs
    *
    * @return {@code true} if tree is pure
    */
   default boolean isPure(Class<? extends Specification<T>> specificationClass) {
      notNull(specificationClass, "specificationClass");

      return this.accept(new CompositeSpecificationVisitor<T, Boolean>() {

         @Override
         public Boolean visit(Specification<T> specification) {
            return specificationClass.isAssignableFrom(specification.getClass());
         }

         @Override
         public Boolean visit(NotSpecification<T> specification) {
            return specification.specification().accept(this);
         }

         @Override
         public Boolean visit(AndSpecification<T> specification) {
            return stream(specification.specifications()).reduce(true,
                                                                 (pure, spec) -> pure && spec.accept(this),
                                                                 (a, b) -> a && b);
         }

         @Override
         public Boolean visit(OrSpecification<T> specification) {
            return stream(specification.specifications()).reduce(true,
                                                                 (pure, spec) -> pure && spec.accept(this),
                                                                 (a, b) -> a && b);
         }
      });
   }
}
