package com.tinubu.commons.ddd2.domain.type;

import java.util.List;

import com.tinubu.commons.ddd2.domain.type.Fields.FieldsBuilder;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantResult;
import com.tinubu.commons.ddd2.invariant.InvariantResults;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;
import com.tinubu.commons.ddd2.invariant.Invariants;
import com.tinubu.commons.lang.validation.CheckReturnValue;

public interface DomainObject {

   /**
    * Initializer called after class construction, and before invariant checking.
    *
    * @return this object
    */
   @SuppressWarnings("unchecked")
   default <T extends DomainObject> T postConstruct() {
      return (T) this;
   }

   /**
    * Initializer called after invariant checking.
    *
    * @return this object
    */
   @SuppressWarnings("unchecked")
   default <T extends DomainObject> T postValidate() {
      return (T) this;
   }

   /**
    * Fields declaration for the domain object.
    *
    * @return declared fields
    */
   default Fields<? extends DomainObject> domainFields() {
      return Fields.builder().empty();
   }

   /**
    * Union of domain fields invariants and {@link #domainInvariants()}.
    * Should not be overridden.
    *
    * @return known invariants for domain objects
    */
   @SuppressWarnings("unchecked")
   default Invariants invariants() {
      return ((Fields<DomainObject>) domainFields())
            .invariants(this)
            .withInvariants(domainInvariants())
            .context(validationContext());
   }

   /**
    * Generates a default context for this domain object validation. You can override this method to
    * customize
    * the context.
    * <p>
    * Default context uses the following rules :
    * <ul>
    *    <li>{@link Entity} object : Use {@link Entity#id} or {@code this.toString()} if id is {@code null}</li>
    *    <li>Other objects : use {@code this.toString()}</li>
    * </ul>
    *
    * @return objet context for validation
    *
    * @implNote We store {@code this.toString()} into context, so that, depending on use case,
    *       partially built class can't be accessible through {@link InvariantValidationException}'s result
    *       context.
    */
   default Object validationContext() {
      if (this instanceof Entity) {
         Id id = ((Entity<?>) this).id();

         if (id != null) {
            return id;
         }
      }

      return this.toString();
   }

   /**
    * Overridable invariants to be declared in domain object.
    * Should reference invariants that apply on multiple values, and are evaluated after field invariants.
    * You can also register these invariants using {@link FieldsBuilder#invariants(Invariant[])}.
    *
    * @return domain object invariants, never {@code null}
    */
   default Invariants domainInvariants() {
      return Invariants.empty();
   }

   /**
    * Validates declared invariants.
    * You must operate the returned result, either with {@link InvariantResult#orThrow()},
    * {@link InvariantResult#defer(InvariantResults)} or other, for the validation to be effective.
    *
    * @param groups validation groups
    *
    * @return invariant results if all invariants success
    */
   @CheckReturnValue
   default InvariantResults validateInvariants(List<String> groups) {
      return invariants().validate(groups);
   }

   /**
    * Validates declared invariants.
    * You must operate the returned result, either with {@link InvariantResult#orThrow()},
    * {@link InvariantResult#defer(InvariantResults)} or other, for the validation to be effective.
    *
    * @param groups optional validation groups
    *
    * @return invariant results if all invariants success
    */
   @CheckReturnValue
   default InvariantResults validateInvariants(String... groups) {
      return invariants().validate(groups);
   }

}
