package com.tinubu.commons.ddd2.domain.type;

import java.util.Arrays;
import java.util.Optional;

/**
 * Helper to implement a comparator by values.
 * <p>
 *
 * @implSpec Currently, {@link Optional} and collections of {@link Value} are not overridden to compare
 *       values using {@link Value#sameValueAs(Value)}, instead, we expect {@link Object#equals(Object)} to be
 *       implemented using {@link Value#sameValueAs(Value)}.
 */
public class SameValueBuilder {

   private boolean sameValue;

   /**
    * Defines the current value and the other value to compare.
    * <p>
    * All the required checks are evaluated here :
    * <ul>
    * <li>If the 2 values are enums, they are directly compared for equality here</li>
    * <li>The exact class is matched so that value's sub-types are not considered the same value</li>
    * </ul>
    * <p>
    * Anyway you must check for parameter nullity in the user-space :
    * <pre>
    * {@code @}Override
    * public boolean sameValueAs(MyValue value) {
    *   return value != null &amp;&amp; new SameValueBuilder(this, value)
    *                               .append(this.field, value.field)
    *                               .sameValue();
    * }
    * </pre>
    * <p>
    * Implementation in an inheriting class :
    * <pre>
    * // Overload
    * public boolean sameValueAs(InheritingMyValue value) {
    *   return super.sameValueAs(value) &amp;&amp; new SameValueBuilder(this, value)
    *                                          .append(this.newField, value.newField)
    *                                          .sameValue();
    * }
    * </pre>
    *
    * @param value1 first value
    * @param value2 second value
    */
   public SameValueBuilder(Object value1, Object value2) {
      if (value1 == null || value2 == null) {
         sameValue = value1 == null && value2 == null;
      } else {
         sameValue = value1.getClass() == value2.getClass() && sameEnum(value1, value2);
      }
   }

   /**
    * Returns the enums comparison result or true if values are not enums.
    *
    * @return the enums comparison result or true if values are not enums
    */
   private boolean sameEnum(Object value1, Object value2) {
      if (value1 instanceof Enum && value2 instanceof Enum) {
         return value1 == value2;
      } else {
         return true;
      }
   }

   /**
    * Append for {@link Object} type.
    *
    * @param value1 first value
    * @param value2 second value
    *
    * @return this builder
    *
    * @implNote Support down-casting of objects for collections of heterogeneous types for example
    *       (e.g.: {@link DomainObject#domainFields()}.
    * @implNote Also must treat enums particularly
    */
   @SuppressWarnings({ "rawtypes", "unchecked", "squid:S3776" /* Cognitive complexity */ })
   public SameValueBuilder append(Object value1, Object value2) {
      if (sameValue && value1 != value2) {

         if (value1 instanceof String[] && value2 instanceof String[]) {
            return append((String[]) value1, (String[]) value2);
         } else if (value1 instanceof char[]) {
            return append((char[]) value1, (char[]) value2);
         } else if (value1 instanceof byte[]) {
            return append((byte[]) value1, (byte[]) value2);
         } else if (value1 instanceof short[]) {
            return append((short[]) value1, (short[]) value2);
         } else if (value1 instanceof int[]) {
            return append((int[]) value1, (int[]) value2);
         } else if (value1 instanceof long[]) {
            return append((long[]) value1, (long[]) value2);
         } else if (value1 instanceof float[]) {
            return append((float[]) value1, (float[]) value2);
         } else if (value1 instanceof double[]) {
            return append((double[]) value1, (double[]) value2);
         } else if (value1 instanceof boolean[]) {
            return append((boolean[]) value1, (boolean[]) value2);
         } else if (value1 instanceof Value[]) {
            return append((Value[]) value1, (Value[]) value2);
         } else if (value1 instanceof Entity[]) {
            return append((Entity[]) value1, (Entity[]) value2);
         } else if (value1 instanceof Value) {
            return append((Value) value1, (Value) value2);
         } else if (value1 instanceof Enum) {
            sameValue = false;
         } else if (value1 instanceof Entity) {
            return append((Entity) value1, (Entity) value2);
         } else if (value1 != null && value1.getClass().isArray()) {
            return append((Object[]) value1, (Object[]) value2);
         } else {
            sameValue = (value1 != null && value1.equals(value2));
         }
      }

      return this;
   }

   /**
    * Appends this equality check to builder.
    *
    * @param value1 first operand
    * @param value2 second operand
    * @param <V> value type (unchecked)
    *
    * @return this builder
    *
    * @implNote operands class must be checked because type is unchecked.
    */
   public <V extends Value> SameValueBuilder append(V value1, V value2) {
      if (sameValue && value1 != value2) {
         sameValue = (value1 != null
                      && value2 != null
                      && value2.getClass().isInstance(value1)
                      && value1.sameValueAs(value2));
      }

      return this;
   }

   /**
    * Appends this equality check to builder.
    *
    * @param value1 first operand
    * @param value2 second operand
    * @param <V> value type (unchecked)
    *
    * @return this builder
    *
    * @implNote operands class must be checked because type is unchecked.
    */
   public <V extends Entity<ID>, ID extends Id> SameValueBuilder append(V value1, V value2) {
      if (sameValue && value1 != value2) {
         sameValue = (value1 != null
                      && value2 != null
                      && value2.getClass().isInstance(value1)
                      && value1.sameIdentityAs(value2));
      }

      return this;
   }

   public SameValueBuilder append(Object[] value1, Object[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.deepEquals(value1, value2);
      }

      return this;
   }

   /**
    * Append for {@code String[]} type.
    *
    * @param value1 first value
    * @param value2 second value
    *
    * @return this builder
    *
    * @implSpec Optimization of {@link #append(Object[], Object[])} for common types
    */
   public SameValueBuilder append(String[] value1, String[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(byte[] value1, byte[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(int[] value1, int[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(long[] value1, long[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(double[] value1, double[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(float[] value1, float[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(boolean[] value1, boolean[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(char[] value1, char[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   public SameValueBuilder append(short[] value1, short[] value2) {
      if (sameValue && value1 != value2) {
         sameValue = Arrays.equals(value1, value2);
      }

      return this;
   }

   /**
    * Final comparison resolver.
    *
    * @return true if values are the same
    */
   public boolean sameValue() {
      return sameValue;
   }

   public Boolean build() {
      return sameValue();
   }
}