package com.tinubu.commons.ddd2.domain.type;

import java.net.URI;

/**
 * Represents an entity id.
 */
public interface Id extends Value, Comparable<Id> {

   /**
    * String representation of the id value.
    * This is NOT the internal representation of the class which is returned using {@link Object#toString()}.
    *
    * @return value string representation. Can be {@code null} if id value is uninitialized
    */
   String stringValue();

   /**
    * URN representation of the id value.
    * <p>
    * URN representation should use <a
    * href="https://www.iana.org/assignments/urn-namespaces/urn-namespaces.xhtml">officially registered
    * URNs</a> when possible, e.g: {@code uuid} NID. Otherwise, the URN should use the non-official
    * {@code urn:id:<id-type>:<id-value>} format.
    *
    * @return value URN representation, as a valid {@link URI}. Can be {@code null} if id value is uninitialized
    *
    * @see <a href="https://tools.ietf.org/html/rfc2141">RFC 2141 : URN Syntax</a>
    */
   URI urnValue();

   /**
    * Informs whether the domain object is a new object.
    * <p>
    * This information can be used later, e.g. by JPA to know if it must persist or merge the object.
    *
    * @implSpec value default is {@code false} because it's the default value for object reconstitution.
    */
   default boolean newObject() {
      return false;
   }

}
