/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.isVersion5;
import static com.tinubu.commons.ddd2.invariant.rules.UuidRules.uuid;
import static java.util.stream.Collectors.joining;

import java.util.List;

import com.tinubu.commons.ddd2.domain.ids.Uuid;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;

/**
 * An {@link AbstractHashVersion} represented as a RFC4122 UUID version 5 (name-based SHA-1) {@link String}.
 * Uppercase UUID format is supported, but will be lower-cased internally.
 */
public class NameBasedSha1UuidHashVersion extends AbstractHashVersion<String> {

   /**
    * Delimiter used in {@code hash(values)} payload.
    * Do not change to not break back-compatibility.
    */
   protected static final String HASH_VALUES_DELIMITER = "+";

   protected NameBasedSha1UuidHashVersion(String value) {
      super(normalize(value));
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends NameBasedSha1UuidHashVersion> defineDomainFields() {
      return Fields
            .<NameBasedSha1UuidHashVersion>builder()
            .superFields((Fields<NameBasedSha1UuidHashVersion>) super.defineDomainFields())
            .field("value", v -> v.value, uuid(isVersion5()))
            .build();
   }

   /**
    * Creates a new {@link IntegerVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static NameBasedSha1UuidHashVersion of(String value) {
      return checkInvariants(new NameBasedSha1UuidHashVersion(value));
   }

   /**
    * Creates a new {@link NameBasedSha1UuidHashVersion} with a name-based (SHA-1) generated UUID (version 5)
    * from specified domain object values.
    *
    * @param domainObject domain object to compute hash from
    *
    * @return new version
    */
   public static <D extends DomainObject> NameBasedSha1UuidHashVersion initialVersion(D domainObject) {
      return internalHashVersion(domainObjectHashValues(domainObject));
   }

   @Override
   protected NameBasedSha1UuidHashVersion hashVersion(List<Object> hashValues) {
      return internalHashVersion(hashValues);
   }

   /**
    * Creates a name-based UUID (version 5) using SHA-1.
    * Created UUID will always be the same for the same input {@code name}.
    *
    * @param name UUID base name to hash
    *
    * @return created UUID
    */
   protected static String nameBasedUuid(String name) {
      return Uuid.newNameBasedSha1Uuid(name).stringValue();
   }

   protected static NameBasedSha1UuidHashVersion internalHashVersion(List<Object> hashValues) {
      return of(nameBasedUuid(hashValues
                                    .stream()
                                    .map(Object::toString)
                                    .collect(joining(HASH_VALUES_DELIMITER))));
   }

   protected static String normalize(String uuid) {
      return uuid != null ? uuid.toLowerCase() : uuid;
   }

}
