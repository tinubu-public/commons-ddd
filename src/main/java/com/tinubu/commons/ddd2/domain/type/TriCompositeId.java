/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import com.tinubu.commons.lang.beans.Getter;

/**
 * Represents a composite id composed of three values.
 * Values can be of any {@link Comparable} type.
 *
 * @param <T1> first value type
 * @param <T2> second value type
 * @param <T3> third value type
 *
 * @see Id
 * @see SimpleId
 */
// FIXME urnValue. urn:id:<type>:<value1>/<value2> -> le séparateur dépend de chaque type
//       il peut choisir son séparateur (technique du <sep><value1><sep><value2> ?)
public interface TriCompositeId<T1 extends Comparable<? super T1>, T2 extends Comparable<? super T2>, T3 extends Comparable<? super T3>>
      extends Id {

   /**
    * First raw id value.
    *
    * @return first raw id value, can be {@code null} if id should be auto-generated
    */
   @Getter
   T1 value1();

   /**
    * Second raw id value.
    *
    * @return second raw id value, can be {@code null} if id should be auto-generated
    */
   @Getter
   T2 value2();

   /**
    * Third raw id value.
    *
    * @return thrid raw id value, can be {@code null} if id should be auto-generated
    */
   @Getter
   T3 value3();

   /**
    * {@inheritDoc}
    *
    * @return raw string representation, can be {@code null} if any value is uninitialized
    */
   @Override
   default String stringValue() {
      if (value1() == null || value2() == null || value3() == null) {
         return null;
      }
      return value1().toString() + "/" + value2().toString() + "/" + value3().toString();
   }

}
