package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import com.tinubu.commons.ddd2.domain.type.support.CachedFields;

/**
 * A DDD default {@link Entity} implementation.
 *
 * @param <ID> id type
 *
 * @implNote Implementation is immutable.
 */
public abstract class AbstractEntity<ID extends Id> implements Entity<ID> {
   protected final CachedFields<? extends Entity<ID>> domainFields =
         new CachedFields<>(this::defineDomainFields);

   /**
    * Overridable domain fields definitions.
    * You should define the {@link #id()} in fields.
    *
    * @return domain fields
    */
   @SuppressWarnings("Convert2MethodRef")
   protected Fields<? extends Entity<ID>> defineDomainFields() {
      return Fields.<Entity<ID>>builder().field("id", v -> v.id(), isNotNull()).build();
   }

   /**
    * Returns cached domain fields.
    *
    * @return domain fields
    */
   @Override
   public Fields<? extends Entity<ID>> domainFields() {
      return domainFields.get();
   }

   @Override
   public String toString() {
      return entityToString();
   }

   @Override
   public boolean equals(Object o) {
      return entityEquals(o);
   }

   @Override
   public int hashCode() {
      return entityHashCode();
   }
}
