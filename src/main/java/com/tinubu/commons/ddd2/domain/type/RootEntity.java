package com.tinubu.commons.ddd2.domain.type;

/**
 * A DDD root entity representation.
 * This interface does not add anything to {@link Entity} but can be used to represent an aggregate root
 * entity.
 *
 * @param <ID> entity id implementation type
 *
 * @see Entity
 */
public interface RootEntity<ID extends Id> extends Entity<ID> {}
