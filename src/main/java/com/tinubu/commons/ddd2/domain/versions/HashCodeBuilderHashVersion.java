/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;

import java.util.List;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.tinubu.commons.ddd2.domain.type.DomainObject;

/**
 * A hash version using {@link HashCodeBuilder} and represented as a {@link Long}.
 */
public class HashCodeBuilderHashVersion extends AbstractHashVersion<Long> {

   /**
    * Generates version with explicit value.
    *
    * @param value version value
    */
   protected HashCodeBuilderHashVersion(Long value) {
      super(value);
   }

   /**
    * Creates a new {@link HashCodeBuilderHashVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static HashCodeBuilderHashVersion of(Long value) {
      return checkInvariants(new HashCodeBuilderHashVersion(value));
   }

   /**
    * Creates a new {@link HashCodeBuilderHashVersion} with initial value.
    *
    * @param domainObject domain object to compute hash from
    *
    * @return new version
    */
   public static <D extends DomainObject> HashCodeBuilderHashVersion initialVersion(D domainObject) {
      return internalHashVersion(domainObjectHashValues(domainObject));
   }

   @Override
   protected HashCodeBuilderHashVersion hashVersion(List<Object> hashValues) {
      return internalHashVersion(hashValues);
   }

   protected static HashCodeBuilderHashVersion internalHashVersion(List<Object> hashValues) {
      HashCodeBuilder hashCodeBuilder = new HashCodeBuilder();
      for (Object hashValue : hashValues) {
         hashCodeBuilder.append(hashValue);
      }
      return of((long) hashCodeBuilder.build());
   }

}
