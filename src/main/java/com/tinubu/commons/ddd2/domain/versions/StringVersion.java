/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.domain.type.AbstractVersion;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Version;

/**
 * A DDD {@link Version} represented as a {@link String}
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class StringVersion extends AbstractVersion<String> {

   protected StringVersion(String value) {
      super(value);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends StringVersion> defineDomainFields() {
      return Fields
            .<StringVersion>builder()
            .superFields((Fields<StringVersion>) super.defineDomainFields())
            .field("value", v -> v.value, isNotBlank())
            .build();
   }

   /**
    * Creates a new {@link StringVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static StringVersion of(String value) {
      return checkInvariants(new StringVersion(value));
   }

   @Override
   public StringVersion nextVersion(DomainObject domainObject) {
      notNull(domainObject, "domainObject");

      throw new UnsupportedOperationException("Can't generate a next version for arbitrary string versions");
   }

}
