/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isTrue;
import static java.util.Comparator.comparing;
import static java.util.Objects.isNull;

import java.util.Comparator;

/**
 * A default {@link BiCompositeId} implementation.
 * The implementation support both persistence style for ids :
 * <ul>
 *    <li>{@link #AbstractBiCompositeId(T1, T2)} : initialized (requesting next id from ORM)</li>
 *    <li>{@link #AbstractBiCompositeId()} : uninitialized (after ORM's save). In this case many operations are restricted</li>
 * </ul>
 *
 * @param <T1> first value type
 * @param <T2> second value type
 *
 * @implNote Implementation is immutable.
 * @see AbstractSimpleId for simple ids.
 */
public abstract class AbstractBiCompositeId<T1 extends Comparable<? super T1>, T2 extends Comparable<? super T2>>
      extends AbstractValue implements BiCompositeId<T1, T2> {

   protected T1 value1;
   protected T2 value2;
   protected final boolean newObject;

   /**
    * Builds {@link AbstractSimpleId} from specified value.
    *
    * @param value1 optional first raw id value
    * @param value2 optional second raw id value
    * @param newObject informs whether the entity object is a new entity. If one value is uninitialized
    *       ({@code null}), newObject must be set to {@code true}
    */
   protected AbstractBiCompositeId(T1 value1, T2 value2, boolean newObject) {
      this.value1 = value1;
      this.value2 = value2;
      this.newObject = newObject;
   }

   /**
    * Builds {@link AbstractSimpleId} from specified value for object reconstitution.
    * {@code newObject} is defaulted to {@code false}.
    *
    * @param value1 optional first raw id value
    * @param value2 optional second raw id value
    */
   protected AbstractBiCompositeId(T1 value1, T2 value2) {
      this(value1, value2, false);
   }

   /**
    * Builds {@link AbstractSimpleId} with uninitialized values.
    * {@code newObject} is defaulted to {@code true}.
    */
   protected AbstractBiCompositeId() {
      this(null, null, true);
   }

   /**
    * Overridable domain fields definitions.
    *
    * @return domain fields
    */
   @Override
   protected Fields<? extends BiCompositeId<T1, T2>> defineDomainFields() {
      return Fields
            .<AbstractBiCompositeId<T1, T2>>builder()
            .field("value1", v -> v.value1)
            .field("value2", v -> v.value2)
            .field("newObject",
                   v -> v.newObject,
                   isTrue("'%1$s' must be true if at least one value is uninitialized",
                          validatingObjectName()).ifIsSatisfied(() -> isNull(value1) || isNull(value2)))
            .build();
   }

   @Override
   public boolean newObject() {
      return newObject;
   }

   public T1 value1() {
      return value1;
   }

   public T2 value2() {
      return value2;
   }

   /**
    * Compares {@link Id} by value.
    * Only comparison of exactly the same id classes is supported.
    * Only comparison of initialized ids is supported.
    *
    * @param value the id to be compared.
    */
   @Override
   @SuppressWarnings("unchecked")
   public int compareTo(Id value) {
      if (!(this.getClass() == value.getClass())) {
         throw new IllegalStateException(String.format("'%s' can't be compared with '%s'",
                                                       this.getClass().getName(),
                                                       value.getClass().getName()));
      }
      if (this.value1 == null
          || this.value2 == null
          || ((AbstractBiCompositeId<?, ?>) value).value1 == null
          || ((AbstractBiCompositeId<?, ?>) value).value2 == null) {
         throw new IllegalStateException("Can't compare uninitialized ids");
      }

      Comparator<AbstractBiCompositeId<T1, T2>> value1Comparator = comparing(id -> id.value1);
      Comparator<AbstractBiCompositeId<T1, T2>> value2Comparator = comparing(id -> id.value2);

      return value1Comparator
            .thenComparing(value2Comparator)
            .compare(this, (AbstractBiCompositeId<T1, T2>) value);
   }

   /**
    * {@inheritDoc}
    * Always return {@code false} if any id is uninitialized.
    */
   @Override
   public boolean sameValueAs(Value value) {
      if (this.value1 == null || this.value2 == null) {
         return false;
      } else {
         return super.sameValueAs(value);
      }
   }
}
