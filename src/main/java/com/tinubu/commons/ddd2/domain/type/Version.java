package com.tinubu.commons.ddd2.domain.type;

import com.tinubu.commons.lang.beans.Getter;

/**
 * Represents an entity version.
 *
 * @param <T> version implementation type
 */
public interface Version<T extends Comparable<? super T>> extends Value, Comparable<Version<T>> {

   /**
    * Raw version value.
    *
    * @return raw version value, never {@code null}
    */
   @Getter
   T value();

   /**
    * Returns the next version from this one.
    *
    * @param domainObject object holding the version, that can be used in computation
    *
    * @return the next version from this one
    */
   Version<T> nextVersion(DomainObject domainObject);

}
