package com.tinubu.commons.ddd2.domain.type;

/**
 * A DDD entity representation with additional version support.
 *
 * @param <ID> entity id implementation type
 * @param <V> entity version implementation type
 */
public interface VersionedEntity<ID extends Id, V extends Version<?>> extends Entity<ID> {

   /**
    * Entity version.
    *
    * @return entity version, never {@code null}
    */
   V version();

   /**
    * Entities compared by version.
    * Compared entity classes must be equals.
    *
    * @param entity the entity to compare to
    *
    * @return {@code true} if the versions are the same, regardless of other attributes.
    */
   default boolean sameVersionAs(VersionedEntity<ID, V> entity) {
      if (entity != null && this.getClass().equals(entity.getClass())) {
         return version() != null && version().sameValueAs(entity.version());
      } else {
         return false;
      }
   }

   /**
    * Returns the next version from this entity version.
    * Entity version won't be updated by this operation.
    *
    * @return the next version from this entity version
    */
   @SuppressWarnings("unchecked")
   default V nextVersion() {
      return (V) version().nextVersion(this);
   }

}
