package com.tinubu.commons.ddd2.domain.ids;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.math.BigInteger;
import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Id;

/**
 * A DDD {@link Id} represented as a {@link BigInteger}.
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class BigIntegerId extends AbstractSimpleId<BigInteger> {

   protected static final String URN_ID_TYPE = "bigint";

   protected BigIntegerId(BigInteger value, boolean newObject) {
      super(value, newObject);
   }

   protected BigIntegerId(BigInteger value) {
      super(value);
   }

   protected BigIntegerId() {
      super();
   }

   /**
    * Creates a new {@link BigIntegerId} for a reconstituting object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static BigIntegerId of(BigInteger value) {
      return checkInvariants(new BigIntegerId(value));
   }

   /**
    * Creates a new {@link BigIntegerId} for a reconstituting object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static BigIntegerId of(URI urn) {
      return of(nullable(urn).map(u -> new BigInteger(idUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link BigIntegerId} for a new object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static BigIntegerId ofNewObject(BigInteger value) {
      return checkInvariants(new BigIntegerId(value, true));
   }

   /**
    * Creates a new {@link BigIntegerId} for a new object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static BigIntegerId ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> new BigInteger(idUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link BigIntegerId} for a new object with uninitialized value.
    *
    * @return new id
    */
   public static BigIntegerId ofNewObject() {
      return checkInvariants(new BigIntegerId());
   }

   @Override
   public BigIntegerId withValue(BigInteger value) {
      return checkInvariants(new BigIntegerId(value, newObject));
   }

   @Override
   public URI urnValue() {
      return idUrn(URN_ID_TYPE);
   }
}
