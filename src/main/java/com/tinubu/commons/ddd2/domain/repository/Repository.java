/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.repository;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;

import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;

/**
 * DDD repository interface for entities.
 *
 * @param <T> entity type
 * @param <ID> entity id type
 */
public interface Repository<T extends Entity<ID>, ID extends Id> {

   /**
    * Determines if specified repository represents the same "storage space" that this repository.
    * For example :
    * <ul>
    *    <li>no noop repository is the same as another repository of any kind, including itself object</li>
    *    <li>no memory based repository is the same as another memory based repository, excepting itself object</li>
    *    <li>two FS based repository are similar only if they point to the same physical storage and have the same storage strategy</li>
    *    <li>two SFTP based repository are similar only if they point to the same server host and base path</li>
    * </ul>
    *
    * @param repository repository to compare
    */
   default boolean sameRepositoryAs(Repository<T, ID> repository) {
      notNull(repository, "repository");

      return Objects.equals(this, repository);
   }

}
