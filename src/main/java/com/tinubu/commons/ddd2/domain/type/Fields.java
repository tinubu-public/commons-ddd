package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.stream.Collectors.toList;

import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import com.tinubu.commons.ddd2.domain.type.Fields.Field;
import com.tinubu.commons.ddd2.invariant.Invariant;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.Invariants;

/**
 * Fields {@link Map} of {@link Field} indexed by field name.
 *
 * @param <T> domain object type
 *
 * @implSpec Immutable class implementation.
 */
public class Fields<T extends DomainObject> extends AbstractMap<String, Field<T, ?>> {
   /** Default groups for fields with no explicit groups. */
   private static final List<String> DEFAULT_GROUPS = list("default");

   private final Map<String, Field<T, ?>> fields;
   private final Invariants invariants;

   private Fields(FieldsBuilder<T> builder) {
      this.fields = immutable(map(() -> new LinkedHashMap<>(), builder.fields));
      this.invariants = notNull(builder.invariants, "invariants");
   }

   public static <T extends DomainObject> FieldsBuilder<T> builder() {
      return new FieldsBuilder<>();
   }

   @Override
   public Set<Entry<String, Field<T, ?>>> entrySet() {
      return new AbstractSet<Entry<String, Field<T, ?>>>() {
         @Override
         public Iterator<Entry<String, Field<T, ?>>> iterator() {
            return fields.entrySet().iterator();
         }

         @Override
         public int size() {
            return fields.size();
         }
      };

   }

   public Invariants invariants() {
      return invariants;
   }

   /**
    * Returns all fields invariants associated to specified domain object.
    *
    * @param domainObject domain object to associate to invariants
    *
    * @return fields invariants
    */
   public Invariants invariants(T domainObject) {
      return Invariants
            .of(fields
                      .values().stream().flatMap(field -> stream(field.invariantForObject(domainObject)))
                      .collect(toList()))
            .withInvariants(invariants);
   }

   /**
    * {@link Fields} builder.
    *
    * @param <T> domain object type
    */
   public static class FieldsBuilder<T extends DomainObject> {
      private final Map<String, Field<T, ?>> fields = new LinkedHashMap<>();
      private Invariants invariants = Invariants.empty();

      public FieldsBuilder() {}

      public Fields<T> empty() {
         return new FieldsBuilder<T>().build();
      }

      /**
       * Includes fields defined in super class into this fields list.
       *
       * @param fields fields from super class
       *
       * @return this
       */
      public FieldsBuilder<T> superFields(Fields<? super T> fields, String... excludes) {
         superFieldsWithoutInvariants(fields, excludes);
         invariants(fields.invariants());
         return this;
      }

      public FieldsBuilder<T> superFieldsWithoutInvariants(Fields<? super T> fields, String... excludes) {
         List<String> excludesList = list(excludes);

         for (Field<? super T, ?> value : fields.values()) {
            if (!excludesList.contains(value.name())) {
               field(Field.from(value));
            }
         }
         return this;
      }

      public <F> FieldsBuilder<T> field(Field<T, F> field) {
         fields.put(field.name, field);
         return this;
      }

      public <F> FieldsBuilder<T> field(String name, Function<? super T, F> value) {
         return field(Field.of(name, value));
      }

      public <F> FieldsBuilder<T> field(String name,
                                        Function<? super T, ? extends F> value,
                                        Function<? super F, Object> displayValue) {
         return field(Field.<T, F>of(name, value).displayValue(displayValue.compose(value)));
      }

      public <F> FieldsBuilder<T> field(String name,
                                        Function<? super T, ? extends F> value,
                                        InvariantRule<F> invariantRule,
                                        String... groups) {
         return field(Field.<T, F>of(name, value).invariant(invariantRule, groups));
      }

      public <F> FieldsBuilder<T> field(String name,
                                        Function<? super T, ? extends F> value,
                                        Function<? super F, Object> displayValue,
                                        InvariantRule<F> invariantRule,
                                        String... groups) {
         return field(Field.<T, F>of(name, value).displayValue(displayValue.compose(value))
                            .invariant(invariantRule, groups));
      }

      public <F> FieldsBuilder<T> technicalField(String name, Function<? super T, F> value) {
         return field(Field.<T, F>of(name, value).technical(true));
      }

      public <F> FieldsBuilder<T> technicalField(String name,
                                                 Function<? super T, ? extends F> value,
                                                 Function<? super F, Object> displayValue) {
         return field(Field.<T, F>of(name, value).displayValue(displayValue.compose(value)).technical(true));
      }

      public <F> FieldsBuilder<T> technicalField(String name,
                                                 Function<? super T, ? extends F> value,
                                                 InvariantRule<F> invariantRule,
                                                 String... groups) {
         return field(Field.<T, F>of(name, value).invariant(invariantRule, groups).technical(true));
      }

      public <F> FieldsBuilder<T> technicalField(String name,
                                                 Function<? super T, ? extends F> value,
                                                 Function<? super F, Object> displayValue,
                                                 InvariantRule<F> invariantRule,
                                                 String... groups) {
         return field(Field
                            .<T, F>of(name, value)
                            .displayValue(displayValue.compose(value))
                            .invariant(invariantRule, groups)
                            .technical(true));
      }

      @SafeVarargs
      public final FieldsBuilder<T> invariants(Invariant<? extends T>... invariants) {
         this.invariants = this.invariants.withInvariants(invariants);

         return this;
      }

      public final FieldsBuilder<T> invariants(Invariants invariants) {
         this.invariants = invariants;

         return this;
      }

      public Fields<T> build() {
         return new Fields<>(this);
      }

   }

   /**
    * Field definition.
    *
    * @param <T> enclosing domain object type
    * @param <F> field type
    *
    * @implSpec Immutable class implementation.
    */
   public static class Field<T extends DomainObject, F> {
      private final String name;
      private final Function<? super T, ? extends F> value;
      private final Function<? super T, Object> displayValue;
      private final Function<? super T, Invariant<F>> invariant;
      /**
       * Indicates if the field is a technical field or a domain field. Technical fields are excluded from
       * {@link Value#sameValueAs(Value)} computation.
       */
      private final boolean technical;

      private Field(String name,
                    Function<? super T, ? extends F> value,
                    Function<? super T, Object> displayValue,
                    Function<? super T, Invariant<F>> invariant,
                    boolean technical) {
         this.name = notBlank(name, "name");
         this.value = notNull(value, "value");
         this.displayValue = notNull(displayValue, "displayValue");
         this.invariant = invariant;
         this.technical = technical;
      }

      @SuppressWarnings("unchecked")
      public static <T extends DomainObject, F> Field<T, F> of(String name,
                                                               Function<? super T, ? extends F> value) {
         return new Field<>(name, value, (Function<? super T, Object>) value, null, false);
      }

      public static <T extends DomainObject, F> Field<T, F> from(final Field<? super T, F> field) {
         return new Field<>(field.name, field.value, field.displayValue, field.invariant, field.technical);
      }

      /**
       * @return field name
       */
      public String name() {
         return name;
      }

      /**
       * @return field value for specified value
       */
      public Function<? super T, ? extends F> value() {
         return value;
      }

      /**
       * @return field display value for specified value
       */
      public Function<? super T, Object> displayValue() {
         return displayValue;
      }

      public Field<T, F> displayValue(Function<? super T, Object> displayValue) {
         return new Field<>(name, value, displayValue, invariant, technical);
      }

      public Field<T, F> invariant(InvariantRule<F> invariantRule, String... groups) {
         return new Field<>(name, value, displayValue, fieldInvariant(invariantRule, groups), technical);
      }

      private Function<T, Invariant<F>> fieldInvariant(InvariantRule<F> invariantRule, String[] groups) {
         return domainObject -> Invariant
               .of(() -> value().apply(domainObject), invariantRule)
               .name(name)
               .objectName(name)
               .groups(streamConcat(stream(name),
                                    groups.length == 0 ? stream(DEFAULT_GROUPS) : stream(groups)).collect(
                     toList()));
      }

      /**
       * Evaluates invariant definition using specified domain object or returns {@link Optional#empty()} if
       * no invariant definition set.
       *
       * @param domainObject domain object to apply to invariant
       * @param <V> domain object type
       *
       * @return invariant or {@link Optional#empty()} if no invariant defined
       */
      private <V extends T> Optional<Invariant<F>> invariantForObject(V domainObject) {
         return nullable(invariant).map(i -> i.apply(domainObject));
      }

      public boolean technical() {
         return technical;
      }

      public Field<T, F> technical(boolean technical) {
         return new Field<>(name, value, displayValue, invariant, technical);
      }

   }
}