/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.repository;

import java.util.stream.Stream;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;

/**
 * DDD queryable repository interface using specification. Since a repository is queryable, all entities can
 * be returned and any specification can be applied to filter the query.
 *
 * @param <T> entity type
 * @param <ID> entity id type
 *
 * @see ReadableRepository
 * @see IterableRepository
 * @see QueryableRepository
 * @see WritableRepository
 */
@FunctionalInterface
public interface QueryableRepository<T extends Entity<ID>, ID extends Id> extends IterableRepository<T, ID> {

   /**
    * Finds entity by specification.
    *
    * @param specification entity specification
    *
    * @return found entities stream
    *
    * @throws RepositoryIOException if unexpected repository IO error occurs
    */
   Stream<T> findBySpecification(Specification<T> specification);

   /**
    * Finds all entities.
    *
    * @return found entities stream
    *
    * @throws RepositoryException if unexpected repository error occurs
    * @throws RepositoryIOException if I/O error occurs
    */
   @Override
   default Stream<T> findAll() {
      return findBySpecification(__ -> true);
   }

}
