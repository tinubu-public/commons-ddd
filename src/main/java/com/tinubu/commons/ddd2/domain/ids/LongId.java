package com.tinubu.commons.ddd2.domain.ids;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.net.URI;

import com.tinubu.commons.ddd2.domain.type.AbstractSimpleId;
import com.tinubu.commons.ddd2.domain.type.Id;

/**
 * A DDD {@link Id} represented as a {@link Long}.
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class LongId extends AbstractSimpleId<Long> {

   protected static final String URN_ID_TYPE = "long";

   protected LongId(Long value, boolean newObject) {
      super(value, newObject);
   }

   protected LongId(Long value) {
      super(value);
   }

   protected LongId() {
      super();
   }

   /**
    * Creates a new {@link LongId} for a reconstituting object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static LongId of(Long value) {
      return checkInvariants(new LongId(value));
   }

   /**
    * Creates a new {@link LongId} for a reconstituting object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static LongId of(URI urn) {
      return of(nullable(urn).map(u -> Long.parseLong(idUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link LongId} for a new object with specified value.
    *
    * @param value id value
    *
    * @return new id
    */
   public static LongId ofNewObject(Long value) {
      return checkInvariants(new LongId(value, true));
   }

   /**
    * Creates a new {@link LongId} for a new object with specified URN.
    *
    * @param urn URN
    *
    * @return new id
    */
   public static LongId ofNewObject(URI urn) {
      return ofNewObject(nullable(urn).map(u -> Long.parseLong(idUrnValue(u, URN_ID_TYPE))).orElse(null));
   }

   /**
    * Creates a new {@link LongId} for a new object with uninitialized value.
    *
    * @return new id
    */
   public static LongId ofNewObject() {
      return checkInvariants(new LongId());
   }

   @Override
   public LongId withValue(Long value) {
      return checkInvariants(new LongId(value, newObject));
   }

   @Override
   public URI urnValue() {
      return idUrn(URN_ID_TYPE);
   }

}
