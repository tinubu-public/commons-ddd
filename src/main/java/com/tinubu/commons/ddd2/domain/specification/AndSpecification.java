package com.tinubu.commons.ddd2.domain.specification;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * 'and' specification, used to create a new specification that is the 'and' of specifications.
 */
public class AndSpecification<T> implements CompositeSpecification<T> {

   /**
    * Specifications to be And'ed.
    */
   private List<Specification<T>> specifications;

   /**
    * Create a new 'and' specification based on two other specifications.
    *
    * @param specifications specifications
    */
   public AndSpecification(final List<Specification<T>> specifications) {
      this.specifications = noNullElements(specifications, "specifications");
   }

   /**
    * {@inheritDoc}
    *
    * @implNote All specifications are always iterated, however Java will short-circuit the second
    *       operand evaluation in accumulator if result is already {@code false}.
    */
   public boolean satisfiedBy(final T object) {
      notNull(object, "object");

      return specifications.stream().allMatch(s -> s.satisfiedBy(object));
   }

   public List<Specification<T>> specifications() {
      return specifications;
   }

   @Override
   public <V> V accept(CompositeSpecificationVisitor<T, V> visitor) {
      notNull(visitor, "visitor");

      return visitor.visit(this);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      AndSpecification<?> that = (AndSpecification<?>) o;
      return Objects.equals(specifications, that.specifications);
   }

   @Override
   public int hashCode() {
      return Objects.hash(specifications);
   }

   @Override
   public String toString() {
      return new StringJoiner(",", AndSpecification.class.getSimpleName() + "[", "]")
            .add("specifications=" + specifications)
            .toString();
   }

}
