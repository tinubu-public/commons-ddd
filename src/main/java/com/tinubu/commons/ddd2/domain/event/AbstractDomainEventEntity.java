/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.event;

import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;

import java.time.Instant;

import com.tinubu.commons.ddd2.domain.type.AbstractEntity;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.lang.datetime.ApplicationClock;

/**
 * Abstract {@link Entity}-backed {@link DomainEvent} implementation.
 *
 * @param <ID> event id type
 */
public abstract class AbstractDomainEventEntity<ID extends Id> extends AbstractEntity<ID>
      implements DomainEvent {

   protected final ID eventId;
   protected final Object source;
   protected final Instant eventDate;

   /**
    * Constructs a prototypical event.
    *
    * @param source the object on which the event initially occurred
    */
   protected AbstractDomainEventEntity(ID eventId, Object source) {
      this.eventId = eventId;
      this.source = source;
      this.eventDate = ApplicationClock.nowAsInstant();
   }

   @Override
   protected Fields<? extends AbstractDomainEventEntity<ID>> defineDomainFields() {
      return Fields
            .<AbstractDomainEventEntity<ID>>builder()
            .field("eventId", v -> v.eventId, isNotNull())
            .field("source", v -> v.source, isNotNull())
            .field("eventDate", v -> v.eventDate, isNotNull())
            .build();
   }

   @Override
   public Object source() {
      return source;
   }

}
