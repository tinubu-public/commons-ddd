/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.versions;

import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.rules.NumberRules.isPositive;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;

import java.math.BigInteger;

import com.tinubu.commons.ddd2.domain.type.AbstractVersion;
import com.tinubu.commons.ddd2.domain.type.DomainObject;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.Version;

/**
 * A DDD {@link Version} represented as a {@link BigInteger}
 *
 * @implNote Implementation is thread-safe and immutable.
 */
public class BigIntegerVersion extends AbstractVersion<BigInteger> {

   protected static final BigInteger INITIAL_VALUE = ZERO;

   protected BigIntegerVersion(BigInteger value) {
      super(value);
   }

   @Override
   @SuppressWarnings("unchecked")
   protected Fields<? extends BigIntegerVersion> defineDomainFields() {
      return Fields.<BigIntegerVersion>builder()
            .superFields((Fields<BigIntegerVersion>) super.defineDomainFields())
            .field("value", v -> v.value, isPositive())
            .build();
   }

   /**
    * Creates a new {@link BigIntegerVersion} with specified value.
    *
    * @param value version value
    *
    * @return new version
    */
   public static BigIntegerVersion of(BigInteger value) {
      return checkInvariants(new BigIntegerVersion(value));
   }

   /**
    * Creates a new {@link BigIntegerVersion} with initial value.
    *
    * @return new version
    */
   public static BigIntegerVersion initialVersion() {
      return checkInvariants(new BigIntegerVersion(INITIAL_VALUE));
   }

   @Override
   public BigIntegerVersion nextVersion(DomainObject domainObject) {
      notNull(domainObject, "domainObject");

      return of(value.add(ONE));
   }

}
