package com.tinubu.commons.ddd2.domain.specification;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * 'or' specification, used to create a new specification that is the 'or' of specifications.
 */
public class OrSpecification<T> implements CompositeSpecification<T> {

   /**
    * Specifications to be Or'ed.
    */
   private List<Specification<T>> specifications;

   /**
    * Creates a new 'or' specification based on two other specifications.
    *
    * @param specifications specifications
    */
   public OrSpecification(List<Specification<T>> specifications) {
      this.specifications = noNullElements(specifications, "specifications");
      ;
   }

   /**
    * {@inheritDoc}
    *
    * @implNote All specifications are always iterated, however Java will short-circuit the second
    *       operand evaluation in accumulator if result is already {@code true}.
    */
   public boolean satisfiedBy(final T object) {
      notNull(object, "object");

      return specifications.stream().anyMatch(s -> s.satisfiedBy(object));
   }

   public List<Specification<T>> specifications() {
      return specifications;
   }

   @Override
   public <V> V accept(CompositeSpecificationVisitor<T, V> visitor) {
      notNull(visitor, "visitor");

      return visitor.visit(this);
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      OrSpecification<?> that = (OrSpecification<?>) o;
      return Objects.equals(specifications, that.specifications);
   }

   @Override
   public int hashCode() {
      return Objects.hash(specifications);
   }

   @Override
   public String toString() {
      return new StringJoiner(",", OrSpecification.class.getSimpleName() + "[", "]")
            .add("specifications=" + specifications)
            .toString();
   }

}
