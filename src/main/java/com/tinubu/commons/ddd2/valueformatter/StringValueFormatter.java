/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.valueformatter;

/**
 * Value formatter for {@link CharSequence}.
 */
public class StringValueFormatter implements ValueFormatter<Object> {

   /**
    * Maximum length to display. Set to 0 to display all elements.
    */
   private static final int DEFAULT_MAX_LENGTH = 0;

   /**
    * Max items to display. Set to 0 to disable the limitation.
    */
   private final int maxLength;

   public StringValueFormatter(int maxLength) {
      this.maxLength = maxLength;
   }

   public StringValueFormatter() {
      this(DEFAULT_MAX_LENGTH);
   }

   @Override
   public CharSequence apply(Object object) {
      if (object == null) {
         return null;
      }

      String string = object.toString();

      if (maxLength == 0 || string.length() <= maxLength) {
         return string;
      } else {
         return string.subSequence(0, maxLength) + "...";
      }
   }

}
