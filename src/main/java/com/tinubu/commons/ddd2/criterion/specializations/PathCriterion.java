/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.criterion.specializations;

import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.CONTAIN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.END_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.IN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.MATCH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_CONTAIN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_END_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_IN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_MATCH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_START_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.START_WITH;
import static com.tinubu.commons.ddd2.criterion.Criterion.Flag.IGNORE_CASE;
import static com.tinubu.commons.ddd2.domain.type.support.DomainObjectSupport.checkInvariants;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.allSatisfies;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.contains;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.containsIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.endsWith;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.endsWithIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.hasNoTraversal;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isEqualToIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isIn;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isInIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotEqualToIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotIn;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.isNotInIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.matches;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.matchesIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notContains;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notContainsIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notEndsWith;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notEndsWithIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notMatches;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notMatchesIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notStartsWith;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.notStartsWithIgnoreCase;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.startsWith;
import static com.tinubu.commons.ddd2.invariant.rules.PathRules.startsWithIgnoreCase;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import com.tinubu.commons.ddd2.criterion.Criterion;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantRule;

/**
 * Specialized {@link Criterion} implementation for {@link Path}.
 * Most operators switch from string-based comparison to path-based comparison that does not cross directory
 * separators.
 * {@link CriterionOperator#MATCH} operator supports {@link FileSystem#getPathMatcher(String)}'s patterns.
 * Pattern should be prefixed by either {@code glob:} or {@code regex:}. If no prefix specified,
 * {@value #DEFAULT_PATTERN_TYPE} type is assumed.
 */
public class PathCriterion extends Criterion<Path> {

   private static final String DEFAULT_PATTERN_TYPE = "glob";

   private final Map<CriterionOperator, Function<Criterion<Path>, Predicate<Path>>> operatorSpecifications =
         new HashMap<CriterionOperator, Function<Criterion<Path>, Predicate<Path>>>() {{
            put(EQUAL, PathCriterion.this::equalPredicate);
            put(NOT_EQUAL, PathCriterion.this::notEqualPredicate);
            put(IN, PathCriterion.this::inPredicate);
            put(NOT_IN, PathCriterion.this::notInPredicate);
            put(MATCH, PathCriterion.this::matchPredicate);
            put(NOT_MATCH, PathCriterion.this::notMatchPredicate);
            put(START_WITH, PathCriterion.this::startWithPredicate);
            put(NOT_START_WITH, PathCriterion.this::notStartWithPredicate);
            put(END_WITH, PathCriterion.this::endWithPredicate);
            put(NOT_END_WITH, PathCriterion.this::notEndWithPredicate);
            put(CONTAIN, PathCriterion.this::containPredicate);
            put(NOT_CONTAIN, PathCriterion.this::notContainPredicate);
         }};

   protected PathCriterion(Criterion<Path> criterion) {
      super(CriterionBuilder.from(criterion.map(Path::normalize)));
   }

   public static PathCriterion of(Criterion<Path> criterion) {
      return checkInvariants(new PathCriterion(criterion));
   }

   @SuppressWarnings("unchecked")
   @Override
   protected Fields<? extends PathCriterion> defineDomainFields() {
      return Fields
            .<PathCriterion>builder()
            .superFields((Fields<PathCriterion>) super.defineDomainFields())
            .field("operands", v -> v.operands, allSatisfies(isValidPath()))
            .build();
   }

   @Override
   public boolean satisfiedBy(Path operand) {
      return nullable(operatorSpecifications.get(operator))
            .map(factory -> factory.apply(this).test(operand))
            .orElseGet(() -> super.satisfiedBy(operand));
   }

   private static InvariantRule<Path> isValidPath() {
      return isNotNull().andValue(hasNoTraversal());
   }

   private Predicate<Path> equalPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return isEqualToIgnoreCase(value(criterion.operand(0)));
      } else {
         return isEqualTo(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> notEqualPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return isNotEqualToIgnoreCase(value(criterion.operand(0)));
      } else {
         return isNotEqualTo(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> inPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return isInIgnoreCase(value(criterion.operands()));
      } else {
         return isIn(value(criterion.operands()));
      }
   }

   private Predicate<Path> notInPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return isNotInIgnoreCase(value(criterion.operands()));
      } else {
         return isNotIn(value(criterion.operands()));
      }
   }

   private Predicate<Path> matchPredicate(Criterion<Path> criterion) {
      String pattern = criterion.operand(0).toString();
      pattern = ensurePatternType(pattern);
      if (criterion.hasFlag(IGNORE_CASE)) {
         return matchesIgnoreCase(value(pattern));
      } else {
         return matches(value(pattern));
      }
   }

   private Predicate<Path> notMatchPredicate(Criterion<Path> criterion) {
      String pattern = criterion.operand(0).toString();
      pattern = ensurePatternType(pattern);

      if (criterion.hasFlag(IGNORE_CASE)) {
         return notMatchesIgnoreCase(value(pattern));
      } else {
         return notMatches(value(pattern));
      }
   }

   private String ensurePatternType(String pattern) {
      if (!pattern.startsWith("glob:") && !pattern.startsWith("regex:")) {
         pattern = DEFAULT_PATTERN_TYPE + ":" + pattern;
      }
      return pattern;
   }

   private Predicate<Path> startWithPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return startsWithIgnoreCase(value(criterion.operand(0)));
      } else {
         return startsWith(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> notStartWithPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return notStartsWithIgnoreCase(value(criterion.operand(0)));
      } else {
         return notStartsWith(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> endWithPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return endsWithIgnoreCase(value(criterion.operand(0)));
      } else {
         return endsWith(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> notEndWithPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return notEndsWithIgnoreCase(value(criterion.operand(0)));
      } else {
         return notEndsWith(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> containPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return containsIgnoreCase(value(criterion.operand(0)));
      } else {
         return contains(value(criterion.operand(0)));
      }
   }

   private Predicate<Path> notContainPredicate(Criterion<Path> criterion) {
      if (criterion.hasFlag(IGNORE_CASE)) {
         return notContainsIgnoreCase(value(criterion.operand(0)));
      } else {
         return notContains(value(criterion.operand(0)));
      }
   }

}