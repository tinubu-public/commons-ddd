/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static java.util.stream.Collectors.toList;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Stream;

/**
 * Holds an invariant context.
 *
 * @implSpec Immutable class implementation.
 */
public class InvariantContext extends AbstractList<Object> {

   private final List<Object> context;

   private InvariantContext(List<Object> context) {
      this.context = noNullElements(context, "context");
   }

   public static InvariantContext empty() {
      return new InvariantContext(new ArrayList<>());
   }

   public static InvariantContext of(List<Object> context) {
      return new InvariantContext(context);
   }

   public static InvariantContext of(Object... context) {
      return new InvariantContext(Arrays.asList(context));
   }

   public InvariantContext addContext(List<Object> context) {
      return new InvariantContext(Stream.concat(stream(), context.stream()).collect(toList()));
   }

   public InvariantContext addContext(Object... context) {
      return addContext(Arrays.asList(context));
   }

   @Override
   public Object get(int index) {
      return context.get(index);
   }

   @Override
   public int size() {
      return context.size();
   }

   @Override
   public String toString() {
      return new StringJoiner(",", InvariantContext.class.getSimpleName() + "[", "]")
            .add("context=" + context)
            .toString();
   }
}