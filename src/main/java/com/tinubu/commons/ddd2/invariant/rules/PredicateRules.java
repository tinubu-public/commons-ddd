/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.function.Predicate;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class PredicateRules {

   private PredicateRules() {
   }

   public static <T> InvariantRule<T> satisfies(final Predicate<? super T> predicate,
                                                final MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return satisfiesValue(predicate, messageFormatter).ruleContext("PredicateRules.satisfies");
   }

   @SafeVarargs
   public static <T> InvariantRule<T> satisfies(final Predicate<? super T> predicate,
                                                final String message,
                                                final MessageValue<T>... values) {
      return satisfies(predicate, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> satisfies(final Predicate<? super T> predicate) {
      return satisfies(predicate, FastStringFormat.of("'", validatingObject(), "' must satisfy predicate"));
   }

   public static <T> InvariantRule<T> notSatisfies(final Predicate<? super T> predicate,
                                                   final MessageFormatter<T> messageFormatter) {
      notNull(predicate, "predicate");

      return notSatisfiesValue(predicate, messageFormatter).ruleContext("PredicateRules.notSatisfies");
   }

   @SafeVarargs
   public static <T> InvariantRule<T> notSatisfies(final Predicate<? super T> predicate,
                                                   final String message,
                                                   final MessageValue<T>... values) {
      return notSatisfies(predicate, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> notSatisfies(final Predicate<? super T> predicate) {
      return notSatisfies(predicate,
                          FastStringFormat.of("'", validatingObject(), "' must not satisfy predicate"));
   }
}
