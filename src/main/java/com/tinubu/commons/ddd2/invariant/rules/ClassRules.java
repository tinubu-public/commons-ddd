/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.ClassValueFormatter;

public class ClassRules {

   private static final ClassValueFormatter CLASS_VALUE_FORMATTER = new ClassValueFormatter();

   private ClassRules() {
   }

   public static <T> InvariantRule<T> isInstanceOf(final ParameterValue<Class<?>> checkClass,
                                                   final MessageFormatter<T> messageFormatter) {
      notNull(checkClass, "checkClass");

      return satisfiesValue(v -> checkClass.requireNonNullValue().isInstance(v),
                                                 messageFormatter).ruleContext("ClassRules.isInstanceOf",
                                                                               checkClass);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isInstanceOf(final ParameterValue<Class<?>> checkClass,
                                                   final String message,
                                                   final MessageValue<T>... values) {
      return isInstanceOf(checkClass, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isInstanceOf(final ParameterValue<Class<?>> checkClass) {
      return isInstanceOf(checkClass,
                          FastStringFormat.of("'",
                                              validatingObject(CLASS_VALUE_FORMATTER.compose(Object::getClass)),
                                              "' must be instance of '",
                                              parameter(0, CLASS_VALUE_FORMATTER),
                                              "' class"));
   }

   public static <T> InvariantRule<T> isNotInstanceOf(final ParameterValue<Class<?>> checkClass,
                                                      final MessageFormatter<T> messageFormatter) {
      notNull(checkClass, "checkClass");

      return notSatisfiesValue(v -> checkClass.requireNonNullValue().isInstance(v),
                                                    messageFormatter).ruleContext("ClassRules.isNotInstanceOf",
                                                                                  checkClass);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isNotInstanceOf(final ParameterValue<Class<?>> checkClass,
                                                      final String message,
                                                      final MessageValue<T>... values) {
      return isNotInstanceOf(checkClass, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isNotInstanceOf(final ParameterValue<Class<?>> checkClass) {
      return isNotInstanceOf(checkClass,
                             FastStringFormat.of("'",
                                                 validatingObject(CLASS_VALUE_FORMATTER.compose(Object::getClass)),
                                                 "' must not be instance of '",
                                                 parameter(0, CLASS_VALUE_FORMATTER),
                                                 "' class"));
   }

   public static InvariantRule<Class<?>> isClassOf(final ParameterValue<Class<?>> checkClass,
                                                   final MessageFormatter<Class<?>> messageFormatter) {
      notNull(checkClass, "checkClass");

      return satisfiesValue(v -> checkClass.requireNonNullValue().isAssignableFrom(v),
                                                 messageFormatter).ruleContext("ClassRules.isClassOf",
                                                                               checkClass);
   }

   @SafeVarargs
   public static InvariantRule<Class<?>> isClassOf(final ParameterValue<Class<?>> checkClass,
                                                   final String message,
                                                   final MessageValue<Class<?>>... values) {
      return isClassOf(checkClass, DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Class<?>> isClassOf(final ParameterValue<Class<?>> checkClass) {
      return isClassOf(checkClass,
                       FastStringFormat.of("'",
                                           validatingObject(CLASS_VALUE_FORMATTER),
                                           "' class must be instance of '",
                                           parameter(0, CLASS_VALUE_FORMATTER),
                                           "' class"));
   }

   public static InvariantRule<Class<?>> isNotClassOf(final ParameterValue<Class<?>> checkClass,
                                                      final MessageFormatter<Class<?>> messageFormatter) {
      notNull(checkClass, "checkClass");

      return notSatisfiesValue(v -> checkClass.requireNonNullValue().isAssignableFrom(v),
                                                    messageFormatter).ruleContext("ClassRules.isNotClassOf",
                                                                                  checkClass);
   }

   @SafeVarargs
   public static InvariantRule<Class<?>> isNotClassOf(final ParameterValue<Class<?>> checkClass,
                                                      final String message,
                                                      final MessageValue<Class<?>>... values) {
      return isNotClassOf(checkClass, DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Class<?>> isNotClassOf(final ParameterValue<Class<?>> checkClass) {
      return isNotClassOf(checkClass,
                          FastStringFormat.of("'",
                                              validatingObject(CLASS_VALUE_FORMATTER),
                                              "' class must not be instance of '",
                                              parameter(0, CLASS_VALUE_FORMATTER),
                                              "' class"));
   }

   public static <T, U extends T> InvariantRule<T> optionalInstanceOf(final Class<U> checkClass,
                                                                      final InvariantRule<U> rule) {
      notNull(checkClass, "checkClass");
      notNull(rule, "rule");

      return isNotInstanceOf(value(checkClass)).orValue(as(checkClass::cast, rule));
   }

   public static <T, U extends T> InvariantRule<T> instanceOf(final Class<U> checkClass,
                                                              final InvariantRule<U> rule) {
      notNull(checkClass, "checkClass");
      notNull(rule, "rule");

      return isInstanceOf(value(checkClass)).andValue(as(checkClass::cast, rule));
   }

}
