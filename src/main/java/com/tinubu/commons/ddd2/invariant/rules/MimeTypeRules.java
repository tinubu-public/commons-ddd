/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.optionalValue;
import static com.tinubu.commons.ddd2.invariant.rules.OptionalRules.requiresValue;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.charset.Charset;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.lang.mimetype.MimeType;

/**
 * Invariant rules for {@link MimeType}.
 */
public final class MimeTypeRules {

   private MimeTypeRules() {}

   public static <T extends MimeType> InvariantRule<T> withStrippedParameters(final InvariantRule<? super MimeType> rule) {
      return isNotNull().andValue(as(MimeType::strippedParameters, rule));
   }

   public static <T extends MimeType> InvariantRule<T> withStrippedExperimental(final InvariantRule<? super MimeType> rule) {
      return isNotNull().andValue(as(MimeType::strippedExperimental, rule));
   }

   public static <T extends MimeType> InvariantRule<T> type(final InvariantRule<? super String> rule) {
      return isNotNull().andValue(property(MimeType::type, "type", rule));
   }

   public static <T extends MimeType> InvariantRule<T> subtype(final InvariantRule<? super String> rule) {
      return isNotNull().andValue(property(MimeType::subtype, "subtype", rule));
   }

   public static <T extends MimeType> InvariantRule<T> charset(final InvariantRule<? super Charset> rule) {
      return isNotNull().andValue(property(MimeType::charset, "charset", requiresValue(rule)));
   }

   public static <T extends MimeType> InvariantRule<T> optionalCharset(final InvariantRule<? super Charset> rule) {
      return isNotNull().andValue(property(MimeType::charset, "charset", optionalValue(rule)));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> isTypeAndSubtypeEqualTo(final ParameterValue<P> value,
                                                                                                   final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.equalsTypeAndSubtype(value.requireNonNullValue()),
                                                 messageFormatter).ruleContext(
            "MimeTypeRules.isTypeAndSubtypeEqualTo", value);
   }

   @SafeVarargs
   public static <T extends MimeType, P extends MimeType> InvariantRule<T> isTypeAndSubtypeEqualTo(final ParameterValue<P> value,
                                                                                                   final String message,
                                                                                                   final MessageValue<T>... values) {
      return isTypeAndSubtypeEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> isTypeAndSubtypeEqualTo(final ParameterValue<P> value) {
      return isTypeAndSubtypeEqualTo(value,
                                     FastStringFormat.of("'",
                                                         validatingObject(),
                                                         "' must be equal to '",
                                                         parameter(0),
                                                         "' (type and subtype only)"));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> matches(final ParameterValue<P> value,
                                                                                   final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.matches(value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("MimeTypeRules.matches",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends MimeType, P extends MimeType> InvariantRule<T> matches(final ParameterValue<P> value,
                                                                                   final String message,
                                                                                   final MessageValue<T>... values) {
      return matches(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> matches(final ParameterValue<P> value) {
      return matches(value,
                     FastStringFormat.of("'", validatingObject(), "' must match '", parameter(0), "'"));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> doesNotMatch(final ParameterValue<P> value,
                                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !v.matches(value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("MimeTypeRules.doesNotMatch",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends MimeType, P extends MimeType> InvariantRule<T> doesNotMatch(final ParameterValue<P> value,
                                                                                        final String message,
                                                                                        final MessageValue<T>... values) {
      return doesNotMatch(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> doesNotMatch(final ParameterValue<P> value) {
      return doesNotMatch(value,
                          FastStringFormat.of("'",
                                              validatingObject(),
                                              "' must not match '",
                                              parameter(0),
                                              "'"));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> includes(final ParameterValue<P> value,
                                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> v.includes(value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("MimeTypeRules.includes",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends MimeType, P extends MimeType> InvariantRule<T> includes(final ParameterValue<P> value,
                                                                                    final String message,
                                                                                    final MessageValue<T>... values) {
      return includes(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> includes(final ParameterValue<P> value) {
      return includes(value,
                      FastStringFormat.of("'", validatingObject(), "' must include '", parameter(0), "'"));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> doesNotInclude(final ParameterValue<P> value,
                                                                                          final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !v.includes(value.requireNonNullValue()),
                                                 messageFormatter).ruleContext("MimeTypeRules.doesNotInclude",
                                                                               value);
   }

   @SafeVarargs
   public static <T extends MimeType, P extends MimeType> InvariantRule<T> doesNotInclude(final ParameterValue<P> value,
                                                                                          final String message,
                                                                                          final MessageValue<T>... values) {
      return doesNotInclude(value, DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType, P extends MimeType> InvariantRule<T> doesNotInclude(final ParameterValue<P> value) {
      return doesNotInclude(value,
                            FastStringFormat.of("'",
                                                validatingObject(),
                                                "' must not include '",
                                                parameter(0),
                                                "'"));
   }

   public static <T extends MimeType> InvariantRule<T> isWildcardType(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(MimeType::wildcardType, messageFormatter).ruleContext(
            "MimeTypeRules.isWildcardType");
   }

   @SafeVarargs
   public static <T extends MimeType> InvariantRule<T> isWildcardType(final String message,
                                                                      final MessageValue<T>... values) {
      return isWildcardType(DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType> InvariantRule<T> isWildcardType() {
      return isWildcardType(FastStringFormat.of("'", validatingObject(), "' must be wildcard"));
   }

   public static <T extends MimeType> InvariantRule<T> isNotWildcardType(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(MimeType::wildcardType, messageFormatter).ruleContext(
            "MimeTypeRules.isNotWildcardType");
   }

   @SafeVarargs
   public static <T extends MimeType> InvariantRule<T> isNotWildcardType(final String message,
                                                                         final MessageValue<T>... values) {
      return isNotWildcardType(DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType> InvariantRule<T> isNotWildcardType() {
      return isNotWildcardType(FastStringFormat.of("'", validatingObject(), "' must not be wildcard"));
   }

   public static <T extends MimeType> InvariantRule<T> isWildcardSubtype(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(MimeType::wildcardSubtype, messageFormatter).ruleContext(
            "MimeTypeRules.isWildcardSubtype");
   }

   @SafeVarargs
   public static <T extends MimeType> InvariantRule<T> isWildcardSubtype(final String message,
                                                                         final MessageValue<T>... values) {
      return isWildcardSubtype(DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType> InvariantRule<T> isWildcardSubtype() {
      return isWildcardSubtype(FastStringFormat.of("'", validatingObject(), "' subtype must be wildcard"));
   }

   public static <T extends MimeType> InvariantRule<T> isNotWildcardSubtype(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(MimeType::wildcardSubtype, messageFormatter).ruleContext(
            "MimeTypeRules.isNotWildcardSubtype");
   }

   @SafeVarargs
   public static <T extends MimeType> InvariantRule<T> isNotWildcardSubtype(final String message,
                                                                            final MessageValue<T>... values) {
      return isNotWildcardSubtype(DefaultMessageFormat.of(message, values));
   }

   public static <T extends MimeType> InvariantRule<T> isNotWildcardSubtype() {
      return isNotWildcardSubtype(FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' subtype must not be wildcard"));
   }

}
