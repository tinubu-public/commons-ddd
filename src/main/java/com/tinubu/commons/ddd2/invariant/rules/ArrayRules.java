/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class ArrayRules {

   private ArrayRules() {
   }

   // T[]

   public static <T> InvariantRule<T[]> isEmpty(final MessageFormatter<T[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static <T> InvariantRule<T[]> isEmpty(final String message, final MessageValue<T[]>... values) {
      return isEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T[]> isEmpty() {
      return isEmpty(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static <T> InvariantRule<T[]> isNotEmpty(final MessageFormatter<T[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static <T> InvariantRule<T[]> isNotEmpty(final String message, final MessageValue<T[]>... values) {
      return isNotEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T[]> isNotEmpty() {
      return isNotEmpty(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // byte[]

   public static InvariantRule<byte[]> isEmptyBytes(final MessageFormatter<byte[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<byte[]> isEmptyBytes(final String message,
                                                    final MessageValue<byte[]>... values) {
      return isEmptyBytes(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<byte[]> isEmptyBytes() {
      return isEmptyBytes(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<byte[]> isNotEmptyBytes(final MessageFormatter<byte[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<byte[]> isNotEmptyBytes(final String message,
                                                       final MessageValue<byte[]>... values) {
      return isNotEmptyBytes(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<byte[]> isNotEmptyBytes() {
      return isNotEmptyBytes(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // char[]

   public static InvariantRule<char[]> isEmptyChars(final MessageFormatter<char[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<char[]> isEmptyChars(final String message,
                                                    final MessageValue<char[]>... values) {
      return isEmptyChars(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<char[]> isEmptyChars() {
      return isEmptyChars(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<char[]> isNotEmptyChars(final MessageFormatter<char[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<char[]> isNotEmptyChars(final String message,
                                                       final MessageValue<char[]>... values) {
      return isNotEmptyChars(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<char[]> isNotEmptyChars() {
      return isNotEmptyChars(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // float[]

   public static InvariantRule<float[]> isEmptyFloats(final MessageFormatter<float[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<float[]> isEmptyFloats(final String message,
                                                      final MessageValue<float[]>... values) {
      return isEmptyFloats(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<float[]> isEmptyFloats() {
      return isEmptyFloats(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<float[]> isNotEmptyFloats(final MessageFormatter<float[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<float[]> isNotEmptyFloats(final String message,
                                                         final MessageValue<float[]>... values) {
      return isNotEmptyFloats(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<float[]> isNotEmptyFloats() {
      return isNotEmptyFloats(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // double[]

   public static InvariantRule<double[]> isEmptyDoubles(final MessageFormatter<double[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<double[]> isEmptyDoubles(final String message,
                                                        final MessageValue<double[]>... values) {
      return isEmptyDoubles(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<double[]> isEmptyDoubles() {
      return isEmptyDoubles(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<double[]> isNotEmptyDoubles(final MessageFormatter<double[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<double[]> isNotEmptyDoubles(final String message,
                                                           final MessageValue<double[]>... values) {
      return isNotEmptyDoubles(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<double[]> isNotEmptyDoubles() {
      return isNotEmptyDoubles(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // int[]

   public static InvariantRule<int[]> isEmptyInts(final MessageFormatter<int[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<int[]> isEmptyInts(final String message, final MessageValue<int[]>... values) {
      return isEmptyInts(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<int[]> isEmptyInts() {
      return isEmptyInts(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<int[]> isNotEmptyInts(final MessageFormatter<int[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<int[]> isNotEmptyInts(final String message,
                                                     final MessageValue<int[]>... values) {
      return isNotEmptyInts(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<int[]> isNotEmptyInts() {
      return isNotEmptyInts(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // long[]

   public static InvariantRule<long[]> isEmptyLongs(final MessageFormatter<long[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<long[]> isEmptyLongs(final String message,
                                                    final MessageValue<long[]>... values) {
      return isEmptyLongs(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<long[]> isEmptyLongs() {
      return isEmptyLongs(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<long[]> isNotEmptyLongs(final MessageFormatter<long[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<long[]> isNotEmptyLongs(final String message,
                                                       final MessageValue<long[]>... values) {
      return isNotEmptyLongs(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<long[]> isNotEmptyLongs() {
      return isNotEmptyLongs(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // short[]

   public static InvariantRule<short[]> isEmptyShorts(final MessageFormatter<short[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<short[]> isEmptyShorts(final String message,
                                                      final MessageValue<short[]>... values) {
      return isEmptyShorts(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<short[]> isEmptyShorts() {
      return isEmptyShorts(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<short[]> isNotEmptyShorts(final MessageFormatter<short[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<short[]> isNotEmptyShorts(final String message,
                                                         final MessageValue<short[]>... values) {
      return isNotEmptyShorts(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<short[]> isNotEmptyShorts() {
      return isNotEmptyShorts(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   // boolean[]

   public static InvariantRule<boolean[]> isEmptyBooleans(final MessageFormatter<boolean[]> messageFormatter) {
      return satisfiesValue(array -> array.length == 0, messageFormatter).ruleContext("ArrayRules.isEmpty");
   }

   @SafeVarargs
   public static InvariantRule<boolean[]> isEmptyBooleans(final String message,
                                                          final MessageValue<boolean[]>... values) {
      return isEmptyBooleans(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<boolean[]> isEmptyBooleans() {
      return isEmptyBooleans(FastStringFormat.of("'", validatingObjectName(), "' must be empty"));
   }

   public static InvariantRule<boolean[]> isNotEmptyBooleans(final MessageFormatter<boolean[]> messageFormatter) {
      return satisfiesValue(array -> array.length != 0,
                            messageFormatter).ruleContext("ArrayRules.isNotEmpty");
   }

   @SafeVarargs
   public static InvariantRule<boolean[]> isNotEmptyBooleans(final String message,
                                                             final MessageValue<boolean[]>... values) {
      return isNotEmptyBooleans(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<boolean[]> isNotEmptyBooleans() {
      return isNotEmptyBooleans(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   /**
    * Maps specified invariant rule to the validating object array, converted to a collection.
    *
    * @param rule invariant rule to apply to collection-converted array
    * @param <T> validating object type, must be an array
    *
    * @return invariant rule
    *
    * @deprecated Use list or *list instead, because of rule is not genericized with this method
    */
   @Deprecated
   public static <T> InvariantRule<T> collection(final InvariantRule<? super Collection<?>> rule) {
      return as(ArrayRules::arrayToList, rule);
   }

   /**
    * Maps specified invariant rule to the validating Object array, converted to a list. You must use
    * specialized rule mappers for primitive arrays.
    *
    * @param rule invariant rule to apply to list-converted array
    * @param <T> validating object type, must be an array
    *
    * @return invariant rule
    */
   public static <T> InvariantRule<T[]> list(final InvariantRule<? super List<T>> rule) {
      return as(Arrays::asList, rule);
   }

   public static InvariantRule<byte[]> byteList(final InvariantRule<? super List<Byte>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<char[]> charList(final InvariantRule<? super List<Character>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<float[]> floatList(final InvariantRule<? super List<Float>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<double[]> doubleList(final InvariantRule<? super List<Double>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<int[]> intList(final InvariantRule<? super List<Integer>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<long[]> longList(final InvariantRule<? super List<Long>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<short[]> shortList(final InvariantRule<? super List<Short>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static InvariantRule<boolean[]> booleanList(final InvariantRule<? super List<Boolean>> rule) {
      return as(array -> Arrays.asList(ArrayUtils.toObject(array)), rule);
   }

   public static <T> InvariantRule<T[]> length(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<byte[]> byteLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<char[]> charLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<float[]> floatLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<double[]> doubleLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<int[]> intLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<long[]> longLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<short[]> shortLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   public static InvariantRule<boolean[]> booleanLength(final InvariantRule<? super Integer> rule) {
      return property(array -> array.length, "length", rule);
   }

   /**
    * Converts array of any type, including primitives, to a {@link Collection} of array elements.
    *
    * @param array array to convert
    * @param <T> array type
    *
    * @return collection
    *
    * @deprecated only used by deprecated method
    */
   @Deprecated
   private static <T> List<?> arrayToList(T array) {
      notNull(array, "array");

      if (array instanceof byte[]) {
         return Arrays.asList(ArrayUtils.toObject((byte[]) array));
      } else if (array instanceof char[]) {
         return Arrays.asList(ArrayUtils.toObject((char[]) array));
      } else if (array instanceof float[]) {
         return Arrays.asList(ArrayUtils.toObject((float[]) array));
      } else if (array instanceof double[]) {
         return Arrays.asList(ArrayUtils.toObject((double[]) array));
      } else if (array instanceof int[]) {
         return Arrays.asList(ArrayUtils.toObject((int[]) array));
      } else if (array instanceof long[]) {
         return Arrays.asList(ArrayUtils.toObject((long[]) array));
      } else if (array instanceof short[]) {
         return Arrays.asList(ArrayUtils.toObject((short[]) array));
      } else if (array instanceof boolean[]) {
         return Arrays.asList(ArrayUtils.toObject((boolean[]) array));
      } else if (array instanceof Object[]) {
         return Arrays.asList((Object[]) array);
      } else {
         throw new IllegalArgumentException("value is not an array : " + array);
      }
   }

}