/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.function.Predicate;

import com.tinubu.commons.lang.validation.CheckReturnValue;

/**
 * Holds validating object for an {@link InvariantRule}. Also holds the validation context during invariant
 * rule evaluation.
 *
 * @param <T> validating object type
 *
 * @implSpec Mutable class implementation to handle validation context (results, rule name, ...)
 */
public class ValidatingObject<T> {
   private final T value;
   private final Object initialValue;
   private final String name;
   private final String initialName;
   private List<Object> validationResults;
   private String ruleName;
   private List<ParameterValue<?>> ruleParameters;

   @SuppressWarnings("StringEquality")
   private ValidatingObject(T value, Object initialValue, String name, String initialName) {
      this.value = value;
      this.initialValue = initialValue;
      this.name = name == null ? null : notBlank(name, "name");
      this.initialName = initialName == null
                         ? null
                         : (initialName == name ? initialName : notBlank(initialName, "initialName"));
      this.validationResults = list();
      this.ruleName = null;
      this.ruleParameters = emptyList();
   }

   public static <T> ValidatingObject<T> validatingObject(T value) {
      return new ValidatingObject<>(value, value, null, null);
   }

   public static <T> ValidatingObject<T> validatingObject(T value, String name) {
      return new ValidatingObject<>(value, value, name, name);
   }

   public static <T> ValidatingObject<T> validatingObject(T value,
                                                          Object initialValue,
                                                          String name,
                                                          String initialName) {
      return new ValidatingObject<>(value, initialValue, name, initialName);
   }

   /**
    * Provides inlined validation of a single invariant rule. You must operate the returned result, either
    * with {@link InvariantResult#orThrow()}, {@link InvariantResult#defer(InvariantResults)} or other, for
    * the validation to be effective.
    *
    * @param invariantRule invariant rule to evaluate
    *
    * @return invariant rule validation result
    */
   @CheckReturnValue
   public InvariantResult<T> validate(InvariantRule<T> invariantRule) {
      notNull(invariantRule, "invariantRule");

      return Invariant.of(() -> value, invariantRule).objectName(name).validate();
   }

   /**
    * Maps this validating object applying value and name mappers.
    *
    * @param valueMapper value mapper
    * @param nameMapper name mapper
    * @param <V> mapped value type
    *
    * @return mapped validating object
    */
   public <V> ValidatingObject<V> map(Function<? super T, ? extends V> valueMapper,
                                      Function<? super String, String> nameMapper) {
      notNull(valueMapper, "valueMapper");
      notNull(nameMapper, "nameMapper");

      return validatingObject(valueMapper.apply(value), initialValue, nameMapper.apply(name), initialName);
   }

   /**
    * Evaluates invariant rule as a predicate on specified validating object.
    *
    * @param invariantRule invariant rule to evaluate
    *
    * @return predicate evaluation result
    */
   public boolean satisfies(Predicate<T> invariantRule) {
      notNull(invariantRule, "invariantRule");

      return invariantRule.test(value);
   }

   /** Current validating object value for applied rule. */
   public T value() {
      return value;
   }

   /** Initial validating object value, before any mapping value, for applied rule. */
   public Object initialValue() {
      return initialValue;
   }

   /** Optional current validating object name for applied rule. */
   public Optional<String> name() {
      return nullable(name);
   }

   /** Optional initial validating object name, before any mapping value, for applied rule. */
   public Optional<String> initialName() {
      return nullable(initialName);
   }

   /** Optional values resulting from computations during object validation. */
   public List<Object> validationResults() {
      return validationResults;
   }

   public ValidatingObject<T> validationResults(List<Object> validationResults) {
      this.validationResults = notNull(validationResults, "validationResults");
      return this;
   }

   public ValidatingObject<T> addValidationResults(List<Object> validationResults) {
      this.validationResults.addAll(notNull(validationResults, "validationResults"));
      return this;
   }

   public ValidatingObject<T> addValidationResults(Object... validationResults) {
      return addValidationResults(Arrays.asList(notNull(validationResults, "validationResults")));
   }

   /** Optional rule name. */
   public Optional<String> ruleName() {
      return nullable(ruleName);
   }

   public ValidatingObject<T> ruleName(String ruleName) {
      this.ruleName = notBlank(ruleName, "ruleName");

      return this;
   }

   /** Optional rule parameters. */
   public List<ParameterValue<?>> ruleParameters() {
      return ruleParameters;
   }

   public ValidatingObject<T> ruleParameters(List<ParameterValue<?>> ruleParameters) {
      this.ruleParameters = immutable(noNullElements(ruleParameters, "ruleParameters"));

      return this;
   }

   public ValidatingObject<T> ruleParameters(ParameterValue<?>... ruleParameters) {
      return ruleParameters(Arrays.asList(noNullElements(ruleParameters, "ruleParameters")));
   }

   public ValidatingObject<T> resetRuleContext() {
      this.ruleName = null;
      this.ruleParameters = emptyList();
      this.validationResults = list();

      return this;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      ValidatingObject<?> that = (ValidatingObject<?>) o;
      return Objects.equals(value, that.value)
             && Objects.equals(initialValue, that.initialValue)
             && Objects.equals(name, that.name)
             && Objects.equals(initialName, that.initialName)
             && Objects.equals(validationResults, that.validationResults)
             && Objects.equals(ruleName, that.ruleName)
             && Objects.equals(ruleParameters, that.ruleParameters);
   }

   @Override
   public int hashCode() {
      return Objects.hash(value, initialValue, name, initialName, validationResults);
   }

   @Override
   public String toString() {
      return new StringJoiner(",", ValidatingObject.class.getSimpleName() + "[", "]")
            .add("value=" + value)
            .add("initialValue=" + initialValue)
            .add("name='" + name + "'")
            .add("initialName='" + initialName + "'")
            .add("validationResults=" + validationResults)
            .add("ruleName=" + ruleName)
            .add("ruleParameters=" + ruleParameters)
            .toString();
   }

}
