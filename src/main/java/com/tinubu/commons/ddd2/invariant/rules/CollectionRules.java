/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validationResult;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfies;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.singletonList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.StreamSupport;

import com.tinubu.commons.ddd2.invariant.InvariantResult;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.ValidatingObject;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;

// FIXME test rule mappers
// FIXME introduce rules for Streams
// FIXME Throws error if an element in a Collection/Iterable/Stream is null ?
public class CollectionRules {

   private static final IterableValueFormatter ITERABLE_VALUE_FORMATTER = new IterableValueFormatter();

   private CollectionRules() {
   }

   public static <T extends Collection<?>> InvariantRule<T> isEmpty(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Collection::isEmpty, messageFormatter).ruleContext("CollectionRules.isEmpty");
   }

   @SafeVarargs
   public static <T extends Collection<?>> InvariantRule<T> isEmpty(final String message,
                                                                    final MessageValue<T>... values) {
      return isEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Collection<?>> InvariantRule<T> isEmpty() {
      return isEmpty(FastStringFormat.of("'", validatingObject(ITERABLE_VALUE_FORMATTER), "' must be empty"));
   }

   public static <T extends Collection<?>> InvariantRule<T> isNotEmpty(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Collection::isEmpty,
                               messageFormatter).ruleContext("CollectionRules.isNotEmpty");
   }

   @SafeVarargs
   public static <T extends Collection<?>> InvariantRule<T> isNotEmpty(final String message,
                                                                       final MessageValue<T>... values) {
      return isNotEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Collection<?>> InvariantRule<T> isNotEmpty() {
      return isNotEmpty(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> hasNoNullElements(final MessageFormatter<T> messageFormatter) {
      return allSatisfies(isNotNull(), messageFormatter);
   }

   @SafeVarargs
   public static <T extends Iterable<U>, U> InvariantRule<T> hasNoNullElements(final String message,
                                                                               final MessageValue<T>... values) {
      return hasNoNullElements(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> hasNoNullElements() {
      return allSatisfies(isNotNull());
   }

   /**
    * Blank elements are {@link String} that are empty or containing only blank characters. Elements must not
    * be {@code null} either.
    */
   public static <T extends Iterable<U>, U extends CharSequence> InvariantRule<T> hasNoBlankElements(final MessageFormatter<T> messageFormatter) {
      return allSatisfies(isNotBlank(), messageFormatter);
   }

   /**
    * Blank elements are {@link String} that are empty or containing only blank characters. Elements must not
    * be {@code null} either.
    */
   @SafeVarargs
   public static <T extends Iterable<U>, U extends CharSequence> InvariantRule<T> hasNoBlankElements(final String message,
                                                                                                     final MessageValue<T>... values) {
      return hasNoBlankElements(DefaultMessageFormat.of(message, values));
   }

   /**
    * Blank elements are {@link String} that are empty or containing only blank characters. Elements must not
    * be {@code null} either.
    */
   public static <T extends Iterable<U>, U extends CharSequence> InvariantRule<T> hasNoBlankElements() {
      return allSatisfies(isNotBlank());
   }

   public static <T extends Iterable<U>, U, V> InvariantRule<T> hasNoDuplicates(final Function<? super U, ? extends V> classifierMapper,
                                                                                final MessageFormatter<T> messageFormatter) {
      return satisfies(v -> {
         List<Entry<? extends V, List<U>>> duplicates = StreamSupport
               .stream(v.value().spliterator(), false)
               .collect(groupingBy(classifierMapper, toList()))
               .entrySet()
               .stream()
               .filter(e -> e.getValue().size() > 1)
               .collect(toList());

         v.addValidationResults(duplicates);

         return duplicates.isEmpty();
      }, messageFormatter);
   }

   @SafeVarargs
   public static <T extends Iterable<U>, U, V> InvariantRule<T> hasNoDuplicates(final Function<? super U, ? extends V> classifierMapper,
                                                                                final String message,
                                                                                final MessageValue<T>... values) {
      return hasNoDuplicates(classifierMapper, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<U>, U, V> InvariantRule<T> hasNoDuplicates(final Function<? super U, ? extends V> classifierMapper) {
      return hasNoDuplicates(classifierMapper,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' has duplicates : ",
                                                 validationResult(0, messageDuplicates(), "{}")));
   }

   private static <U, V> Function<List<Entry<? extends V, List<U>>>, String> messageDuplicates() {
      return (List<Entry<? extends V, List<U>>> classifiersDuplicates) -> classifiersDuplicates
            .stream()
            .map(classifierDuplicates -> classifierDuplicates.getKey() + "->" + classifierDuplicates
                  .getValue()
                  .stream()
                  .map(Object::toString)
                  .collect(joining(",", "[", "]")))
            .collect(joining(",", "{", "}"));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> hasNoDuplicates(final MessageFormatter<T> messageFormatter) {
      return hasNoDuplicates(identity(), messageFormatter);
   }

   @SafeVarargs
   public static <T extends Iterable<U>, U> InvariantRule<T> hasNoDuplicates(final String message,
                                                                             final MessageValue<T>... values) {
      return hasNoDuplicates(identity(), message, values);
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> hasNoDuplicates() {
      return hasNoDuplicates(identity());
   }

   public static <T extends Iterable<?>> InvariantRule<T> containsAll(final ParameterValue<Iterable<?>> items,
                                                                      final MessageFormatter<T> messageFormatter) {
      notNull(items, "items");

      return satisfies(v -> containsAll(v, items.requireNonNullValue()),
                                            messageFormatter).ruleContext("CollectionRules.containsAll",
                                                                          items);
   }

   @SafeVarargs
   public static <T extends Iterable<?>> InvariantRule<T> containsAll(final ParameterValue<Iterable<?>> items,
                                                                      final String message,
                                                                      final MessageValue<T>... values) {
      return containsAll(items, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<?>> InvariantRule<T> containsAll(final ParameterValue<Iterable<?>> items) {
      return containsAll(items,
                         FastStringFormat.of("'",
                                             validatingObject(ITERABLE_VALUE_FORMATTER),
                                             "' must contain ",
                                             parameter(0, ITERABLE_VALUE_FORMATTER)));
   }

   public static <T extends Iterable<?>> InvariantRule<T> contains(final ParameterValue<?> item,
                                                                   final MessageFormatter<T> messageFormatter) {
      notNull(item, "item");

      return satisfies(v -> containsAll(v, singletonList(item.value())), messageFormatter).ruleContext(
            "CollectionRules.contains",
            item);
   }

   @SafeVarargs
   public static <T extends Iterable<?>> InvariantRule<T> contains(final ParameterValue<?> item,
                                                                   final String message,
                                                                   final MessageValue<T>... values) {
      return contains(item, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<?>> InvariantRule<T> contains(final ParameterValue<?> item) {
      return contains(item,
                      FastStringFormat.of("'",
                                          validatingObject(ITERABLE_VALUE_FORMATTER),
                                          "' must contain '",
                                          parameter(0),
                                          "'"));
   }

   public static <T extends Iterable<?>> InvariantRule<T> containsExactly(final ParameterValue<Iterable<?>> items,
                                                                          final MessageFormatter<T> messageFormatter) {
      notNull(items, "items");

      return satisfies(v -> containsExactly(v, items.requireNonNullValue()),
                                            messageFormatter).ruleContext("CollectionRules.containsExactly",
                                                                          items);
   }

   @SafeVarargs
   public static <T extends Iterable<?>> InvariantRule<T> containsExactly(final ParameterValue<Iterable<?>> items,
                                                                          final String message,
                                                                          final MessageValue<T>... values) {
      return containsExactly(items, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<?>> InvariantRule<T> containsExactly(final ParameterValue<Iterable<?>> items) {
      return containsExactly(items,
                             FastStringFormat.of("'",
                                                 validatingObject(ITERABLE_VALUE_FORMATTER),
                                                 "' must contain exactly ",
                                                 parameter(0, ITERABLE_VALUE_FORMATTER)));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> allSatisfies(final InvariantRule<? super U> rule,
                                                                          MessageFormatter<T> messageFormatter) {
      notNull(rule, "rule");

      return satisfies(v -> allElementsSatisfy(v, rule), messageFormatter).ruleContext(
            "CollectionRules.allSatisfies");
   }

   @SafeVarargs
   public static <T extends Iterable<U>, U> InvariantRule<T> allSatisfies(final InvariantRule<? super U> rule,
                                                                          final String message,
                                                                          final MessageValue<T>... values) {
      return allSatisfies(rule, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> allSatisfies(final InvariantRule<? super U> rule) {
      return allSatisfies(rule,
                          FastStringFormat.of("'",
                                              validatingObject(ITERABLE_VALUE_FORMATTER),
                                              "' > ",
                                              validationResult(1,
                                                               (Function<InvariantResult<?>, String>) InvariantResult::message,
                                                               "all elements must satisfy rule")));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> anySatisfies(final InvariantRule<? super U> rule,
                                                                          MessageFormatter<T> messageFormatter) {
      notNull(rule, "rule");

      return satisfies(v -> anyElementSatisfies(v, rule), messageFormatter).ruleContext(
            "CollectionRules.anySatisfies");
   }

   @SafeVarargs
   public static <T extends Iterable<U>, U> InvariantRule<T> anySatisfies(final InvariantRule<? super U> rule,
                                                                          final String message,
                                                                          final MessageValue<T>... values) {
      return anySatisfies(rule, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> anySatisfies(final InvariantRule<? super U> rule) {
      return anySatisfies(rule,
                          FastStringFormat.of("'",
                                              validatingObject(ITERABLE_VALUE_FORMATTER),
                                              "' > ",
                                              validationResult(1,
                                                               (Function<InvariantResult<?>, String>) InvariantResult::message,
                                                               "any elements must satisfy rule")));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> noneSatisfies(final InvariantRule<? super U> rule,
                                                                           MessageFormatter<T> messageFormatter) {
      notNull(rule, "rule");

      return satisfies(v -> noElementSatisfies(v, rule), messageFormatter).ruleContext(
            "CollectionRules.noneSatisfies");
   }

   @SafeVarargs
   public static <T extends Iterable<U>, U> InvariantRule<T> noneSatisfies(final InvariantRule<? super U> rule,
                                                                           final String message,
                                                                           final MessageValue<T>... values) {
      return noneSatisfies(rule, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Iterable<U>, U> InvariantRule<T> noneSatisfies(final InvariantRule<? super U> rule) {
      return noneSatisfies(rule,
                           FastStringFormat.of("'",
                                               validatingObject(ITERABLE_VALUE_FORMATTER),
                                               "' > ",
                                               validationResult(1,
                                                                (Function<InvariantResult<?>, String>) InvariantResult::message,
                                                                "no element must satisfy rule")));
   }

   /**
    * Applies an invariant to validating object collection's size.
    *
    * @param rule collection's size invariant
    * @param <T> validating object type
    *
    * @return invariant rule
    */
   public static <T extends Collection<?>> InvariantRule<T> size(final InvariantRule<? super Integer> rule) {
      return property(Collection::size, "size", rule);
   }

   private static <T extends Iterable<U>, U> boolean allElementsSatisfy(final ValidatingObject<T> validatingObject,
                                                                        final InvariantRule<? super U> rule) {
      int i = 0;
      for (final Iterator<U> it = validatingObject.value().iterator(); it.hasNext(); i++) {
         InvariantResult<U> result =
               rule.evaluate(it.next(), validatingObject.name().orElse("") + "[" + i + "]");

         if (!result.success()) {
            validatingObject.addValidationResults(i, result);
            return false;
         }
      }
      validatingObject.addValidationResults(-1, null);
      return true;
   }

   private static <T extends Iterable<U>, U> boolean anyElementSatisfies(final ValidatingObject<T> validatingObject,
                                                                         final InvariantRule<? super U> rule) {
      int i = 0;
      for (final Iterator<U> it = validatingObject.value().iterator(); it.hasNext(); i++) {
         InvariantResult<U> result =
               rule.evaluate(it.next(), validatingObject.name().orElse("") + "[" + i + "]");

         if (result.success()) {
            validatingObject.addValidationResults(i, result);
            return true;
         }
      }
      validatingObject.addValidationResults(-1, null);
      return false;
   }

   private static <T extends Iterable<U>, U> boolean noElementSatisfies(final ValidatingObject<T> validatingObject,
                                                                        final InvariantRule<? super U> rule) {
      int i = 0;
      for (final Iterator<U> it = validatingObject.value().iterator(); it.hasNext(); i++) {
         InvariantResult<U> result =
               rule.evaluate(it.next(), validatingObject.name().orElse("") + "[" + i + "]");

         if (result.success()) {
            validatingObject.addValidationResults(i, result);
            return false;
         }
      }
      validatingObject.addValidationResults(-1, null);
      return true;
   }

   private static <T extends Iterable<?>> boolean containsAll(final ValidatingObject<T> validatingObject,
                                                              final Iterable<?> items) {
      for (Object item : items)
         if (!contains(validatingObject, item)) {
            return false;
         }
      return true;
   }

   private static <T extends Iterable<?>> boolean containsExactly(final ValidatingObject<T> validatingObject,
                                                                  final Iterable<?> items) {
      Iterator<?> itCollection = notNull(validatingObject, "validatingObject").value().iterator();
      Iterator<?> itItems = notNull(items, "items").iterator();

      while (itCollection.hasNext()) {
         if (!itItems.hasNext() || !Objects.equals(itCollection.next(), itItems.next())) {
            return false;
         }
      }
      return !itItems.hasNext();
   }

   private static <T extends Iterable<?>> boolean contains(final ValidatingObject<T> validatingObject,
                                                           final Object item) {
      Iterator<?> it = validatingObject.value().iterator();

      if (item == null) {
         while (it.hasNext()) {
            if (it.next() == null) {
               return true;
            }
         }
      } else {
         while (it.hasNext()) {
            if (item.equals(it.next())) {
               return true;
            }
         }
      }
      return false;
   }

}
