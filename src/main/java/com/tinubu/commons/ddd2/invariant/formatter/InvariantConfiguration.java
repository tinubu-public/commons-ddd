/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.formatter;

import java.util.Locale;
import java.util.function.BiFunction;

import com.tinubu.commons.ddd2.invariant.MessageValue;

/**
 * Invariant configuration singleton used to parameterize framework defaults.
 */
public final class InvariantConfiguration {

   private static final Locale MESSAGE_FORMATTER_DEFAULT_LOCALE = Locale.getDefault(Locale.Category.FORMAT);

   /**
    * Default locale used with default message formatter. By default the default locale for {@link
    * Locale.Category#FORMAT} category will be used.
    */
   private Locale messageFormatterDefaultLocale = MESSAGE_FORMATTER_DEFAULT_LOCALE;

   private InvariantConfiguration() {}

   /**
    * Singleton instance.
    */
   private static final InvariantConfiguration INSTANCE = new InvariantConfiguration();

   /**
    * Retrieves invariant configuration singleton instance.
    *
    * @return invariant configuration singleton instance
    */
   public static InvariantConfiguration instance() {
      return INSTANCE;
   }

   public <T> BiFunction<String, MessageValue<T>[], MessageFormatter<T>> defaultMessageFormatter() {
      return StringFormat::of;
   }

   public Locale messageFormatterDefaultLocale() {
      return messageFormatterDefaultLocale;
   }

   public InvariantConfiguration messageFormatterDefaultLocale(Locale locale) {
      this.messageFormatterDefaultLocale = locale;
      return this;
   }
}
