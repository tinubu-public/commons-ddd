/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Collection;
import java.util.Objects;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;

// FIXME isIn/isNotIn : Throws error if an element in a Collection/Iterable/Stream is null ?
public class EqualsRules {

   private static final IterableValueFormatter ITERABLE_VALUE_FORMATTER = new IterableValueFormatter();

   private EqualsRules() {
   }

   public static <T> InvariantRule<T> isEqualTo(final ParameterValue<?> value,
                                                final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> Objects.equals(v, value.value()),
                                                 messageFormatter).ruleContext("EqualsRules.isEqualTo",
                                                                               value);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isEqualTo(final ParameterValue<?> value,
                                                final String message,
                                                final MessageValue<T>... values) {
      return isEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isEqualTo(final ParameterValue<?> value) {
      return isEqualTo(value,
                       FastStringFormat.of("'",
                                           validatingObject(),
                                           "' must be equal to '",
                                           parameter(0),
                                           "'"));
   }

   public static <T> InvariantRule<T> isNotEqualTo(final ParameterValue<?> value,
                                                   final MessageFormatter<T> messageFormatter) {
      notNull(value, "value");

      return satisfiesValue(v -> !Objects.equals(v, value.value()),
                                                 messageFormatter).ruleContext("EqualsRules.isNotEqualTo",
                                                                               value);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isNotEqualTo(final ParameterValue<?> value,
                                                   final String message,
                                                   final MessageValue<T>... values) {
      return isNotEqualTo(value, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isNotEqualTo(final ParameterValue<?> value) {
      return isNotEqualTo(value,
                          FastStringFormat.of("'",
                                              validatingObject(),
                                              "' must not be equal to '", parameter(0),
                                              "'"));
   }

   public static <T> InvariantRule<T> isIn(final ParameterValue<Collection<? extends T>> collection,
                                           final MessageFormatter<T> messageFormatter) {
      notNull(collection, "collection");

      return satisfiesValue(v -> collection.requireNonNullValue().contains(v),
                                                 messageFormatter).ruleContext("EqualsRules.isIn",
                                                                               collection);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isIn(final ParameterValue<Collection<? extends T>> collection,
                                           final String message,
                                           final MessageValue<T>... values) {

      return isIn(collection, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isIn(final ParameterValue<Collection<? extends T>> collection) {
      return isIn(collection,
                  FastStringFormat.of("'",
                                      validatingObject(),
                                      "' must be in ", parameter(0, ITERABLE_VALUE_FORMATTER)));
   }

   public static <T> InvariantRule<T> isNotIn(final ParameterValue<Collection<? extends T>> collection,
                                              final MessageFormatter<T> messageFormatter) {
      notNull(collection, "collection");

      return notSatisfiesValue(v -> collection.requireNonNullValue().contains(v),
                                                    messageFormatter).ruleContext("EqualsRules.isNotIn",
                                                                                  collection);
   }

   @SafeVarargs
   public static <T> InvariantRule<T> isNotIn(final ParameterValue<Collection<? extends T>> collection,
                                              final String message,
                                              final MessageValue<T>... values) {
      return isNotIn(collection, DefaultMessageFormat.of(message, values));
   }

   public static <T> InvariantRule<T> isNotIn(final ParameterValue<Collection<? extends T>> collection) {
      return isNotIn(collection,
                     FastStringFormat.of("'",
                                         validatingObject(),
                                         "' must not be in ", parameter(0, ITERABLE_VALUE_FORMATTER)));
   }

}
