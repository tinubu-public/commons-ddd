/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

// FIXME test rule mappers
public final class LocaleRules {

   private LocaleRules() {
   }

   public static InvariantRule<Locale> hasLanguage(final MessageFormatter<Locale> messageFormatter) {
      return satisfiesValue(locale -> !(locale.getLanguage() == null
                                                             || StringUtils.isBlank(locale.getLanguage())),
                            messageFormatter).ruleContext("LocaleRules.hasLanguage");
   }

   @SafeVarargs
   public static InvariantRule<Locale> hasLanguage(final String message,
                                                   final MessageValue<Locale>... values) {
      return hasLanguage(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Locale> hasLanguage() {
      return hasLanguage(FastStringFormat.of("'", validatingObject(), "' locale must have a language set"));
   }

   public static InvariantRule<Locale> hasCountry(final MessageFormatter<Locale> messageFormatter) {
      return satisfiesValue(locale -> !(locale.getCountry() == null
                                                             || StringUtils.isBlank(locale.getCountry())),
                            messageFormatter).ruleContext("LocaleRules.hasCountry");
   }

   @SafeVarargs
   public static InvariantRule<Locale> hasCountry(final String message,
                                                  final MessageValue<Locale>... values) {
      return hasCountry(DefaultMessageFormat.of(message, values));
   }

   public static InvariantRule<Locale> hasCountry() {
      return hasCountry(FastStringFormat.of("'", validatingObject(), "' locale must have a country set"));
   }

   /**
    * Alias for {@link #hasLanguage(String, MessageValue...)}.
    *
    * @param messageFormatter validation error message formatter
    *
    * @return invariant rule
    */
   public static InvariantRule<Locale> isNotEmpty(final MessageFormatter<Locale> messageFormatter) {
      return hasLanguage(messageFormatter).orValue(hasCountry(messageFormatter));
   }

   /**
    * Alias for {@link #hasLanguage(String, MessageValue...)}.
    *
    * @param message validation error message
    * @param values validation error message values
    *
    * @return invariant rule
    */
   @SafeVarargs
   public static InvariantRule<Locale> isNotEmpty(final String message,
                                                  final MessageValue<Locale>... values) {
      return isNotEmpty(DefaultMessageFormat.of(message, values));
   }

   /**
    * Alias for {@link #hasLanguage()}.
    *
    * @return invariant rule
    */
   public static InvariantRule<Locale> isNotEmpty() {
      return isNotEmpty(FastStringFormat.of("'",
                                            validatingObject(),
                                            "' locale must have a language or a country set"));
   }

   /**
    * Applies an invariant to validating object locale's language (ISO 639-1 alpha-2).
    *
    * @param rule locale's language invariant
    *
    * @return invariant rule
    */
   public static InvariantRule<Locale> isoAlpha2Language(final InvariantRule<? super String> rule) {
      return hasLanguage().andValue(property(Locale::getLanguage, "language(iso2)", rule));
   }

   /**
    * Applies an invariant to validating object locale's language (ISO 639-2 alpha-3).
    *
    * @param rule locale's language invariant
    *
    * @return invariant rule
    */
   public static InvariantRule<Locale> isoAlpha3Language(final InvariantRule<? super String> rule) {
      return hasLanguage().andValue(property(Locale::getISO3Language, "language(iso3)", rule));
   }

   /**
    * Applies an invariant to validating object locale's country (ISO 3166-1 alpha-2).
    *
    * @param rule locale's country invariant
    *
    * @return invariant rule
    */
   public static InvariantRule<Locale> isoAlpha2Country(final InvariantRule<? super String> rule) {
      return hasCountry().andValue(property(Locale::getCountry, "country(iso2)", rule));
   }

   /**
    * Applies an invariant to validating object locale's country (ISO 3166-1 alpha-3).
    *
    * @param rule locale's country invariant
    *
    * @return invariant rule
    */
   public static InvariantRule<Locale> isoAlpha3Country(final InvariantRule<? super String> rule) {
      return hasCountry().andValue(property(Locale::getISO3Country, "country(iso3)", rule));
   }

}
