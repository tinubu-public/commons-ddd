/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import com.tinubu.commons.ddd2.domain.event.DomainEvent;
import com.tinubu.commons.ddd2.domain.type.BiCompositeId;
import com.tinubu.commons.ddd2.domain.type.Entity;
import com.tinubu.commons.ddd2.domain.type.Id;
import com.tinubu.commons.ddd2.domain.type.SimpleId;
import com.tinubu.commons.ddd2.domain.type.Value;
import com.tinubu.commons.ddd2.domain.type.Version;
import com.tinubu.commons.ddd2.domain.type.VersionedEntity;
import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.lang.util.Pair;

public class DddRules {

   private DddRules() {
   }

   public static class ValueRules {

      private ValueRules() {
      }

      public static <T extends Value> InvariantRule<T> isSameValueAs(final ParameterValue<T> value,
                                                                     final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameValueAs(value.requireNonNullValue()), messageFormatter).ruleContext(
               "DddRules.ValueRules.isSameValueAs",
               value);
      }

      @SafeVarargs
      public static <T extends Value> InvariantRule<T> isSameValueAs(final ParameterValue<T> value,
                                                                     final String message,
                                                                     final MessageValue<T>... values) {
         return isSameValueAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends Value> InvariantRule<T> isSameValueAs(final ParameterValue<T> value) {
         return isSameValueAs(value,
                              FastStringFormat.of("'",
                                                  validatingObject(),
                                                  "' must be same value as '", parameter(0),
                                                  "'"));
      }

      public static <T extends Value> InvariantRule<T> isNotSameValueAs(final ParameterValue<T> value,
                                                                        final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> !v.sameValueAs(value.requireNonNullValue()),
                               messageFormatter).ruleContext("DddRules.ValueRules.isNotSameValueAs", value);
      }

      @SafeVarargs
      public static <T extends Value> InvariantRule<T> isNotSameValueAs(final ParameterValue<T> value,
                                                                        final String message,
                                                                        final MessageValue<T>... values) {
         return isNotSameValueAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends Value> InvariantRule<T> isNotSameValueAs(final ParameterValue<T> value) {
         return isNotSameValueAs(value,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must not be same value as '", parameter(0),
                                                     "'"));
      }

   }

   public static class EntityRules {

      private EntityRules() {}

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isSameIdentityAs(final ParameterValue<T> value,
                                                                                            final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameIdentityAs(value.requireNonNullValue()),
                               messageFormatter).ruleContext("DddRules.EntityRules.isSameIdentityAs", value);
      }

      @SafeVarargs
      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isSameIdentityAs(final ParameterValue<T> value,
                                                                                            final String message,
                                                                                            final MessageValue<T>... values) {
         return isSameIdentityAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isSameIdentityAs(final ParameterValue<T> value) {
         return isSameIdentityAs(value,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must be same identity as '", parameter(0),
                                                     "'"));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isNotSameIdentityAs(final ParameterValue<T> value,
                                                                                               final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> !v.sameIdentityAs(value.requireNonNullValue()),
                               messageFormatter).ruleContext("DddRules.EntityRules.isNotSameIdentityAs",
                                                             value);
      }

      @SafeVarargs
      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isNotSameIdentityAs(final ParameterValue<T> value,
                                                                                               final String message,
                                                                                               final MessageValue<T>... values) {
         return isNotSameIdentityAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isNotSameIdentityAs(final ParameterValue<T> value) {
         return isNotSameIdentityAs(value,
                                    FastStringFormat.of("'",
                                                        validatingObject(),
                                                        "' must not be same identity as '", parameter(0),
                                                        "'"));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isSameValueAs(final ParameterValue<T> value,
                                                                                         final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameValueAs(value.requireNonNullValue()), messageFormatter).ruleContext(
               "DddRules.EntityRules.isSameValueAs",
               value);
      }

      @SafeVarargs
      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isSameValueAs(final ParameterValue<T> value,
                                                                                         final String message,
                                                                                         final MessageValue<T>... values) {
         return isSameValueAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isSameValueAs(final ParameterValue<T> value) {
         return isSameValueAs(value,
                              FastStringFormat.of("'",
                                                  validatingObject(),
                                                  "' must be same value as '", parameter(0),
                                                  "'"));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isNotSameValueAs(final ParameterValue<T> value,
                                                                                            final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameValueAs(value.requireNonNullValue()), messageFormatter).ruleContext(
               "DddRules.EntityRules.isNotSameValueAs",
               value);
      }

      @SafeVarargs
      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isNotSameValueAs(final ParameterValue<T> value,
                                                                                            final String message,
                                                                                            final MessageValue<T>... values) {
         return isNotSameValueAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> isNotSameValueAs(final ParameterValue<T> value) {
         return isNotSameValueAs(value,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must not be same value as '", parameter(0),
                                                     "'"));
      }

      public static <T extends VersionedEntity<ID, V>, V extends Version<?>, ID extends Id> InvariantRule<T> isSameVersionAs(
            final ParameterValue<T> value,
            final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameVersionAs(value.requireNonNullValue()),
                               messageFormatter).ruleContext("DddRules.EntityRules.isSameVersionAs", value);
      }

      @SafeVarargs
      public static <T extends VersionedEntity<ID, V>, V extends Version<?>, ID extends Id> InvariantRule<T> isSameVersionAs(
            final ParameterValue<T> value,
            final String message,
            final MessageValue<T>... values) {
         return isSameVersionAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends VersionedEntity<ID, V>, V extends Version<?>, ID extends Id> InvariantRule<T> isSameVersionAs(
            final ParameterValue<T> value) {
         return isSameVersionAs(value,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' must be same version as '", parameter(0),
                                                    "'"));
      }

      public static <T extends VersionedEntity<ID, V>, V extends Version<?>, ID extends Id> InvariantRule<T> isNotSameVersionAs(
            final ParameterValue<T> value,
            final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameVersionAs(value.requireNonNullValue()),
                               messageFormatter).ruleContext("DddRules.EntityRules.isNotSameVersionAs",
                                                             value);
      }

      @SafeVarargs
      public static <T extends VersionedEntity<ID, V>, V extends Version<?>, ID extends Id> InvariantRule<T> isNotSameVersionAs(
            final ParameterValue<T> value,
            final String message,
            final MessageValue<T>... values) {
         return isNotSameVersionAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends VersionedEntity<ID, V>, V extends Version<?>, ID extends Id> InvariantRule<T> isNotSameVersionAs(
            final ParameterValue<T> value) {
         return isNotSameVersionAs(value,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' must not be same version as '", parameter(0),
                                                       "'"));
      }

      /**
       * Applies an invariant to validating object entity's id.
       *
       * @param rule entity's id invariant
       * @param <T> validating object type
       *
       * @return invariant rule
       */
      public static <T extends Entity<ID>, ID extends Id> InvariantRule<T> id(final InvariantRule<? super ID> rule) {
         return as(Entity::id, rule);
      }

      /**
       * Applies an invariant to validating object versioned entity's version.
       *
       * @param rule versioned entity's version invariant
       * @param <T> validating object type
       *
       * @return invariant rule
       */
      public static <T extends VersionedEntity<ID, V>, ID extends Id, V extends Version<?>> InvariantRule<T> version(
            final InvariantRule<? super V> rule) {
         return as(VersionedEntity::version, rule);
      }

   }

   public static class EventRules {

      private EventRules() {}

      public static <T extends DomainEvent> InvariantRule<T> isSameEventAs(final ParameterValue<T> value,
                                                                           final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameEventAs(value.requireNonNullValue()), messageFormatter).ruleContext(
               "DddRules.EventRules.isSameEventAs",
               value);
      }

      @SafeVarargs
      public static <T extends DomainEvent> InvariantRule<T> isSameEventAs(final ParameterValue<T> value,
                                                                           final String message,
                                                                           final MessageValue<T>... values) {
         return isSameEventAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends DomainEvent> InvariantRule<T> isSameEventAs(final ParameterValue<T> value) {
         return isSameEventAs(value,
                              FastStringFormat.of("'",
                                                  validatingObject(),
                                                  "' must be same event as '", parameter(0),
                                                  "'"));
      }

      public static <T extends DomainEvent> InvariantRule<T> isNotSameEventAs(final ParameterValue<T> value,
                                                                              final MessageFormatter<T> messageFormatter) {
         notNull(value, "value");

         return satisfiesValue(v -> v.sameEventAs(value.requireNonNullValue()), messageFormatter).ruleContext(
               "DddRules.EventRules.isNotSameEventAs",
               value);
      }

      @SafeVarargs
      public static <T extends DomainEvent> InvariantRule<T> isNotSameEventAs(final ParameterValue<T> value,
                                                                              final String message,
                                                                              final MessageValue<T>... values) {
         return isNotSameEventAs(value, DefaultMessageFormat.of(message, values));
      }

      public static <T extends DomainEvent> InvariantRule<T> isNotSameEventAs(final ParameterValue<T> value) {
         return isNotSameEventAs(value,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must not be same event as '", parameter(0),
                                                     "'"));
      }

   }

   public static class IdRules {

      private IdRules() {}

      public static <T extends Id> InvariantRule<T> idStringValue(final InvariantRule<? super String> rule) {
         return as(Id::stringValue, rule);
      }

      public static <T extends SimpleId<U>, U extends Comparable<? super U>> InvariantRule<T> idValue(final InvariantRule<? super U> rule) {
         return as(SimpleId::value, rule);
      }

      public static <T extends BiCompositeId<U1, U2>, U1 extends Comparable<? super U1>, U2 extends Comparable<? super U2>> InvariantRule<T> biCompositeIdValue(
            final InvariantRule<Pair<? super U1, ? super U2>> rule) {
         return as(id -> Pair.of(id.value1(), id.value2()), rule);
      }

   }

   public static class VersionRules {

      private VersionRules() {}

      public static <T extends Version<U>, U extends Comparable<? super U>> InvariantRule<T> versionValue(
            final InvariantRule<? super U> rule) {
         return as(Version::value, rule);
      }

   }

}