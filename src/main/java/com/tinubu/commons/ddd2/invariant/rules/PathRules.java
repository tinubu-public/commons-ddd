/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.parameter;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.notSatisfiesValue;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.property;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.File;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.commons.lang3.StringUtils;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;
import com.tinubu.commons.ddd2.valueformatter.IterableValueFormatter;

// FIXME tests
// FIXME isPosix to use satisfies for ruleName to be applied
public class PathRules {

   private static final FileSystem FILE_SYSTEM = FileSystems.getDefault();
   private static final IterableValueFormatter ITERABLE_VALUE_FORMATTER = new IterableValueFormatter();

   private PathRules() {
   }

   public static <T extends Path> InvariantRule<T> isEmpty(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> path.toString().equals(""), messageFormatter).ruleContext(
            "PathRules.isEmpty");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isEmpty(final String message,
                                                           final MessageValue<T>... values) {
      return isEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isEmpty() {
      return isEmpty(FastStringFormat.of("'", validatingObject(), "' must be empty"));
   }

   public static <T extends Path> InvariantRule<T> isNotEmpty(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(path -> path.toString().equals(""), messageFormatter).ruleContext(
            "PathRules.isNotEmpty");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isNotEmpty(final String message,
                                                              final MessageValue<T>... values) {
      return isNotEmpty(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isNotEmpty() {
      return isNotEmpty(FastStringFormat.of("'", validatingObjectName(), "' must not be empty"));
   }

   public static <T extends Path> InvariantRule<T> isAbsolute(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(Path::isAbsolute, messageFormatter).ruleContext("PathRules.isAbsolute");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isAbsolute(final String message,
                                                              final MessageValue<T>... values) {
      return isAbsolute(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isAbsolute() {
      return isAbsolute(FastStringFormat.of("'", validatingObject(), "' must be absolute path"));
   }

   public static <T extends Path> InvariantRule<T> isNotAbsolute(final MessageFormatter<T> messageFormatter) {
      return notSatisfiesValue(Path::isAbsolute, messageFormatter).ruleContext("PathRules.isNotAbsolute");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isNotAbsolute(final String message,
                                                                 final MessageValue<T>... values) {
      return isNotAbsolute(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isNotAbsolute() {
      return isNotAbsolute(FastStringFormat.of("'", validatingObject(), "' must not be absolute path"));
   }

   public static <T extends Path> InvariantRule<T> hasNoTraversal(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> {
         for (int i = 0; i < path.getNameCount(); i++) {
            if (path.getName(i).toString().equals("..")) {
               return false;
            }
         }
         return true;
      }, messageFormatter).ruleContext("PathRules.hasNoTraversal");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> hasNoTraversal(final String message,
                                                                  final MessageValue<T>... values) {
      return hasNoTraversal(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> hasNoTraversal() {
      return hasNoTraversal(FastStringFormat.of("'", validatingObject(), "' must not have traversal paths"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isEqualTo(final ParameterValue<U> path,
                                                                             final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathEquals(v, path.value(), false), messageFormatter).ruleContext(
            "PathRules.isEqualTo",
            path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> isEqualTo(final ParameterValue<U> path,
                                                                             final String message,
                                                                             final MessageValue<T>... values) {
      return isEqualTo(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isEqualTo(final ParameterValue<U> path) {
      return isEqualTo(path,
                       FastStringFormat.of("'",
                                           validatingObject(),
                                           "' must be equal to '",
                                           parameter(0),
                                           "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isEqualToIgnoreCase(final ParameterValue<U> path,
                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathEquals(v, path.value(), true),
                                                 messageFormatter).ruleContext("PathRules.isEqualToIgnoreCase",
                                                                               path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> isEqualToIgnoreCase(final ParameterValue<U> path,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
      return isEqualToIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isEqualToIgnoreCase(final ParameterValue<U> path) {
      return isEqualToIgnoreCase(path,
                                 FastStringFormat.of("'",
                                                     validatingObject(),
                                                     "' must be equal to '",
                                                     parameter(0),
                                                     "' (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isNotEqualTo(final ParameterValue<U> path,
                                                                                final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathEquals(v, path.value(), false),
                                                 messageFormatter).ruleContext("PathRules.isNotEqualTo",
                                                                               path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> isNotEqualTo(final ParameterValue<U> path,
                                                                                final String message,
                                                                                final MessageValue<T>... values) {
      return isNotEqualTo(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isNotEqualTo(final ParameterValue<U> path) {
      return isNotEqualTo(path,
                          FastStringFormat.of("'",
                                              validatingObject(),
                                              "' must not be equal to '",
                                              parameter(0),
                                              "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isNotEqualToIgnoreCase(final ParameterValue<U> path,
                                                                                          final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathEquals(v, path.value(), true),
                                                 messageFormatter).ruleContext(
            "PathRules.isNotEqualToIgnoreCase", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> isNotEqualToIgnoreCase(final ParameterValue<U> path,
                                                                                          final String message,
                                                                                          final MessageValue<T>... values) {
      return isNotEqualToIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> isNotEqualToIgnoreCase(final ParameterValue<U> path) {
      return isNotEqualToIgnoreCase(path,
                                    FastStringFormat.of("'",
                                                        validatingObject(),
                                                        "' must not be equal to '",
                                                        parameter(0),
                                                        "' (case-insensitive)"));
   }

   public static <T extends Path> InvariantRule<T> isIn(final ParameterValue<Collection<? extends T>> collection,
                                                        final MessageFormatter<T> messageFormatter) {
      notNull(collection, "collection");

      return satisfiesValue(v -> collection
            .requireNonNullValue()
            .stream()
            .anyMatch(path -> pathEquals(v, path, false)), messageFormatter).ruleContext("PathRules.isIn",
                                                                                         collection);
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isIn(final ParameterValue<Collection<? extends T>> collection,
                                                        final String message,
                                                        final MessageValue<T>... values) {
      return isIn(collection, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isIn(final ParameterValue<Collection<? extends T>> collection) {
      return isIn(collection,
                  FastStringFormat.of("'",
                                      validatingObject(),
                                      "' must be in ", parameter(0, ITERABLE_VALUE_FORMATTER)));
   }

   public static <T extends Path> InvariantRule<T> isInIgnoreCase(final ParameterValue<Collection<? extends T>> collection,
                                                                  final MessageFormatter<T> messageFormatter) {
      notNull(collection, "collection");

      return satisfiesValue(v -> collection
            .requireNonNullValue()
            .stream()
            .anyMatch(path -> pathEquals(v, path, true)), messageFormatter).ruleContext(
            "PathRules.isInIgnoreCase", collection);
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isInIgnoreCase(final ParameterValue<Collection<? extends T>> collection,
                                                                  final String message,
                                                                  final MessageValue<T>... values) {
      return isInIgnoreCase(collection, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isInIgnoreCase(final ParameterValue<Collection<? extends T>> collection) {
      return isInIgnoreCase(collection,
                            FastStringFormat.of("'",
                                                validatingObject(),
                                                "' must be in ", parameter(0, ITERABLE_VALUE_FORMATTER),
                                                " (case-insensitive)"));
   }

   public static <T extends Path> InvariantRule<T> isNotIn(final ParameterValue<Collection<? extends T>> collection,
                                                           final MessageFormatter<T> messageFormatter) {
      notNull(collection, "collection");

      return satisfiesValue(v -> collection
            .requireNonNullValue()
            .stream()
            .noneMatch(path -> pathEquals(v, path, false)), messageFormatter).ruleContext("PathRules.isNotIn",
                                                                                          collection);
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isNotIn(final ParameterValue<Collection<? extends T>> collection,
                                                           final String message,
                                                           final MessageValue<T>... values) {
      return isNotIn(collection, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isNotIn(final ParameterValue<Collection<? extends T>> collection) {
      return isNotIn(collection,
                     FastStringFormat.of("'",
                                         validatingObject(),
                                         "' must not be in ", parameter(0, ITERABLE_VALUE_FORMATTER)));
   }

   public static <T extends Path> InvariantRule<T> isNotInIgnoreCase(final ParameterValue<Collection<? extends T>> collection,
                                                                     final MessageFormatter<T> messageFormatter) {
      notNull(collection, "collection");

      return satisfiesValue(v -> collection
            .requireNonNullValue()
            .stream()
            .noneMatch(path -> pathEquals(v, path, true)), messageFormatter).ruleContext(
            "PathRules.isNotInIgnoreCase", collection);
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isNotInIgnoreCase(final ParameterValue<Collection<? extends T>> collection,
                                                                     final String message,
                                                                     final MessageValue<T>... values) {
      return isNotInIgnoreCase(collection, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isNotInIgnoreCase(final ParameterValue<Collection<? extends T>> collection) {
      return isNotInIgnoreCase(collection,
                               FastStringFormat.of("'",
                                                   validatingObject(),
                                                   "' must not be in ",
                                                   parameter(0, ITERABLE_VALUE_FORMATTER),
                                                   " (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> contains(final ParameterValue<U> path,
                                                                            final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathContains(v, path.value(), false), messageFormatter).ruleContext(
            "PathRules.contains",
            path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> contains(final ParameterValue<U> path,
                                                                            final String message,
                                                                            final MessageValue<T>... values) {
      return contains(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> contains(final ParameterValue<U> path) {
      return contains(path,
                      FastStringFormat.of("'", validatingObject(), "' must contain '", parameter(0), "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> containsIgnoreCase(final ParameterValue<U> path,
                                                                                      final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathContains(v, path.value(), true),
                                                 messageFormatter).ruleContext("PathRules.containsIgnoreCase",
                                                                               path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> containsIgnoreCase(final ParameterValue<U> path,
                                                                                      final String message,
                                                                                      final MessageValue<T>... values) {
      return containsIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> containsIgnoreCase(final ParameterValue<U> path) {
      return containsIgnoreCase(path,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' must contain '",
                                                    parameter(0),
                                                    "' (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notContains(final ParameterValue<U> path,
                                                                               final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathContains(v, path.value(), false),
                                                 messageFormatter).ruleContext("PathRules.notContains", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> notContains(final ParameterValue<U> path,
                                                                               final String message,
                                                                               final MessageValue<T>... values) {
      return notContains(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notContains(final ParameterValue<U> path) {
      return notContains(path,
                         FastStringFormat.of("'",
                                             validatingObject(),
                                             "' must not contain '",
                                             parameter(0),
                                             "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notContainsIgnoreCase(final ParameterValue<U> path,
                                                                                         final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathContains(v, path.value(), true),
                                                 messageFormatter).ruleContext(
            "PathRules.notContainsIgnoreCase", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> notContainsIgnoreCase(final ParameterValue<U> path,
                                                                                         final String message,
                                                                                         final MessageValue<T>... values) {
      return notContainsIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notContainsIgnoreCase(final ParameterValue<U> path) {
      return notContainsIgnoreCase(path,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' must not contain '",
                                                       parameter(0),
                                                       "' (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> startsWith(final ParameterValue<U> path,
                                                                              final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathStartWith(v, path.value(), false), messageFormatter).ruleContext(
            "PathRules.startWith",
            path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> startsWith(final ParameterValue<U> path,
                                                                              final String message,
                                                                              final MessageValue<T>... values) {
      return startsWith(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> startsWith(final ParameterValue<U> path) {
      return startsWith(path,
                        FastStringFormat.of("'",
                                            validatingObject(),
                                            "' must start with '",
                                            parameter(0),
                                            "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> startsWithIgnoreCase(final ParameterValue<U> path,
                                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathStartWith(v, path.value(), true),
                                                 messageFormatter).ruleContext(
            "PathRules.startsWithIgnoreCase", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> startsWithIgnoreCase(final ParameterValue<U> path,
                                                                                        final String message,
                                                                                        final MessageValue<T>... values) {
      return startsWithIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> startsWithIgnoreCase(final ParameterValue<U> path) {
      return startsWithIgnoreCase(path,
                                  FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' must start with '",
                                                      parameter(0),
                                                      "' (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notStartsWith(final ParameterValue<U> path,
                                                                                 final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !v.startsWith(path.value()),
                                                 messageFormatter).ruleContext("PathRules.notStartsWith",
                                                                               path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> notStartsWith(final ParameterValue<U> path,
                                                                                 final String message,
                                                                                 final MessageValue<T>... values) {
      return notStartsWith(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notStartsWith(final ParameterValue<U> path) {
      return notStartsWith(path,
                           FastStringFormat.of("'",
                                               validatingObject(),
                                               "' must not start with '",
                                               parameter(0),
                                               "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notStartsWithIgnoreCase(final ParameterValue<U> path,
                                                                                           final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathStartWith(v, path.value(), true),
                                                 messageFormatter).ruleContext(
            "PathRules.notStartsWithIgnoreCase", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> notStartsWithIgnoreCase(final ParameterValue<U> path,
                                                                                           final String message,
                                                                                           final MessageValue<T>... values) {
      return notStartsWithIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notStartsWithIgnoreCase(final ParameterValue<U> path) {
      return notStartsWithIgnoreCase(path,
                                     FastStringFormat.of("'",
                                                         validatingObject(),
                                                         "' must not start with '",
                                                         parameter(0),
                                                         "' (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> endsWith(final ParameterValue<U> path,
                                                                            final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathEndWith(v, path.value(), false), messageFormatter).ruleContext(
            "PathRules.endsWith",
            path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> endsWith(final ParameterValue<U> path,
                                                                            final String message,
                                                                            final MessageValue<T>... values) {
      return endsWith(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> endsWith(final ParameterValue<U> path) {
      return endsWith(path,
                      FastStringFormat.of("'", validatingObject(), "' must end with '", parameter(0), "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> endsWithIgnoreCase(final ParameterValue<U> path,
                                                                                      final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> pathEndWith(v, path.value(), true),
                                                 messageFormatter).ruleContext("PathRules.endsWithIgnoreCase",
                                                                               path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> endsWithIgnoreCase(final ParameterValue<U> path,
                                                                                      final String message,
                                                                                      final MessageValue<T>... values) {
      return endsWithIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> endsWithIgnoreCase(final ParameterValue<U> path) {
      return endsWithIgnoreCase(path,
                                FastStringFormat.of("'",
                                                    validatingObject(),
                                                    "' must end with '",
                                                    parameter(0),
                                                    "' (case-insensitive)"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notEndsWith(final ParameterValue<U> path,
                                                                               final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathEndWith(v, path.value(), false),
                                                 messageFormatter).ruleContext("PathRules.notEndsWith", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> notEndsWith(final ParameterValue<U> path,
                                                                               final String message,
                                                                               final MessageValue<T>... values) {
      return notEndsWith(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notEndsWith(final ParameterValue<U> path) {
      return notEndsWith(path,
                         FastStringFormat.of("'",
                                             validatingObject(),
                                             "' must not end with '",
                                             parameter(0),
                                             "'"));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notEndsWithIgnoreCase(final ParameterValue<U> path,
                                                                                         final MessageFormatter<T> messageFormatter) {
      notNull(path, "path");

      return satisfiesValue(v -> !pathEndWith(v, path.value(), true),
                                                 messageFormatter).ruleContext(
            "PathRules.notEndsWithIgnoreCase", path);
   }

   @SafeVarargs
   public static <T extends Path, U extends Path> InvariantRule<T> notEndsWithIgnoreCase(final ParameterValue<U> path,
                                                                                         final String message,
                                                                                         final MessageValue<T>... values) {
      return notEndsWithIgnoreCase(path, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends Path> InvariantRule<T> notEndsWithIgnoreCase(final ParameterValue<U> path) {
      return notEndsWithIgnoreCase(path,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' must not end with '",
                                                       parameter(0),
                                                       "' (case-insensitive)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matches(final ParameterValue<U> pattern,
                                                                                   final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> FILE_SYSTEM
            .getPathMatcher(pattern.requireNonNullValue().toString()).matches(v),
                            messageFormatter).ruleContext("PathRules.matches", pattern);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> matches(final ParameterValue<U> pattern,
                                                                                   final String message,
                                                                                   final MessageValue<T>... values) {
      return matches(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matches(final ParameterValue<U> pattern) {
      return matches(pattern,
                     FastStringFormat.of("'", validatingObject(), "' must match '", parameter(0), "'"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatches(final ParameterValue<U> pattern,
                                                                                      final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !FILE_SYSTEM
            .getPathMatcher(pattern.requireNonNullValue().toString()).matches(v),
                            messageFormatter).ruleContext("PathRules.notMatches", pattern);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatches(final ParameterValue<U> pattern,
                                                                                      final String message,
                                                                                      final MessageValue<T>... values) {
      return notMatches(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatches(final ParameterValue<U> pattern) {
      return notMatches(pattern,
                        FastStringFormat.of("'",
                                            validatingObject(),
                                            "' must not match '",
                                            parameter(0),
                                            "'"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesIgnoreCase(final ParameterValue<U> pattern,
                                                                                             final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> FILE_SYSTEM
            .getPathMatcher(caseInsensitive(pattern.requireNonNullValue().toString()))
            .matches(caseInsensitive(v)), messageFormatter).ruleContext("PathRules.matchesIgnoreCase",
                                                                        pattern);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesIgnoreCase(final ParameterValue<U> pattern,
                                                                                             final String message,
                                                                                             final MessageValue<T>... values) {
      return matchesIgnoreCase(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesIgnoreCase(final ParameterValue<U> pattern) {
      return matchesIgnoreCase(pattern,
                               FastStringFormat.of("'",
                                                   validatingObject(),
                                                   "' must match '",
                                                   parameter(0),
                                                   "' (case-insensitive)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesIgnoreCase(final ParameterValue<U> pattern,
                                                                                                final MessageFormatter<T> messageFormatter) {
      notNull(pattern, "pattern");

      return satisfiesValue(v -> !FILE_SYSTEM
            .getPathMatcher(caseInsensitive(pattern.requireNonNullValue().toString()))
            .matches(caseInsensitive(v)), messageFormatter).ruleContext("PathRules.notMatchesIgnoreCase",
                                                                        pattern);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesIgnoreCase(final ParameterValue<U> pattern,
                                                                                                final String message,
                                                                                                final MessageValue<T>... values) {
      return notMatchesIgnoreCase(pattern, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesIgnoreCase(final ParameterValue<U> pattern) {
      return notMatchesIgnoreCase(pattern,
                                  FastStringFormat.of("'",
                                                      validatingObject(),
                                                      "' must not match '",
                                                      parameter(0),
                                                      "' (case-insensitive)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesGlob(final ParameterValue<U> glob,
                                                                                       final MessageFormatter<T> messageFormatter) {
      notNull(glob, "glob");

      return satisfiesValue(v -> FILE_SYSTEM
            .getPathMatcher("glob:" + glob.requireNonNullValue()).matches(v), messageFormatter).ruleContext(
            "PathRules.matchesGlob",
            glob);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesGlob(final ParameterValue<U> glob,
                                                                                       final String message,
                                                                                       final MessageValue<T>... values) {
      return matchesGlob(glob, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesGlob(final ParameterValue<U> glob) {
      return matchesGlob(glob,
                         FastStringFormat.of("'",
                                             validatingObject(),
                                             "' must match '",
                                             parameter(0),
                                             "' (glob)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesGlob(final ParameterValue<U> glob,
                                                                                          final MessageFormatter<T> messageFormatter) {
      notNull(glob, "glob");

      return satisfiesValue(v -> !FILE_SYSTEM
            .getPathMatcher("glob:" + glob.requireNonNullValue()).matches(v), messageFormatter).ruleContext(
            "PathRules.notMatchesGlob",
            glob);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesGlob(final ParameterValue<U> glob,
                                                                                          final String message,
                                                                                          final MessageValue<T>... values) {
      return notMatchesGlob(glob, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesGlob(final ParameterValue<U> glob) {
      return notMatchesGlob(glob,
                            FastStringFormat.of("'",
                                                validatingObject(),
                                                "' must not match '",
                                                parameter(0),
                                                "' (glob)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesGlobIgnoreCase(final ParameterValue<U> glob,
                                                                                                 final MessageFormatter<T> messageFormatter) {
      notNull(glob, "glob");

      return satisfiesValue(v -> FILE_SYSTEM
            .getPathMatcher(caseInsensitive("glob:" + glob.requireNonNullValue()))
            .matches(caseInsensitive(v)), messageFormatter).ruleContext("PathRules.matchesGlobIgnoreCase",
                                                                        glob);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesGlobIgnoreCase(final ParameterValue<U> glob,
                                                                                                 final String message,
                                                                                                 final MessageValue<T>... values) {
      return matchesGlobIgnoreCase(glob, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesGlobIgnoreCase(final ParameterValue<U> glob) {
      return matchesGlobIgnoreCase(glob,
                                   FastStringFormat.of("'",
                                                       validatingObject(),
                                                       "' must match '",
                                                       parameter(0),
                                                       "' (case-insensitive glob)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesGlobIgnoreCase(final ParameterValue<U> glob,
                                                                                                    final MessageFormatter<T> messageFormatter) {
      notNull(glob, "glob");

      return satisfiesValue(v -> !FILE_SYSTEM
            .getPathMatcher(caseInsensitive("glob:" + glob.requireNonNullValue()))
            .matches(caseInsensitive(v)), messageFormatter).ruleContext("PathRules.notMatchesGlobIgnoreCase",
                                                                        glob);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesGlobIgnoreCase(final ParameterValue<U> glob,
                                                                                                    final String message,
                                                                                                    final MessageValue<T>... values) {
      return notMatchesGlobIgnoreCase(glob, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesGlobIgnoreCase(final ParameterValue<U> glob) {
      return notMatchesGlobIgnoreCase(glob,
                                      FastStringFormat.of("'",
                                                          validatingObject(),
                                                          "' must not match '",
                                                          parameter(0),
                                                          "' (case-insensitive glob)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesRegex(final ParameterValue<U> regex,
                                                                                        final MessageFormatter<T> messageFormatter) {
      notNull(regex, "regex");

      return satisfiesValue(v -> FILE_SYSTEM
            .getPathMatcher("regex:" + regex.requireNonNullValue()).matches(v), messageFormatter).ruleContext(
            "PathRules.matchesRegex",
            regex);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesRegex(final ParameterValue<U> regex,
                                                                                        final String message,
                                                                                        final MessageValue<T>... values) {
      return matchesRegex(regex, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesRegex(final ParameterValue<U> regex) {
      return matchesRegex(regex,
                          FastStringFormat.of("'",
                                              validatingObject(),
                                              "' must match '",
                                              parameter(0),
                                              "' (regex)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesRegex(final ParameterValue<U> regex,
                                                                                           final MessageFormatter<T> messageFormatter) {
      notNull(regex, "regex");

      return satisfiesValue(v -> !FILE_SYSTEM
            .getPathMatcher("regex:" + regex.requireNonNullValue()).matches(v), messageFormatter).ruleContext(
            "PathRules.notMatchesRegex",
            regex);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesRegex(final ParameterValue<U> regex,
                                                                                           final String message,
                                                                                           final MessageValue<T>... values) {
      return notMatchesRegex(regex, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesRegex(final ParameterValue<U> regex) {
      return notMatchesRegex(regex,
                             FastStringFormat.of("'",
                                                 validatingObject(),
                                                 "' must not match '",
                                                 parameter(0),
                                                 "' (regex)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesRegexIgnoreCase(final ParameterValue<U> regex,
                                                                                                  final MessageFormatter<T> messageFormatter) {
      notNull(regex, "regex");

      return satisfiesValue(v -> FILE_SYSTEM
            .getPathMatcher(caseInsensitive("regex:" + regex.requireNonNullValue()))
            .matches(caseInsensitive(v)), messageFormatter).ruleContext("PathRules.matchesRegexIgnoreCase",
                                                                        regex);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesRegexIgnoreCase(final ParameterValue<U> regex,
                                                                                                  final String message,
                                                                                                  final MessageValue<T>... values) {
      return matchesRegexIgnoreCase(regex, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> matchesRegexIgnoreCase(final ParameterValue<U> regex) {
      return matchesRegexIgnoreCase(regex,
                                    FastStringFormat.of("'",
                                                        validatingObject(),
                                                        "' must match '",
                                                        parameter(0),
                                                        "' (case-insensitive regex)"));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesRegexIgnoreCase(final ParameterValue<U> regex,
                                                                                                     final MessageFormatter<T> messageFormatter) {
      notNull(regex, "regex");

      return satisfiesValue(v -> !FILE_SYSTEM
            .getPathMatcher(caseInsensitive("regex:" + regex.requireNonNullValue()))
            .matches(caseInsensitive(v)), messageFormatter).ruleContext("PathRules.notMatchesRegexIgnoreCase",
                                                                        regex);
   }

   @SafeVarargs
   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesRegexIgnoreCase(final ParameterValue<U> regex,
                                                                                                     final String message,
                                                                                                     final MessageValue<T>... values) {
      return notMatchesRegexIgnoreCase(regex, DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path, U extends CharSequence> InvariantRule<T> notMatchesRegexIgnoreCase(final ParameterValue<U> regex) {
      return notMatchesRegexIgnoreCase(regex,
                                       FastStringFormat.of("'",
                                                           validatingObject(),
                                                           "' must not match '",
                                                           parameter(0),
                                                           "' (case-insensitive regex)"));
   }

   public static <T extends Path> InvariantRule<T> hasRoot(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> path.getRoot() != null,
                            messageFormatter).ruleContext("PathRules.hasRoot");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> hasRoot(final String message,
                                                           final MessageValue<T>... values) {
      return hasRoot(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> hasRoot() {
      return hasRoot(FastStringFormat.of("'", validatingObject(), "' must have a root component"));
   }

   public static <T extends Path> InvariantRule<T> hasParent(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> path.getParent() != null, messageFormatter).ruleContext(
            "PathRules.hasParent");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> hasParent(final String message,
                                                             final MessageValue<T>... values) {
      return hasParent(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> hasParent() {
      return hasParent(FastStringFormat.of("'", validatingObject(), "' must have a parent component"));
   }

   public static <T extends Path> InvariantRule<T> hasFileName(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(path -> path.getFileName() != null, messageFormatter).ruleContext(
            "PathRules.hasFileName");
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> hasFileName(final String message,
                                                               final MessageValue<T>... values) {
      return hasFileName(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> hasFileName() {
      return hasFileName(FastStringFormat.of("'", validatingObject(), "' must have a filename component"));
   }

   // FIXME ruleName not applied when not on PredicateRules::satisfies
   public static <T extends Path> InvariantRule<T> isPosix(final MessageFormatter<T> messageFormatter) {
      return isNotNull().andValue(matchesRegex(value("[a-zA-Z0-9.-/]*"), messageFormatter).ruleContext(
            "PathRules.isPosix"));
   }

   @SafeVarargs
   public static <T extends Path> InvariantRule<T> isPosix(final String message,
                                                           final MessageValue<T>... values) {
      return isPosix(DefaultMessageFormat.of(message, values));
   }

   public static <T extends Path> InvariantRule<T> isPosix() {
      return isPosix(FastStringFormat.of("'", validatingObject(), "' must contain only POSIX characters"));
   }

   public static <T extends String> InvariantRule<T> path(final InvariantRule<? super Path> rule) {
      return as(Paths::get, rule);
   }

   public static <T extends Path> InvariantRule<T> normalized(final InvariantRule<? super Path> rule) {
      return isNotNull().andValue(as(Path::normalize, rule));
   }

   public static <T extends Path> InvariantRule<T> root(final InvariantRule<? super Path> rule) {
      return hasRoot().andValue(property(Path::getRoot, "root", rule));
   }

   public static <T extends Path> InvariantRule<T> parent(final InvariantRule<? super Path> rule) {
      return hasParent().andValue(property(Path::getParent, "parent", rule));
   }

   public static <T extends Path> InvariantRule<T> fileName(final InvariantRule<? super Path> rule) {
      return hasFileName().andValue(property(Path::getFileName, "fileName", rule));
   }

   public static <T extends Path> InvariantRule<T> extension(final InvariantRule<? super String> rule) {
      return hasFileName().andValue(property(PathRules::extension, "extension", rule));
   }

   public static <T extends Path> InvariantRule<T> file(final InvariantRule<? super File> rule) {
      return as(Path::toFile, rule);
   }

   /**
    * Returns the index of the last directory separator character.
    * <p>
    * This method will handle a file in either Unix or Windows format.
    * The position of the last forward or backslash is returned.
    * <p>
    * The output will be the same irrespective of the machine that the code is running on.
    *
    * @param filename the filename to find the last path separator in, null returns -1
    *
    * @return the index of the last separator character, or -1 if there
    *       is no such character
    */
   private static int indexOfLastSeparator(final String filename) {
      if (filename == null) {
         return -1;
      }
      final int lastUnixPos = filename.lastIndexOf('/');
      final int lastWindowsPos = filename.lastIndexOf('\\');
      return Math.max(lastUnixPos, lastWindowsPos);
   }

   /**
    * Returns the index of the last extension separator character, which is a dot.
    * <p>
    * This method also checks that there is no directory separator after the last dot. To do this it uses
    * {@link #indexOfLastSeparator(String)} which will handle a file in either Unix or Windows format.
    * </p>
    * <p>
    * The output will be the same irrespective of the machine that the code is running on.
    * </p>
    *
    * @param path the path to find the last extension separator in, null returns -1
    *
    * @return the index of the last extension separator character, or -1 if there is no such character
    */
   private static int indexOfExtension(final String path) {
      if (path == null) {
         return -1;
      }
      final int extensionPos = path.lastIndexOf('.');
      final int lastSeparator = indexOfLastSeparator(path);
      return lastSeparator > extensionPos ? -1 : extensionPos;
   }

   private static String caseInsensitive(String string) {
      if (string == null) {
         return null;
      }
      return string.toUpperCase().toLowerCase();
   }

   private static Path caseInsensitive(Path path) {
      if (path == null) {
         return null;
      }

      return Paths.get(caseInsensitive(path.toString()));
   }

   /**
    * Gets the extension of a filename.
    * <p>
    * This method returns the textual part of the filename after the last dot.
    * There must be no directory separator after the dot.
    * <pre>
    * foo.txt      --&gt; "txt"
    * a/b/c.jpg    --&gt; "jpg"
    * a/b.txt/c    --&gt; ""
    * a/b/c        --&gt; ""
    * </pre>
    * <p>
    * The output will be the same irrespective of the machine that the code is running on.
    *
    * @param path the path to retrieve the extension of.
    *
    * @return the extension of the file or an empty string if none exists or {@code null}
    *       if the filename is {@code null}.
    */
   private static String extension(Path path) {
      if (path == null) {
         return null;
      }
      String pathString = path.toString();
      int index = indexOfExtension(pathString);
      if (index == -1) {
         return "";
      } else {
         return pathString.substring(index + 1);
      }
   }

   private static boolean pathEquals(Path path1, Path path2, boolean ignoreCase) {
      path1 = notNull(path1, "path1").normalize();
      path2 = notNull(path2, "path2").normalize();

      if (path1.isAbsolute() != path2.isAbsolute()) {
         return false;
      }

      Iterator<Path> path1Iterator = path1.iterator();
      Iterator<Path> path2Iterator = path2.iterator();

      while (path1Iterator.hasNext()) {
         if (path2Iterator.hasNext()) {
            Path path1Segment = path1Iterator.next();
            Path path2Segment = path2Iterator.next();
            if (!equalsPathSegment(path1Segment, path2Segment, ignoreCase)) {
               return false;
            }
         } else {
            break;
         }
      }
      return !path1Iterator.hasNext() && !path2Iterator.hasNext();
   }

   /**
    * Checks if sub path is contained in path. If sub path is absolute, path must be absolute and start with
    * sub path.
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param ignoreCase case-insensitive match
    *
    * @return {@code true} of sub path contained in path
    */
   // FIXME Review algorithm to support : other/other/other/path contains other/other/path ? -> double boucle sur le realpath (1 qui avance de 1, et une inner qui boucle sur le subpath)
   private static boolean pathContains(Path path, Path subPath, boolean ignoreCase) {
      path = notNull(path, "path").normalize();
      subPath = notNull(subPath, "subPath").normalize();

      if (subPath.isAbsolute()) {
         return pathStartWith(path, subPath, ignoreCase);
      }

      Iterator<Path> realPathIterator = path.iterator();
      Iterator<Path> subPathIterator = subPath.iterator();

      while (realPathIterator.hasNext()) {
         Path realPathSegment = realPathIterator.next();
         if (subPathIterator.hasNext()) {
            Path subPathSegment = subPathIterator.next();
            if (!equalsPathSegment(realPathSegment, subPathSegment, ignoreCase)) {
               subPathIterator = subPath.iterator();
            }
         } else {
            break;
         }
      }
      return !subPathIterator.hasNext();
   }

   private static boolean pathStartWith(Path path, Path subPath, boolean ignoreCase) {
      path = notNull(path, "path").normalize();
      subPath = notNull(subPath, "subPath").normalize();

      if (subPath.isAbsolute() != path.isAbsolute()) {
         return false;
      }

      Iterator<Path> realPathIterator = path.iterator();
      Iterator<Path> subPathIterator = subPath.iterator();

      while (realPathIterator.hasNext()) {
         Path realPathSegment = realPathIterator.next();
         if (subPathIterator.hasNext()) {
            Path subPathSegment = subPathIterator.next();
            if (!equalsPathSegment(realPathSegment, subPathSegment, ignoreCase)) {
               return false;
            }
         } else {
            break;
         }
      }
      return !subPathIterator.hasNext();
   }

   private static boolean pathEndWith(Path path, Path subPath, boolean ignoreCase) {
      path = notNull(path, "path").normalize();
      subPath = notNull(subPath, "subPath").normalize();

      if (subPath.isAbsolute()) {
         return pathEquals(path, subPath, ignoreCase);
      }

      Iterator<Path> realPathIterator = reverseIterator(path);
      Iterator<Path> subPathIterator = reverseIterator(subPath);

      while (realPathIterator.hasNext()) {
         Path realPathSegment = realPathIterator.next();
         if (subPathIterator.hasNext()) {
            Path subPathSegment = subPathIterator.next();
            if (!equalsPathSegment(realPathSegment, subPathSegment, ignoreCase)) {
               return false;
            }
         } else {
            break;
         }
      }
      return !subPathIterator.hasNext();
   }

   private static boolean equalsPathSegment(Path path1, Path path2, boolean ignoreCase) {
      return ignoreCase
             ? StringUtils.equalsIgnoreCase(path1.toString(), path2.toString())
             : StringUtils.equals(path1.toString(), path2.toString());
   }

   private static Iterator<Path> reverseIterator(Path path) {
      return new Iterator<Path>() {
         private int i = path.getNameCount() - 1;

         @Override
         public boolean hasNext() {
            return i >= 0;
         }

         @Override
         public Path next() {
            if (i >= 0) {
               Path result = path.getName(i);
               i--;
               return result;
            } else {
               throw new NoSuchElementException();
            }
         }
      };
   }

}
