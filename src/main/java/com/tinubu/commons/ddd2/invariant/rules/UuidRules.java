/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObject;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.as;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;

import java.util.UUID;
import java.util.function.Predicate;

import com.tinubu.commons.ddd2.invariant.InvariantRule;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.formatter.DefaultMessageFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.MessageFormatter;

public class UuidRules {

   private UuidRules() {
   }

   public static <T extends CharSequence> InvariantRule<T> isUuid(final MessageFormatter<T> messageFormatter) {
      return isNotBlank().andValue(satisfiesValue(validUuid(), messageFormatter).ruleContext(
            "UuidRules.isUuid"));
   }

   @SafeVarargs
   public static <T extends CharSequence> InvariantRule<T> isUuid(final String message,
                                                                  final MessageValue<T>... values) {
      return isUuid(DefaultMessageFormat.of(message, values));
   }

   public static <T extends CharSequence> InvariantRule<T> isUuid() {
      return isUuid(FastStringFormat.of("'", validatingObject(), "' UUID must be valid"));
   }

   public static <T extends UUID> InvariantRule<T> isVersion1(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.version() == 1, messageFormatter).ruleContext("UuidRules.isUuidV1");
   }

   @SafeVarargs
   public static <T extends UUID> InvariantRule<T> isVersion1(final String message,
                                                              final MessageValue<T>... values) {
      return isVersion1(DefaultMessageFormat.of(message, values));
   }

   public static <T extends UUID> InvariantRule<T> isVersion1() {
      return isVersion1(FastStringFormat.of("'",
                                            validatingObject(),
                                            "' UUID version must be 1 (time-based)"));
   }

   public static <T extends UUID> InvariantRule<T> isVersion2(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.version() == 2, messageFormatter).ruleContext("UuidRules.isUuidV2");
   }

   @SafeVarargs
   public static <T extends UUID> InvariantRule<T> isVersion2(final String message,
                                                              final MessageValue<T>... values) {
      return isVersion2(DefaultMessageFormat.of(message, values));
   }

   public static <T extends UUID> InvariantRule<T> isVersion2() {
      return isVersion2(FastStringFormat.of("'",
                                            validatingObject(),
                                            "' UUID version must be 2 (DCE security)"));
   }

   public static <T extends UUID> InvariantRule<T> isVersion3(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.version() == 3, messageFormatter).ruleContext("UuidRules.isUuidV3");
   }

   @SafeVarargs
   public static <T extends UUID> InvariantRule<T> isVersion3(final String message,
                                                              final MessageValue<T>... values) {
      return isVersion3(DefaultMessageFormat.of(message, values));
   }

   public static <T extends UUID> InvariantRule<T> isVersion3() {
      return isVersion3(FastStringFormat.of("'",
                                            validatingObject(),
                                            "' UUID version must be 3 (name-based MD5)"));
   }

   public static <T extends UUID> InvariantRule<T> isVersion4(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.version() == 4, messageFormatter).ruleContext("UuidRules.isUuidV4");
   }

   @SafeVarargs
   public static <T extends UUID> InvariantRule<T> isVersion4(final String message,
                                                              final MessageValue<T>... values) {
      return isVersion4(DefaultMessageFormat.of(message, values));
   }

   public static <T extends UUID> InvariantRule<T> isVersion4() {
      return isVersion4(FastStringFormat.of("'", validatingObject(), "' UUID version must be 4 (random)"));
   }

   public static <T extends UUID> InvariantRule<T> isVersion5(final MessageFormatter<T> messageFormatter) {
      return satisfiesValue(v -> v.version() == 5, messageFormatter).ruleContext("UuidRules.isUuidV5");
   }

   @SafeVarargs
   public static <T extends UUID> InvariantRule<T> isVersion5(final String message,
                                                              final MessageValue<T>... values) {
      return isVersion5(DefaultMessageFormat.of(message, values));
   }

   public static <T extends UUID> InvariantRule<T> isVersion5() {
      return isVersion5(FastStringFormat.of("'",
                                            validatingObject(),
                                            "' UUID version must be 5 (name-based SHA-1)"));
   }

   public static <T extends CharSequence> InvariantRule<T> uuid(final InvariantRule<? super UUID> rule) {
      return isUuid().andValue(as(UuidRules::uuid, rule));
   }

   public static <T extends UUID> InvariantRule<T> version(final InvariantRule<? super Integer> rule) {
      return as(UUID::version, rule);
   }

   public static <T extends UUID> InvariantRule<T> variant(final InvariantRule<? super Integer> rule) {
      return as(UUID::variant, rule);
   }

   public static <T extends UUID> InvariantRule<T> timestamp(final InvariantRule<? super Long> rule) {
      return isVersion1().andValue(as(UUID::timestamp, rule));
   }

   public static <T extends UUID> InvariantRule<T> clockSequence(final InvariantRule<? super Integer> rule) {
      return isVersion1().andValue(as(UUID::clockSequence, rule));
   }

   public static <T extends UUID> InvariantRule<T> node(final InvariantRule<? super Long> rule) {
      return isVersion1().andValue(as(UUID::node, rule));
   }

   private static <T extends CharSequence> UUID uuid(T uuid) {
      return UUID.fromString(uuid.toString());
   }

   private static <T extends CharSequence> Predicate<T> validUuid() {
      return (T uuid) -> {
         try {
            uuid(uuid);
            return true;
         } catch (IllegalArgumentException e) {
            return false;
         }
      };
   }

}
