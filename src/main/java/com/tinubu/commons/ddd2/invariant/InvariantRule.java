/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Predicate;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.commons.ddd2.invariant.InvariantResult.InvariantStatus;
import com.tinubu.commons.ddd2.invariant.formatter.FixedString;

/**
 * Primitive invariant rule to be applied to any validating object value.
 * Invariant rules are function of (validating object value, validating object name) and return an {@link
 * InvariantResult}.
 * They are reusable for any validating object value of the specified {@code T} type.
 * <p>
 * Implementations must override {@link #apply(Object)}.
 *
 * @param <T> validating object type
 */
public interface InvariantRule<T>
      extends Function<ValidatingObject<T>, InvariantResult<T>>, Specification<T> {

   @SuppressWarnings("unchecked")
   default <V extends T> InvariantResult<V> evaluate(ValidatingObject<T> validatingObject) {
      return (InvariantResult<V>) apply(validatingObject);
   }

   default <V extends T> InvariantResult<V> evaluate(T objectValue, String objectName) {
      return evaluate(ValidatingObject.validatingObject(objectValue, objectName));
   }

   default <V extends T> InvariantResult<V> evaluate(T objectValue) {
      return evaluate(ValidatingObject.validatingObject(objectValue));
   }

   @Override
   default boolean satisfiedBy(T object) {
      return evaluate(object).success();
   }

   /**
    * Updates current rule name. Use {@link #ruleContext(String, List<ParameterValue>)} to also update
    * parameters in the same operation.
    *
    * @param ruleName rule name
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return new wrapping rule with validation context updated with rule name
    *
    * @see #ruleContext(String, List)
    * @see #ruleContext(String, ParameterValue[])
    */
   @SuppressWarnings("unchecked")
   default <V extends T> InvariantRule<V> ruleContext(String ruleName) {
      notBlank(ruleName, "ruleName");

      return validatingObject -> evaluate((ValidatingObject<T>) validatingObject.ruleName(ruleName));
   }

   /**
    * Updates current rule definition (rule name, optional rule parameters).
    *
    * @param ruleName rule name
    * @param ruleParameters optional rule parameters
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return new wrapping rule with validation context updated with rule definition
    *
    * @see #ruleContext(String)
    */
   @SuppressWarnings("unchecked")
   default <V extends T> InvariantRule<V> ruleContext(String ruleName,
                                                      List<ParameterValue<?>> ruleParameters) {
      notBlank(ruleName, "ruleName");
      noNullElements(ruleParameters, "ruleParameters");

      return validatingObject -> evaluate((ValidatingObject<T>) validatingObject
            .ruleName(ruleName)
            .ruleParameters(ruleParameters));
   }

   default <V extends T> InvariantRule<V> ruleContext(String ruleName, ParameterValue<?>... ruleParameters) {
      return ruleContext(ruleName, Arrays.asList(noNullElements(ruleParameters, "ruleParameters")));
   }

   /**
    * Composes current invariant rule with specified rule.
    * If current rule returns an {@link InvariantStatus#ERROR} result, the specified
    * invariant rule is not evaluated.
    *
    * @param invariantRule invariant rule to compose
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return composed invariant rule
    */
   default <V extends T> InvariantRule<V> andValue(InvariantRule<V> invariantRule) {
      notNull(invariantRule, "invariantRule");

      return validatingObject -> {
         notNull(validatingObject, "validatingObject");

         @SuppressWarnings("unchecked")
         InvariantResult<V> leftResult = evaluate((ValidatingObject<T>) validatingObject);
         InvariantResult<V> result;

         if (leftResult.status() != InvariantStatus.SUCCESS) {
            result = leftResult;
         } else {
            result = invariantRule.evaluate(validatingObject.resetRuleContext());
         }

         return result;
      };
   }

   /**
    * Composes current invariant rule with specified rule.
    * If current rule returns an {@link InvariantStatus#ERROR} result, the specified
    * invariant rule is evaluated, and current rule result is discarded in final result.
    *
    * @param invariantRule invariant rule to compose
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return composed invariant rule
    */
   default <V extends T> InvariantRule<V> orValue(InvariantRule<V> invariantRule) {
      notNull(invariantRule, "invariantRule");

      return validatingObject -> {
         notNull(validatingObject, "validatingObject");

         @SuppressWarnings("unchecked")
         InvariantResult<V> preApply = evaluate((ValidatingObject<T>) validatingObject);
         InvariantResult<V> result;

         if (preApply.status() == InvariantStatus.SUCCESS) {
            result = preApply;
         } else {
            result = invariantRule.evaluate(validatingObject.resetRuleContext());
         }

         return result;
      };
   }

   /**
    * Makes conditional the rule this method applies on.
    * Condition is successful if specified condition supplies a {@link Boolean#TRUE}.
    *
    * @param condition condition
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return conditional invariant rule
    */
   default <V extends T> InvariantRule<V> ifIsSatisfied(BooleanSupplier condition) {
      notNull(condition, "condition");

      return validatingObject -> {
         notNull(validatingObject, "validatingObject");

         if (condition.getAsBoolean()) {
            //noinspection unchecked
            return evaluate((ValidatingObject<T>) validatingObject);
         } else {
            return InvariantResult.success(validatingObject,
                                           FixedString.of("Conditional rule not evaluated"));
         }
      };
   }

   /**
    * Makes conditional the rule this method applies on.
    * Condition is successful if specified object satisfies the specified rule.
    *
    * @param object object to apply rule on
    * @param rule rule to apply on object
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return conditional invariant rule
    */
   default <V extends T, P> InvariantRule<V> ifIsSatisfied(P object, InvariantRule<P> rule) {
      notNull(rule, "rule");

      return ifIsSatisfied(() -> rule.satisfiedBy(object));
   }

   /**
    * Makes conditional the rule this method applies on.
    * Condition is successful if specified parameter value is {@link ParameterValue#nonNull()}.
    *
    * @param value value to check for nullity
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return conditional invariant rule
    */
   default <V extends T, P> InvariantRule<V> ifNonNull(ParameterValue<P> value) {
      return ifIsSatisfied(value::nonNull);
   }

   /**
    * Makes conditional the rule this method applies on.
    * Condition is successful if specified object is not {@code null}.
    *
    * @param object object to check for nullity
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return conditional invariant rule
    */
   default <V extends T, P> InvariantRule<V> ifNonNull(P object) {
      return ifIsSatisfied(() -> Objects.nonNull(object));
   }

   /**
    * Makes conditional the rule this method applies on.
    * Condition is successful if specified optional object is {@link Optional#isPresent()}.
    *
    * @param object object to check
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return conditional invariant rule
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   default <V extends T, P> InvariantRule<V> ifPresent(Optional<P> object) {
      notNull(object, "object");

      return ifIsSatisfied(object::isPresent);
   }

   /**
    * Makes conditional the rule this method applies on.
    * Condition is successful if validating value satisfies specified predicate (Note that
    * {@link InvariantRule} extends {@link Specification} extends {@link Predicate}).
    *
    * @param predicate predicate to apply to validating object
    * @param <V> validating object type supporting rules on wider types (e.g. {@link Object})
    *
    * @return conditional invariant rule
    */
   default <V extends T> InvariantRule<V> ifValue(Predicate<V> predicate) {
      notNull(predicate, "predicate");

      return validatingObject -> {
         notNull(validatingObject, "validatingObject");

         if (predicate.test(validatingObject.value())) {
            //noinspection unchecked
            return evaluate((ValidatingObject<T>) validatingObject);
         } else {
            return InvariantResult.success(validatingObject,
                                           FixedString.of("Conditional rule not evaluated"));
         }
      };
   }

   /**
    * Maps an invariant rules to an alternative value of originating validated object.
    * Validating object nullity must be managed externally.
    *
    * @param <V> originating object type
    * @param valueMapper mapping function to apply to originating validated object's value. Function
    *       argument can be {@code null} if value is {@code null}
    * @param nameMapper mapping function to apply to originating validated object's name. Function
    *       argument can be {@code null} if name not set
    *
    * @return invariant rule of mapped object
    */
   default <V> InvariantRule<V> map(Function<? super V, ? extends T> valueMapper,
                                    Function<? super String, String> nameMapper) {
      notNull(nameMapper, "nameMapper");
      notNull(valueMapper, "valueMapper");

      return validatingObject -> {
         notNull(validatingObject, "validatingObject");

         ValidatingObject<T> mappedVo = validatingObject.map(valueMapper, nameMapper);
         InvariantResult<T> evaluate = evaluate(applyRuleContext(mappedVo, validatingObject));

         return new InvariantResult<>(evaluate.invariantName().orElse(null),
                                      applyRuleContext(validatingObject, evaluate.validatingObject()),
                                      evaluate.status(),
                                      evaluate.message());
      };
   }

   default <V> InvariantRule<V> map(Function<? super V, ? extends T> valueMapper) {
      return map(valueMapper, Function.identity());
   }

   static <V, T> ValidatingObject<V> applyRuleContext(ValidatingObject<V> targetValidatingObject,
                                                      ValidatingObject<T> sourceValidatingObject) {
      return sourceValidatingObject
            .ruleName()
            .map(targetValidatingObject::ruleName)
            .orElse(targetValidatingObject)
            .ruleParameters(sourceValidatingObject.ruleParameters());
   }

}
