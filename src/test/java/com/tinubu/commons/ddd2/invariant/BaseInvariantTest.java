/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.function.Supplier;
import java.util.regex.Pattern;

import org.assertj.core.api.AbstractThrowableAssert;

import com.tinubu.commons.ddd2.invariant.InvariantResult.InvariantStatus;
import com.tinubu.commons.ddd2.invariant.formatter.FixedString;

public class BaseInvariantTest {

   public <T> void assertThatSuccess(T value,
                                     String name,
                                     InvariantRule<T> rule,
                                     String message,
                                     ValidatingObject<T> validatingObject) {
      assertThat(rule.evaluate(value, name)).isEqualTo(InvariantResult.success(validatingObject,
                                                                               FixedString.of(message)));
   }

   public <T> void assertThatSuccess(T value, String name, InvariantRule<T> rule, String message) {
      InvariantResult<T> evaluate = name != null ? rule.evaluate(value, name) : rule.evaluate(value);

      assertThat(evaluate.status()).isEqualTo(InvariantStatus.SUCCESS);
      assertThat(evaluate.message()).isEqualTo(InvariantResult.VALIDATION_MESSAGE_ON_SUCCESS ? message : "");
      assertThat(evaluate.validatingObject().name()).isEqualTo(nullable(name));
      assertThat(evaluate.validatingObject().value()).isEqualTo(value);
   }

   public <T> void assertThatSuccess(T value, String name, InvariantRule<T> rule, Pattern message) {
      InvariantResult<T> evaluate = name != null ? rule.evaluate(value, name) : rule.evaluate(value);

      assertThat(evaluate.status()).isEqualTo(InvariantStatus.SUCCESS);
      if (InvariantResult.VALIDATION_MESSAGE_ON_SUCCESS) {
         assertThat(evaluate.message()).matches(message);
      } else {
         assertThat(evaluate.message()).isEqualTo("");
      }
      assertThat(evaluate.validatingObject().name()).isEqualTo(nullable(name));
      assertThat(evaluate.validatingObject().value()).isEqualTo(value);
   }

   public <T> void assertThatSuccess(T value, InvariantRule<T> rule, String message) {
      assertThatSuccess(value, null, rule, message);
   }

   public <T> void assertThatSuccess(T value, InvariantRule<T> rule, Pattern message) {
      assertThatSuccess(value, null, rule, message);
   }

   public <T> void assertThatError(T value,
                                   String name,
                                   InvariantRule<T> rule,
                                   String message,
                                   ValidatingObject<T> validatingObject) {
      assertThat(rule.evaluate(value, name)).isEqualTo(InvariantResult.error(validatingObject,
                                                                             FixedString.of(message)));
   }

   public <T> void assertThatError(T value, String name, InvariantRule<T> rule, String message) {
      InvariantResult<T> evaluate = name != null ? rule.evaluate(value, name) : rule.evaluate(value);

      assertThat(evaluate.status()).isEqualTo(InvariantStatus.ERROR);
      assertThat(evaluate.message()).isEqualTo(message);
      assertThat(evaluate.validatingObject().name()).isEqualTo(nullable(name));
      assertThat(evaluate.validatingObject().value()).isEqualTo(value);
   }

   public <T> void assertThatError(T value, String name, InvariantRule<T> rule, Pattern message) {
      InvariantResult<T> evaluate = name != null ? rule.evaluate(value, name) : rule.evaluate(value);

      assertThat(evaluate.status()).isEqualTo(InvariantStatus.ERROR);
      assertThat(evaluate.message()).matches(message);
      assertThat(evaluate.validatingObject().name()).isEqualTo(nullable(name));
      assertThat(evaluate.validatingObject().value()).isEqualTo(value);
   }

   public <T> void assertThatError(T value, InvariantRule<T> rule, String message) {
      assertThatError(value, null, rule, message);
   }

   public <T> void assertThatError(T value, InvariantRule<T> rule, Pattern message) {
      assertThatError(value, null, rule, message);
   }

   public <T> AbstractThrowableAssert<?, ? extends Throwable> assertThatThrown(T value,
                                                                               String name,
                                                                               Supplier<InvariantRule<T>> rule) {
      return assertThatThrownBy(() -> rule.get().evaluate(value, name), "field");
   }

   public <T> AbstractThrowableAssert<?, ? extends Throwable> assertThatThrown(T value,
                                                                               Supplier<InvariantRule<T>> rule) {
      return assertThatThrownBy(() -> rule.get().evaluate(value), "field");
   }

   public <T> ValidatingObject<T> validatingObject(T value, String name) {
      return ValidatingObject.validatingObject(value, name);
   }

   public <T> ValidatingObject<T> validatingObject(T value) {
      return ValidatingObject.validatingObject(value);
   }

}
