/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.MessageValue;

public class MatchingRulesTest extends BaseInvariantTest {

   @Test
   public void testMatch() {
      assertThatSuccess("a phrase to match",
                        MatchingRules.matches(value(Pattern.compile("a phrase.*match"))),
                        "'a phrase to match' must match regexp /a phrase.*match/0");
      assertThatError("a phrase to match",
                      "field",
                      MatchingRules.matches(value(Pattern.compile("phrase.*match"))),
                      "'field=a phrase to match' must match regexp /phrase.*match/0");
      assertThatError("a phrase to match",
                      "field",
                      MatchingRules.matches(value(Pattern.compile("a phrase.*to"))),
                      "'field=a phrase to match' must match regexp /a phrase.*to/0");
   }

   @Test
   public void testMatchWhenNullPattern() {
      assertThatThrown("a phrase to match", () -> MatchingRules.matches(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'pattern' must not be null");
   }

   @Test
   public void testMatchWhenCustomMessage() {

      assertThatError("a phrase to match", "field",
                      MatchingRules.matches(value(Pattern.compile("a phrase.*to")),
                                            "this '%1$s' must match",
                                            MessageValue.validatingObjectName()), "this 'field' must match");
   }

   @Test
   public void testPartiallyMatch() {
      assertThatSuccess("a phrase to match",
                        MatchingRules.partiallyMatches(value(Pattern.compile("a phrase.*match"))),
                        "'a phrase to match' must partially match regexp /a phrase.*match/0");
      assertThatSuccess("a phrase to match",
                        MatchingRules.partiallyMatches(value(Pattern.compile("phrase.*match"))),
                        "'a phrase to match' must partially match regexp /phrase.*match/0");
      assertThatSuccess("a phrase to match",
                        MatchingRules.partiallyMatches(value(Pattern.compile("a phrase.*to"))),
                        "'a phrase to match' must partially match regexp /a phrase.*to/0");
      assertThatError("a phrase to match",
                      "field",
                      MatchingRules.partiallyMatches(value(Pattern.compile("a NOMATCH.*to"))),
                      "'field=a phrase to match' must partially match regexp /a NOMATCH.*to/0");
   }

   @Test
   public void testPartiallyMatchWhenNullPattern() {
      assertThatThrown("a phrase to match", () -> MatchingRules.partiallyMatches(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'pattern' must not be null");
   }

   @Test
   public void testPartiallyMatchWhenCustomMessage() {

      assertThatError("a phrase to match", "field",
                      MatchingRules.partiallyMatches(value(Pattern.compile("a NOMATCH.*to")),
                                                     "this '%1$s' must match",
                                                     MessageValue.validatingObjectName()),
                      "this 'field' must match");
   }

}