/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class TemporalRulesTest extends BaseInvariantTest {

   private static final Instant INSTANT1 =
         OffsetDateTime.of(2021, 3, 1, 0, 0, 0, 0, ZoneOffset.UTC).toInstant();
   private static final Instant INSTANT2 =
         OffsetDateTime.of(2021, 3, 2, 0, 0, 0, 0, ZoneOffset.UTC).toInstant();

   @Test
   public void testIsEqualTo() {
      assertThatSuccess(offsetDateTime(INSTANT1),
                        TemporalRules.isEqualTo(value(offsetDateTime(INSTANT1))),
                        "'2021-03-01T00:00:00Z' must be equal to '2021-03-01T00:00:00Z'");
      assertThatError(offsetDateTime(INSTANT1),
                      "field",
                      TemporalRules.isEqualTo(value(offsetDateTime(INSTANT2))),
                      "'field=2021-03-01T00:00:00Z' must be equal to '2021-03-02T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT1),
                        TemporalRules.isEqualTo(value(zonedDateTime(INSTANT1))),
                        "'2021-03-01T00:00:00Z' must be equal to '2021-03-01T00:00:00Z'");
      assertThatError(zonedDateTime(INSTANT1),
                      "field",
                      TemporalRules.isEqualTo(value(zonedDateTime(INSTANT2))),
                      "'field=2021-03-01T00:00:00Z' must be equal to '2021-03-02T00:00:00Z'");
      assertThatSuccess(localDateTime(INSTANT1),
                        TemporalRules.isEqualTo(value(localDateTime(INSTANT1))),
                        "'2021-03-01T00:00:00' must be equal to '2021-03-01T00:00:00'");
      assertThatError(localDateTime(INSTANT1),
                      "field",
                      TemporalRules.isEqualTo(value(localDateTime(INSTANT2))),
                      "'field=2021-03-01T00:00:00' must be equal to '2021-03-02T00:00:00'");
      assertThatSuccess(localDate(INSTANT1),
                        TemporalRules.isEqualTo(value(localDate(INSTANT1))),
                        "'2021-03-01' must be equal to '2021-03-01'");
      assertThatError(localDate(INSTANT1),
                      "field",
                      TemporalRules.isEqualTo(value(localDate(INSTANT2))),
                      "'field=2021-03-01' must be equal to '2021-03-02'");
      assertThatSuccess(INSTANT1,
                        TemporalRules.isEqualTo(value(INSTANT1)),
                        "'2021-03-01T00:00:00Z' must be equal to '2021-03-01T00:00:00Z'");
      assertThatError(INSTANT1,
                      "field",
                      TemporalRules.isEqualTo(value(INSTANT2)),
                      "'field=2021-03-01T00:00:00Z' must be equal to '2021-03-02T00:00:00Z'");
   }

   @Test
   public void testIsEqualToWhenNullValue() {
      assertThatThrown(offsetDateTime(INSTANT1), "field", () -> TemporalRules.isEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsBefore() {
      assertThatSuccess(offsetDateTime(INSTANT1),
                        TemporalRules.isBefore(value(offsetDateTime(INSTANT2))),
                        "'2021-03-01T00:00:00Z' must be before '2021-03-02T00:00:00Z'");
      assertThatError(offsetDateTime(INSTANT1),
                      "field",
                      TemporalRules.isBefore(value(offsetDateTime(INSTANT1))),
                      "'field=2021-03-01T00:00:00Z' must be before '2021-03-01T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT1),
                        TemporalRules.isBefore(value(zonedDateTime(INSTANT2))),
                        "'2021-03-01T00:00:00Z' must be before '2021-03-02T00:00:00Z'");
      assertThatError(zonedDateTime(INSTANT1),
                      "field",
                      TemporalRules.isBefore(value(zonedDateTime(INSTANT1))),
                      "'field=2021-03-01T00:00:00Z' must be before '2021-03-01T00:00:00Z'");
      assertThatSuccess(localDateTime(INSTANT1),
                        TemporalRules.isBefore(value(localDateTime(INSTANT2))),
                        "'2021-03-01T00:00:00' must be before '2021-03-02T00:00:00'");
      assertThatError(localDateTime(INSTANT1),
                      "field",
                      TemporalRules.isBefore(value(localDateTime(INSTANT1))),
                      "'field=2021-03-01T00:00:00' must be before '2021-03-01T00:00:00'");
      assertThatSuccess(localDate(INSTANT1),
                        TemporalRules.isBefore(value(localDate(INSTANT2))),
                        "'2021-03-01' must be before '2021-03-02'");
      assertThatError(localDate(INSTANT1),
                      "field",
                      TemporalRules.isBefore(value(localDate(INSTANT1))),
                      "'field=2021-03-01' must be before '2021-03-01'");
      assertThatSuccess(INSTANT1,
                        TemporalRules.isBefore(value(INSTANT2)),
                        "'2021-03-01T00:00:00Z' must be before '2021-03-02T00:00:00Z'");
      assertThatError(INSTANT1,
                      "field",
                      TemporalRules.isBefore(value(INSTANT1)),
                      "'field=2021-03-01T00:00:00Z' must be before '2021-03-01T00:00:00Z'");
   }

   @Test
   public void testIsBeforeWhenNullValue() {
      assertThatThrown(offsetDateTime(INSTANT1), "field", () -> TemporalRules.isBefore(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsNotBefore() {
      assertThatSuccess(offsetDateTime(INSTANT2),
                        TemporalRules.isNotBefore(value(offsetDateTime(INSTANT1))),
                        "'2021-03-02T00:00:00Z' must not be before '2021-03-01T00:00:00Z'");
      assertThatSuccess(offsetDateTime(INSTANT2),
                        "field",
                        TemporalRules.isNotBefore(value(offsetDateTime(INSTANT2))),
                        "'field=2021-03-02T00:00:00Z' must not be before '2021-03-02T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT2),
                        TemporalRules.isNotBefore(value(zonedDateTime(INSTANT1))),
                        "'2021-03-02T00:00:00Z' must not be before '2021-03-01T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT2),
                        "field",
                        TemporalRules.isNotBefore(value(zonedDateTime(INSTANT2))),
                        "'field=2021-03-02T00:00:00Z' must not be before '2021-03-02T00:00:00Z'");
      assertThatSuccess(localDateTime(INSTANT2),
                        TemporalRules.isNotBefore(value(localDateTime(INSTANT1))),
                        "'2021-03-02T00:00:00' must not be before '2021-03-01T00:00:00'");
      assertThatSuccess(localDateTime(INSTANT2),
                        "field",
                        TemporalRules.isNotBefore(value(localDateTime(INSTANT2))),
                        "'field=2021-03-02T00:00:00' must not be before '2021-03-02T00:00:00'");
      assertThatSuccess(localDate(INSTANT2),
                        TemporalRules.isNotBefore(value(localDate(INSTANT1))),
                        "'2021-03-02' must not be before '2021-03-01'");
      assertThatSuccess(localDate(INSTANT2),
                        "field",
                        TemporalRules.isNotBefore(value(localDate(INSTANT2))),
                        "'field=2021-03-02' must not be before '2021-03-02'");
      assertThatSuccess(INSTANT2,
                        TemporalRules.isNotBefore(value(INSTANT1)),
                        "'2021-03-02T00:00:00Z' must not be before '2021-03-01T00:00:00Z'");
      assertThatSuccess(INSTANT2,
                        "field",
                        TemporalRules.isNotBefore(value(INSTANT2)),
                        "'field=2021-03-02T00:00:00Z' must not be before '2021-03-02T00:00:00Z'");
   }

   @Test
   public void testIsNotBeforeWhenNullValue() {
      assertThatThrown(offsetDateTime(INSTANT1), "field", () -> TemporalRules.isNotBefore(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsAfter() {
      assertThatSuccess(offsetDateTime(INSTANT2),
                        TemporalRules.isAfter(value(offsetDateTime(INSTANT1))),
                        "'2021-03-02T00:00:00Z' must be after '2021-03-01T00:00:00Z'");
      assertThatError(offsetDateTime(INSTANT1),
                      "field",
                      TemporalRules.isAfter(value(offsetDateTime(INSTANT1))),
                      "'field=2021-03-01T00:00:00Z' must be after '2021-03-01T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT2),
                        TemporalRules.isAfter(value(zonedDateTime(INSTANT1))),
                        "'2021-03-02T00:00:00Z' must be after '2021-03-01T00:00:00Z'");
      assertThatError(zonedDateTime(INSTANT1),
                      "field",
                      TemporalRules.isAfter(value(zonedDateTime(INSTANT1))),
                      "'field=2021-03-01T00:00:00Z' must be after '2021-03-01T00:00:00Z'");
      assertThatSuccess(localDateTime(INSTANT2),
                        TemporalRules.isAfter(value(localDateTime(INSTANT1))),
                        "'2021-03-02T00:00:00' must be after '2021-03-01T00:00:00'");
      assertThatError(localDateTime(INSTANT1),
                      "field",
                      TemporalRules.isAfter(value(localDateTime(INSTANT1))),
                      "'field=2021-03-01T00:00:00' must be after '2021-03-01T00:00:00'");
      assertThatSuccess(localDate(INSTANT2),
                        TemporalRules.isAfter(value(localDate(INSTANT1))),
                        "'2021-03-02' must be after '2021-03-01'");
      assertThatError(localDate(INSTANT1),
                      "field",
                      TemporalRules.isAfter(value(localDate(INSTANT1))),
                      "'field=2021-03-01' must be after '2021-03-01'");
      assertThatSuccess(INSTANT2,
                        TemporalRules.isAfter(value(INSTANT1)),
                        "'2021-03-02T00:00:00Z' must be after '2021-03-01T00:00:00Z'");
      assertThatError(INSTANT1,
                      "field",
                      TemporalRules.isAfter(value(INSTANT1)),
                      "'field=2021-03-01T00:00:00Z' must be after '2021-03-01T00:00:00Z'");
   }

   @Test
   public void testIsAfterWhenNullValue() {
      assertThatThrown(offsetDateTime(INSTANT1), "field", () -> TemporalRules.isAfter(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsNotAfter() {
      assertThatSuccess(offsetDateTime(INSTANT1),
                        TemporalRules.isNotAfter(value(offsetDateTime(INSTANT2))),
                        "'2021-03-01T00:00:00Z' must not be after '2021-03-02T00:00:00Z'");
      assertThatSuccess(offsetDateTime(INSTANT2),
                        "field",
                        TemporalRules.isNotAfter(value(offsetDateTime(INSTANT2))),
                        "'field=2021-03-02T00:00:00Z' must not be after '2021-03-02T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT1),
                        TemporalRules.isNotAfter(value(zonedDateTime(INSTANT2))),
                        "'2021-03-01T00:00:00Z' must not be after '2021-03-02T00:00:00Z'");
      assertThatSuccess(zonedDateTime(INSTANT2),
                        "field",
                        TemporalRules.isNotAfter(value(zonedDateTime(INSTANT2))),
                        "'field=2021-03-02T00:00:00Z' must not be after '2021-03-02T00:00:00Z'");
      assertThatSuccess(localDateTime(INSTANT1),
                        TemporalRules.isNotAfter(value(localDateTime(INSTANT2))),
                        "'2021-03-01T00:00:00' must not be after '2021-03-02T00:00:00'");
      assertThatSuccess(localDateTime(INSTANT2),
                        "field",
                        TemporalRules.isNotAfter(value(localDateTime(INSTANT2))),
                        "'field=2021-03-02T00:00:00' must not be after '2021-03-02T00:00:00'");
      assertThatSuccess(localDate(INSTANT1),
                        TemporalRules.isNotAfter(value(localDate(INSTANT2))),
                        "'2021-03-01' must not be after '2021-03-02'");
      assertThatSuccess(localDate(INSTANT2),
                        "field",
                        TemporalRules.isNotAfter(value(localDate(INSTANT2))),
                        "'field=2021-03-02' must not be after '2021-03-02'");
      assertThatSuccess(INSTANT1,
                        TemporalRules.isNotAfter(value(INSTANT2)),
                        "'2021-03-01T00:00:00Z' must not be after '2021-03-02T00:00:00Z'");
      assertThatSuccess(INSTANT2,
                        "field",
                        TemporalRules.isNotAfter(value(INSTANT2)),
                        "'field=2021-03-02T00:00:00Z' must not be after '2021-03-02T00:00:00Z'");
   }

   @Test
   public void testIsNotAfterWhenNullValue() {
      assertThatThrown(offsetDateTime(INSTANT1), "field", () -> TemporalRules.isNotAfter(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   private OffsetDateTime offsetDateTime(Instant instant) {
      return OffsetDateTime.ofInstant(instant, ZoneId.of("UTC"));
   }

   private ZonedDateTime zonedDateTime(Instant instant) {
      return ZonedDateTime.ofInstant(instant, ZoneId.of("UTC"));
   }

   private LocalDateTime localDateTime(Instant instant) {
      return LocalDateTime.ofInstant(instant, ZoneId.of("UTC"));
   }

   private LocalDate localDate(Instant instant) {
      return localDateTime(instant).toLocalDate();
   }

}