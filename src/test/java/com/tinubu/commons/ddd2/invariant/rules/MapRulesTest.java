/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectInitialValue;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.isEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.CollectionRules.isNotEmpty;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.keys;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.size;
import static com.tinubu.commons.ddd2.invariant.rules.MapRules.values;
import static java.util.Collections.emptyMap;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class MapRulesTest extends BaseInvariantTest {

   @Test
   public void testIsNotEmpty() {
      assertThatSuccess(new HashMap<Integer, String>() {{put(1, "a");}},
                        MapRules.isNotEmpty(),
                        "'<object>' must not be empty");
      assertThatError(emptyMap(), "field", MapRules.isNotEmpty(), "'field' must not be empty");
      assertThatError(null, "field", MapRules.isNotEmpty(), "'field' must not be null");
   }

   @Test
   public void testHasNoNullElements() {
      assertThatError(null, "field", MapRules.hasNoNullElements(), "'field' must not be null");
      assertThatSuccess(new HashMap<Integer, String>() {{put(1, "a");}},
                        MapRules.hasNoNullElements(),
                        "'<object>' must not have null key or null value");
      assertThatError(new HashMap<Integer, String>() {{put(1, null);}},
                      MapRules.hasNoNullElements(),
                      "'<object>' must not have null key or null value");
      assertThatError(new HashMap<Integer, String>() {{put(null, "a");}},
                      MapRules.hasNoNullElements(),
                      "'<object>' must not have null key or null value");
      assertThatError(new HashMap<Integer, String>() {{put(null, null);}},
                      MapRules.hasNoNullElements(),
                      "'<object>' must not have null key or null value");
   }

   @Test
   public void testSize() {
      HashMap<Integer, String> map = new HashMap<Integer, String>() {{
         put(1, "a");
         put(2, "b");
      }};
      assertThatSuccess(map, size(isEqualTo(value(2))), "'size=2' must be equal to '2'");
      assertThatError(map, "field", size(isEqualTo(value(5))), "'field.size=2' must be equal to '5'");
      assertThatError(null, "field", size(isEqualTo(value(3))), "'field' must not be null");

      assertThatError(map,
                      "field",
                      size(isEqualTo(value(5),
                                     "this '%1$s' value must be = 5 : %2$s (%3$s)",
                                     validatingObjectName(),
                                     validatingObjectValue(),
                                     validatingObjectInitialValue())),
                      "this 'field.size' value must be = 5 : 2 ({1=a, 2=b})");
   }

   @Test
   public void testKeys() {
      HashMap<Integer, String> map = new HashMap<Integer, String>() {{
         put(1, "a");
         put(2, "b");
      }};
      assertThatSuccess(map, keys(isNotEmpty()), "'keys' must not be empty");
      assertThatError(map, "field", keys(isEmpty()), "'field.keys=[1,2]' must be empty");
      assertThatError(null, "field", keys(isEmpty()), "'field' must not be null");

      assertThatError(map,
                      "field",
                      keys(isEmpty("this '%1$s' value must be empty : %2$s (%3$s)",
                                   validatingObjectName(),
                                   validatingObjectValue(),
                                   validatingObjectInitialValue())),
                      "this 'field.keys' value must be empty : [1, 2] ({1=a, 2=b})");
   }

   @Test
   public void testValues() {
      HashMap<Integer, String> map = new HashMap<Integer, String>() {{
         put(1, "a");
         put(2, "b");
      }};
      assertThatSuccess(map, values(isNotEmpty()), "'values' must not be empty");
      assertThatError(map, "field", values(isEmpty()), "'field.values=[a,b]' must be empty");
      assertThatError(null, "field", values(isEmpty()), "'field' must not be null");

      assertThatError(map,
                      "field",
                      values(isEmpty("this '%1$s' value must be empty : %2$s (%3$s)",
                                     validatingObjectName(),
                                     validatingObjectValue(),
                                     validatingObjectInitialValue())),
                      "this 'field.values' value must be empty : [a, b] ({1=a, 2=b})");
   }

}