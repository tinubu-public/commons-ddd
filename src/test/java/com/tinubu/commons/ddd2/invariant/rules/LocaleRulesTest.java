/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import java.util.Locale;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.MessageValue;

public class LocaleRulesTest extends BaseInvariantTest {

   @Test
   public void testHasLanguage() {
      assertThatSuccess(new Locale("fr"), LocaleRules.hasLanguage(), "'fr' locale must have a language set");
      assertThatError(null, "field", LocaleRules.hasLanguage(), "'field' must not be null");
      assertThatError(new Locale(""),
                      "field",
                      LocaleRules.hasLanguage(),
                      "'field=' locale must have a language set");
      assertThatError(new Locale("", "FR"),
                      "field",
                      LocaleRules.hasLanguage(),
                      "'field=_FR' locale must have a language set");
   }

   @Test
   public void testHasLanguageWhenCustomMessage() {
      assertThatError(new Locale("", "FR"), "field", LocaleRules.hasLanguage("Custom message : '%1$s'",
                                                                             MessageValue.validatingObjectValue()),
                      "Custom message : '_FR'");
   }

   @Test
   public void testHasCountry() {
      assertThatSuccess(new Locale("fr", "FR"),
                        LocaleRules.hasCountry(),
                        "'fr_FR' locale must have a country set");
      assertThatSuccess(new Locale("", "FR"),
                        LocaleRules.hasCountry(),
                        "'_FR' locale must have a country set");
      assertThatError(null, "field", LocaleRules.hasCountry(), "'field' must not be null");
      assertThatError(new Locale("fr", ""),
                      "field",
                      LocaleRules.hasCountry(),
                      "'field=fr' locale must have a country set");
   }

   @Test
   public void testHasCountryWhenCustomMessage() {
      assertThatError(new Locale("fr", ""), "field",
                      LocaleRules.hasCountry("Custom message : '%1$s'", MessageValue.validatingObjectValue()),
                      "Custom message : 'fr'");
   }

   @Test
   public void testIsNotEmpty() {
      assertThatSuccess(new Locale("fr", "FR"),
                        LocaleRules.isNotEmpty(),
                        "'fr_FR' locale must have a language or a country set");
      assertThatError(null, "field", LocaleRules.isNotEmpty(), "'field' must not be null");
      assertThatSuccess(new Locale("", "FR"),
                        LocaleRules.isNotEmpty(),
                        "'_FR' locale must have a language or a country set");
      assertThatSuccess(new Locale("fr", ""),
                        LocaleRules.isNotEmpty(),
                        "'fr' locale must have a language or a country set");
      assertThatError(new Locale("", ""),
                      "field",
                      LocaleRules.isNotEmpty(),
                      "'field=' locale must have a language or a country set");
   }

   @Test
   public void testIsNotEmptyWhenCustomMessage() {
      assertThatError(new Locale("", ""), "field",
                      LocaleRules.isNotEmpty("Custom message : '%1$s'", MessageValue.validatingObjectValue()),
                      "Custom message : ''");
   }

}