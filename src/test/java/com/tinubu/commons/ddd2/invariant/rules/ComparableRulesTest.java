/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional ∞rmation
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.MessageValue.validatingObjectName;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.MessageValue;
import com.tinubu.commons.ddd2.invariant.ParameterValue;

public class ComparableRulesTest extends BaseInvariantTest {

   @Test
   public void testIsEqual() {
      assertThatSuccess(4, ComparableRules.isEqualTo(value(4)), "'4' must be equal to '4'");
      assertThatError(4, ComparableRules.isEqualTo(value(5)), "'4' must be equal to '5'");
      assertThatError(null, ComparableRules.isEqualTo(value(5)), "'<object>' must not be null");
      assertThatThrown(4, () -> ComparableRules.isEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsNotEqual() {
      assertThatError(4, ComparableRules.isNotEqualTo(value(4)), "'4' must not be equal to '4'");
      assertThatSuccess(4, ComparableRules.isNotEqualTo(value(5)), "'4' must not be equal to '5'");
      assertThatError(null, ComparableRules.isNotEqualTo(value(5)), "'<object>' must not be null");
      assertThatThrown(4, () -> ComparableRules.isNotEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsGreaterThan() {
      assertThatSuccess(4, ComparableRules.isGreaterThan(value(3)), "'4' must be greater than '3'");
      assertThatError(4, ComparableRules.isGreaterThan(value(4)), "'4' must be greater than '4'");
      assertThatError(4, ComparableRules.isGreaterThan(value(5)), "'4' must be greater than '5'");
      assertThatError(null, ComparableRules.isGreaterThan(value(5)), "'<object>' must not be null");
      assertThatThrown(4, () -> ComparableRules.isGreaterThan(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsGreaterThanOrEqualTo() {
      assertThatSuccess(4,
                        ComparableRules.isGreaterThanOrEqualTo(value(3)),
                        "'4' must be greater than or equal to '3'");
      assertThatSuccess(4,
                        ComparableRules.isGreaterThanOrEqualTo(value(4)),
                        "'4' must be greater than or equal to '4'");
      assertThatError(4,
                      ComparableRules.isGreaterThanOrEqualTo(value(5)),
                      "'4' must be greater than or equal to '5'");
      assertThatError(null, ComparableRules.isGreaterThanOrEqualTo(value(5)), "'<object>' must not be null");
      assertThatThrown(4, () -> ComparableRules.isGreaterThanOrEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsLessThan() {
      assertThatSuccess(4, ComparableRules.isLessThan(value(5)), "'4' must be less than '5'");
      assertThatError(4, ComparableRules.isLessThan(value(4)), "'4' must be less than '4'");
      assertThatError(4, ComparableRules.isLessThan(value(3)), "'4' must be less than '3'");
      assertThatError(null, ComparableRules.isLessThan(value(5)), "'<object>' must not be null");
      assertThatThrown(4, () -> ComparableRules.isLessThan(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsLessThanOrEqualTo() {
      assertThatSuccess(4,
                        ComparableRules.isLessThanOrEqualTo(value(5)),
                        "'4' must be less than or equal to '5'");
      assertThatSuccess(4,
                        ComparableRules.isLessThanOrEqualTo(value(4)),
                        "'4' must be less than or equal to '4'");
      assertThatError(4,
                      ComparableRules.isLessThanOrEqualTo(value(3)),
                      "'4' must be less than or equal to '3'");
      assertThatError(null, ComparableRules.isLessThanOrEqualTo(value(5)), "'<object>' must not be null");
      assertThatThrown(4, () -> ComparableRules.isLessThanOrEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsInRange() {
      assertThatError(-8,
                      "field",
                      ComparableRules.isInRange(value(-4), value(5)),
                      "'field=-8' must be in [-4,5[ range");
      assertThatSuccess(-4, ComparableRules.isInRange(value(-4), value(5)), "'-4' must be in [-4,5[ range");
      assertThatSuccess(4, ComparableRules.isInRange(value(-4), value(5)), "'4' must be in [-4,5[ range");
      assertThatError(5,
                      "field",
                      ComparableRules.isInRange(value(-4), value(5)),
                      "'field=5' must be in [-4,5[ range");
      assertThatError(null,
                      "field",
                      ComparableRules.isInRange(value(-4), value(5)),
                      "'field' must not be null");
      assertThatSuccess(-8, ComparableRules.isInRange(value(null), value(5)), "'-8' must be in [-∞,5[ range");
      assertThatError(8,
                      "field",
                      ComparableRules.isInRange(value(null), value(5)),
                      "'field=8' must be in [-∞,5[ range");
      assertThatSuccess(5, ComparableRules.isInRange(value(-4), value(null)), "'5' must be in [-4,+∞[ range");
      assertThatError(-5,
                      "field",
                      ComparableRules.isInRange(value(-4), value(null)),
                      "'field=-5' must be in [-4,+∞[ range");
      assertThatSuccess(5,
                        ComparableRules.isInRange(ParameterValue.<Integer>value(null), value(null)),
                        "'5' must be in [-∞,+∞[ range");
      assertThatError(8,
                      "field",
                      ComparableRules.isInRange(value(null),
                                                value(5),
                                                "this '%1$s' value must be in [%2$s,%3$s[ range",
                                                validatingObjectName(),
                                                MessageValue.value("-∞"),
                                                MessageValue.value(5)),
                      "this 'field' value must be in [-∞,5[ range");
   }

   @Test
   public void testIsInRangeInclusive() {
      assertThatError(-8,
                      "field",
                      ComparableRules.isInRangeInclusive(value(-4), value(5)),
                      "'field=-8' must be in [-4,5] range");
      assertThatSuccess(-4,
                        ComparableRules.isInRangeInclusive(value(-4), value(5)),
                        "'-4' must be in [-4,5] range");
      assertThatSuccess(5,
                        ComparableRules.isInRangeInclusive(value(-4), value(5)),
                        "'5' must be in [-4,5] range");
      assertThatError(6,
                      "field",
                      ComparableRules.isInRangeInclusive(value(-4), value(5)),
                      "'field=6' must be in [-4,5] range");
      assertThatError(null,
                      "field",
                      ComparableRules.isInRangeInclusive(value(-4), value(5)),
                      "'field' must not be null");
      assertThatSuccess(-8,
                        ComparableRules.isInRangeInclusive(value(null), value(5)),
                        "'-8' must be in [-∞,5] range");
      assertThatError(8,
                      "field",
                      ComparableRules.isInRangeInclusive(value(null), value(5)),
                      "'field=8' must be in [-∞,5] range");
      assertThatSuccess(5,
                        ComparableRules.isInRangeInclusive(value(-4), value(null)),
                        "'5' must be in [-4,+∞] range");
      assertThatError(-5,
                      "field",
                      ComparableRules.isInRangeInclusive(value(-4), value(null)),
                      "'field=-5' must be in [-4,+∞] range");
      assertThatSuccess(5,
                        ComparableRules.isInRangeInclusive(ParameterValue.<Integer>value(null), value(null)),
                        "'5' must be in [-∞,+∞] range");
      assertThatError(8,
                      "field",
                      ComparableRules.isInRangeInclusive(value(null),
                                                         value(5),
                                                         "this '%1$s' value must be in [%2$s,%3$s] range",
                                                         validatingObjectName(),
                                                         MessageValue.value("-∞"),
                                                         MessageValue.value(5)),
                      "this 'field' value must be in [-∞,5] range");
   }

   @Test
   public void testIsInRangeExclusive() {
      assertThatError(-8,
                      "field",
                      ComparableRules.isInRangeExclusive(value(-4), value(5)),
                      "'field=-8' must be in ]-4,5[ range");
      assertThatSuccess(-3,
                        ComparableRules.isInRangeExclusive(value(-4), value(5)),
                        "'-3' must be in ]-4,5[ range");
      assertThatSuccess(4,
                        ComparableRules.isInRangeExclusive(value(-4), value(5)),
                        "'4' must be in ]-4,5[ range");
      assertThatError(5,
                      "field",
                      ComparableRules.isInRangeExclusive(value(-4), value(5)),
                      "'field=5' must be in ]-4,5[ range");
      assertThatError(null,
                      "field",
                      ComparableRules.isInRangeExclusive(value(-4), value(5)),
                      "'field' must not be null");
      assertThatSuccess(-8,
                        ComparableRules.isInRangeExclusive(value(null), value(5)),
                        "'-8' must be in ]-∞,5[ range");
      assertThatError(8,
                      "field",
                      ComparableRules.isInRangeExclusive(value(null), value(5)),
                      "'field=8' must be in ]-∞,5[ range");
      assertThatSuccess(5,
                        ComparableRules.isInRangeExclusive(value(-4), value(null)),
                        "'5' must be in ]-4,+∞[ range");
      assertThatError(-5,
                      "field",
                      ComparableRules.isInRangeExclusive(value(-4), value(null)),
                      "'field=-5' must be in ]-4,+∞[ range");
      assertThatSuccess(5,
                        ComparableRules.isInRangeExclusive(ParameterValue.<Integer>value(null), value(null)),
                        "'5' must be in ]-∞,+∞[ range");
      assertThatError(8,
                      "field",
                      ComparableRules.isInRangeInclusive(value(null),
                                                         value(5),
                                                         "this '%1$s' value must be in ]%2$s,%3$s[ range",
                                                         validatingObjectName(),
                                                         MessageValue.value("-∞"),
                                                         MessageValue.value(5)),
                      "this 'field' value must be in ]-∞,5[ range");
   }

}