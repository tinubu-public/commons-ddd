/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.InvariantRuleTest.MyEnum.VALUE1;
import static com.tinubu.commons.ddd2.invariant.InvariantRuleTest.MyEnum.VALUE2;
import static com.tinubu.commons.ddd2.invariant.InvariantRuleTest.MyEnum.VALUE3;
import static com.tinubu.commons.ddd2.invariant.MessageValue.parameterValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfies;
import static com.tinubu.commons.ddd2.invariant.PredicateInvariantRule.satisfiesValue;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNull;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isTrue;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.EqualsRules.isNotEqualTo;
import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.Objects;
import java.util.StringJoiner;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.formatter.FastStringFormat;
import com.tinubu.commons.ddd2.invariant.formatter.FixedString;

public class InvariantRuleTest extends BaseInvariantTest {

   @Test
   public void testSatisfiesWhenNominal() {
      assertThat(validate("value", "name", satisfies(Objects::nonNull, "error message"))).isEqualTo(
            InvariantResult.success(validatingObject("value", "name"), FixedString.of("error message")));
   }

   @Test
   public void testNotSatisfiesWhenNominal() {
      assertThat(validate("value",
                          "name",
                          PredicateInvariantRule.notSatisfies(Objects::isNull, "error message"))).isEqualTo(
            InvariantResult.success(validatingObject("value", "name"), FixedString.of("error message")));
   }

   @Test
   public void testSatisfiesWhenObjectReferenceArgument() {
      assertThat(satisfiesValue(myEnum -> myEnum != VALUE1,
                                "Bad '%2$s' enum value : %1$s",
                                MessageValue.validatingObjectValue(),
                                MessageValue.validatingObjectName()).evaluate(VALUE1, "name")).isEqualTo(
            InvariantResult.error(validatingObject(VALUE1, "name"),
                                  FixedString.of("Bad 'name' enum value : VALUE1")));

      assertThat(satisfiesValue(myEnum -> myEnum != VALUE1,
                                "Bad '%2$s' enum value : %1$s",
                                MessageValue.validatingObjectValue((MyEnum myEnum) -> myEnum.name() + "!"),
                                MessageValue.validatingObjectName()).evaluate(VALUE1, "name")).isEqualTo(
            InvariantResult.error(validatingObject(VALUE1, "name"),
                                  FixedString.of("Bad 'name' enum value : VALUE1!")));

      assertThat(satisfiesValue(myEnum -> myEnum != VALUE1,
                                "Bad '%2$s' enum value : %1$s",
                                MessageValue.<MyEnum, String>validatingObjectValue(myEnum -> myEnum.name()
                                                                                             + "!"),
                                MessageValue.validatingObjectName()).evaluate(VALUE1, "name")).isEqualTo(
            InvariantResult.error(validatingObject(VALUE1, "name"),
                                  FixedString.of("Bad 'name' enum value : VALUE1!")));

      MessageValue<MyEnum> myEnumValue = MessageValue.validatingObjectValue(myEnum -> myEnum.name() + "!");

      assertThat(satisfiesValue(myEnum -> myEnum != VALUE1,
                                "Bad '%2$s' enum value : %1$s",
                                myEnumValue,
                                MessageValue.validatingObjectName()).evaluate(VALUE1, "name")).isEqualTo(
            InvariantResult.error(validatingObject(VALUE1, "name"),
                                  FixedString.of("Bad 'name' enum value : VALUE1!")));
   }

   @Test
   public void testSatisfiesWhenObjectReferenceValidationResultArgument() {
      InvariantRule<MyClass> rule = isNotNull().andValue(satisfies(validationObject -> {
                                                                      validationObject.addValidationResults("result1", 42);
                                                                      return validationObject.value().value == VALUE1;
                                                                   },
                                                                   "'%1$s' value must be VALUE1, validation results are : %2$s|%3$s",
                                                                   MessageValue.validatingObjectName(),
                                                                   MessageValue.validationResult(0),
                                                                   MessageValue.validationResult(1)));

      assertThat(rule.evaluate(new MyClass(VALUE2), "myClass")).isEqualTo(InvariantResult.error(
            validatingObject(new MyClass(VALUE2), "myClass").validationResults(Arrays.asList("result1", 42)),
            FixedString.of("'myClass' value must be VALUE1, validation results are : result1|42")));
   }

   @Test
   public void testSatisfiesWhenObjectReferenceValidationResultArgumentWithOutOutOfBounds() {
      InvariantRule<MyClass> rule = isNotNull().andValue(satisfies(validationObject -> {
                                                                      validationObject.addValidationResults("result1", 42);
                                                                      return validationObject.value().value == VALUE1;
                                                                   },
                                                                   "'%1$s' value must be VALUE1, validation results are : %2$s|%3$s",
                                                                   MessageValue.validatingObjectName(),
                                                                   MessageValue.validationResult(0),
                                                                   MessageValue.validationResult(1),
                                                                   MessageValue.validationResult(2)));

      assertThatThrownBy(() -> rule.evaluate(new MyClass(VALUE2), "myClass"))
            .isInstanceOf(IndexOutOfBoundsException.class)
            .hasMessage("No validation result for index : 2");
   }

   @Test
   public void testAndRuleWhenNominal() {
      assertThat(isNull().andValue(isNotBlank()).evaluate(null, "name")).isEqualTo(InvariantResult.error(
            validatingObject(null, "name").ruleName("StringRules.isNotBlank"),
            FixedString.of("'name' must not be null")));

      assertThat(isNull().andValue(isNotBlank()).evaluate(" ", "name")).isEqualTo(InvariantResult.error(
            validatingObject(" ", "name").ruleName("BaseRules.isNull"),
            FixedString.of("'name= ' must be null")));

      assertThat(isNotNull().andValue(isNotBlank()).evaluate(" ", "name")).isEqualTo(InvariantResult.error(
            validatingObject(" ", "name").ruleName("StringRules.isNotBlank"),
            FixedString.of("'name' must not be blank")));
   }

   @Test
   public void testOrRuleWhenNominal() {
      assertThat(isNull().orValue(isNotBlank()).evaluate(null, "name")).isEqualTo(InvariantResult.success(
            validatingObject(null, "name").ruleName("BaseRules.isNull"),
            FixedString.of("'name=null' must be null")));

      assertThat(isNull().orValue(isNotBlank()).evaluate(" ", "name")).isEqualTo(InvariantResult.error(
            validatingObject(" ", "name").ruleName("StringRules.isNotBlank"),
            FixedString.of("'name' must not be blank")));

      assertThat(isNotNull().orValue(isNotBlank()).evaluate(" ", "name")).isEqualTo(InvariantResult.success(
            validatingObject(" ", "name").ruleName("BaseRules.isNotNull"),
            FixedString.of("'name' must not be null")));
   }

   @Test
   public void testMapValueWhenNominal() {
      InvariantRule<MyClass> rule = isTrue("myClass value must be VALUE1").map(mc -> mc.value == VALUE1);

      assertThat(rule.evaluate(new MyClass(VALUE2), "name")).isEqualTo(InvariantResult.error(validatingObject(
            new MyClass(VALUE2),
            "name").ruleName("BaseRules.isTrue"), FixedString.of("myClass value must be VALUE1")));
   }

   @Test
   public void testMapValueWhenComposedWithValidatingType() {
      InvariantRule<MyClass> rule =
            isNotNull().andValue(isTrue("myClass value must be VALUE1").map(mc -> mc.value == VALUE1));

      assertThat(rule.evaluate(new MyClass(VALUE2), "name")).isEqualTo(InvariantResult.error(validatingObject(
            new MyClass(VALUE2),
            "name").ruleName("BaseRules.isTrue"), FixedString.of("myClass value must be VALUE1")));
   }

   @Test
   public void testMapValueWhenComposedWithDerivedType() {
      InvariantRule<MyClass> rule = isTrue("myClass value must be VALUE1")
            .<MyClass>map(mc -> mc.value == VALUE1)
            .orValue(isTrue("myClass value must be VALUE2").map(mc -> mc.value == VALUE2));

      assertThat(rule.evaluate(new MyClass(VALUE3), "name")).isEqualTo(InvariantResult.error(validatingObject(
            new MyClass(VALUE3),
            "name").ruleName("BaseRules.isTrue"), FixedString.of("myClass value must be VALUE2")));
   }

   @Test
   public void testMapValueWhenComposedWithDifferentDerivedType() {
      InvariantRule<MyClass> rule = isNotBlank("myClass value must be blank")
            .<MyClass>map(mc -> mc.value.name())
            .andValue(isTrue("myClass value must be VALUE1").map(mc -> mc.value == VALUE1));

      assertThat(rule.evaluate(new MyClass(VALUE2), "name")).isEqualTo(InvariantResult.error(validatingObject(
            new MyClass(VALUE2),
            "name").ruleName("BaseRules.isTrue"), FixedString.of("myClass value must be VALUE1")));
   }

   @Test
   public void testMapValueWhenObjectReferenceArgument() {
      InvariantRule<MyClass> rule = isNotNull().andValue(isTrue(
            "'%1$s' '%2$s' value must be VALUE1, rule result is '%3$s'",
            MessageValue.validatingObjectName(),
            MessageValue.validatingObjectInitialValue(e -> ((MyClass) e).value.name()),
            MessageValue.validatingObjectValue()).map(mc -> mc.value == VALUE1));

      assertThat(rule.evaluate(new MyClass(VALUE2), "myClass")).isEqualTo(InvariantResult.error(
            validatingObject(new MyClass(VALUE2), "myClass").ruleName("BaseRules.isTrue"),
            FixedString.of("'myClass' 'VALUE2' value must be VALUE1, rule result is 'false'")));
   }

   @Test
   public void testIfSatisfiesWhenConditionMatches() {
      InvariantRule<MyClass> rule = isNotNull("myClass must not be null").ifIsSatisfied(() -> true);

      assertThat(rule.evaluate((MyClass) null)).isEqualTo(InvariantResult.error(validatingObject(null).ruleName(
            "BaseRules.isNotNull"), FixedString.of("myClass must not be null")));
   }

   @Test
   public void testIfSatisfiesWhenConditionDoesNotMatch() {
      InvariantRule<MyClass> rule = isNotNull("myClass must not be null").ifIsSatisfied(() -> false);

      assertThatSuccess(null,
                        "name",
                        rule,
                        "Conditional rule not evaluated",
                        validatingObject((MyClass) null, "name"));
   }

   @Test
   public void testRuleContextWhenNominal() {
      ParameterValue<Object> eqParameter = value(new MyClass(VALUE1));
      InvariantRule<MyClass> rule = isEqualTo(eqParameter);

      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rule,
                        "'name=MyClass[value=VALUE1]' must be equal to 'MyClass[value=VALUE1]'",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter));
      assertThatError((MyClass) null,
                      "name",
                      rule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name")
                            .ruleName("EqualsRules.isEqualTo")
                            .ruleParameters(eqParameter));
   }

   @Test
   public void testRuleContextWhenAndRule() {
      ParameterValue<Object> eqParameter1 = value(new MyClass(VALUE1));
      ParameterValue<Object> eqParameter2 = value(new MyClass(VALUE2));

      InvariantRule<MyClass> rule = isNotEqualTo(eqParameter2).andValue(isEqualTo(eqParameter1));

      assertThatError((MyClass) null,
                      "name",
                      rule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name")
                            .ruleName("EqualsRules.isNotEqualTo")
                            .ruleParameters(eqParameter2));
      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rule,
                        "'name=MyClass[value=VALUE1]' must be equal to 'MyClass[value=VALUE1]'",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter1));
      assertThatError(new MyClass(VALUE2),
                      "name",
                      rule,
                      "'name=MyClass[value=VALUE2]' must not be equal to 'MyClass[value=VALUE2]'",
                      validatingObject(new MyClass(VALUE2), "name")
                            .ruleName("EqualsRules.isNotEqualTo")
                            .ruleParameters(eqParameter2));
   }

   @Test
   public void testRuleContextWhenAndRuleWithAnonymousRule() {
      ParameterValue<Object> eqParameter2 = value(new MyClass(VALUE2));

      InvariantRule<MyClass> rightAnonymousRule =
            isNotEqualTo(eqParameter2).andValue(isNotNull().andValue(satisfies(v -> v
                  .value()
                  .equals(new MyClass(VALUE1)), FixedString.of("value must be equal to VALUE1"))));

      assertThatError((MyClass) null,
                      "name",
                      rightAnonymousRule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name")
                            .ruleName("EqualsRules.isNotEqualTo")
                            .ruleParameters(eqParameter2));
      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rightAnonymousRule,
                        "value must be equal to VALUE1",
                        validatingObject(new MyClass(VALUE1), "name"));
      assertThatError(new MyClass(VALUE2),
                      "name",
                      rightAnonymousRule,
                      "'name=MyClass[value=VALUE2]' must not be equal to 'MyClass[value=VALUE2]'",
                      validatingObject(new MyClass(VALUE2), "name")
                            .ruleName("EqualsRules.isNotEqualTo")
                            .ruleParameters(eqParameter2));

      InvariantRule<MyClass> leftAnonymousRule = isNotNull()
            .andValue(satisfies(v -> v.value().equals(new MyClass(VALUE1)),
                                FixedString.of("value must be equal to VALUE1")))
            .andValue(isNotEqualTo(eqParameter2));

      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        leftAnonymousRule,
                        "'name=MyClass[value=VALUE1]' must not be equal to 'MyClass[value=VALUE2]'",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("EqualsRules.isNotEqualTo")
                              .ruleParameters(eqParameter2));
      assertThatError(new MyClass(VALUE2),
                      "name",
                      leftAnonymousRule,
                      "value must be equal to VALUE1",
                      validatingObject(new MyClass(VALUE2), "name"));
   }

   @Test
   public void testRuleContextWhenOrRule() {
      ParameterValue<Object> eqParameter1 = value(new MyClass(VALUE1));
      ParameterValue<Object> eqParameter2 = value(new MyClass(VALUE2));

      InvariantRule<MyClass> rule = isEqualTo(eqParameter2).orValue(isEqualTo(eqParameter1));

      assertThatError((MyClass) null,
                      "name",
                      rule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name")
                            .ruleName("EqualsRules.isEqualTo")
                            .ruleParameters(eqParameter1));
      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rule,
                        "'name=MyClass[value=VALUE1]' must be equal to 'MyClass[value=VALUE1]'",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter1));
      assertThatSuccess(new MyClass(VALUE2),
                        "name",
                        rule,
                        "'name=MyClass[value=VALUE2]' must be equal to 'MyClass[value=VALUE2]'",
                        validatingObject(new MyClass(VALUE2), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter2));
   }

   @Test
   public void testRuleContextWhenOrRuleWithAnonymousRule() {
      ParameterValue<Object> eqParameter2 = value(new MyClass(VALUE2));

      InvariantRule<MyClass> rightAnonymousRule =
            isEqualTo(eqParameter2).orValue(isNotNull().andValue(satisfies(v -> v
                  .value()
                  .equals(new MyClass(VALUE1)), FixedString.of("value must be equal to VALUE1"))));

      assertThatError((MyClass) null,
                      "name",
                      rightAnonymousRule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name").ruleName("BaseRules.isNotNull"));
      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rightAnonymousRule,
                        "value must be equal to VALUE1",
                        validatingObject(new MyClass(VALUE1), "name"));
      assertThatSuccess(new MyClass(VALUE2),
                        "name",
                        rightAnonymousRule,
                        "'name=MyClass[value=VALUE2]' must be equal to 'MyClass[value=VALUE2]'",
                        validatingObject(new MyClass(VALUE2), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter2));

      InvariantRule<MyClass> leftAnonymousRule = isNotNull()
            .andValue(satisfies(v -> v.value().equals(new MyClass(VALUE1)),
                                FixedString.of("value must be equal to VALUE1")))
            .orValue(isEqualTo(eqParameter2));

      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        leftAnonymousRule,
                        "value must be equal to VALUE1",
                        validatingObject(new MyClass(VALUE1), "name"));
      assertThatSuccess(new MyClass(VALUE2),
                        "name",
                        leftAnonymousRule,
                        "'name=MyClass[value=VALUE2]' must be equal to 'MyClass[value=VALUE2]'",
                        validatingObject(new MyClass(VALUE2), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter2));
   }

   @Test
   public void testRuleContextWhenMap() {
      ParameterValue<Object> eqParameter1 = value(VALUE1);
      ParameterValue<Object> eqParameter2 = value(new MyClass(VALUE2));

      InvariantRule<MyClass> rule = isEqualTo(eqParameter2).orValue(isEqualTo(eqParameter1).map(myClass ->
                                                                                                      myClass
                                                                                                      == null
                                                                                                      ? null
                                                                                                      : myClass.value));

      assertThatError((MyClass) null,
                      "name",
                      rule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name")
                            .ruleName("EqualsRules.isEqualTo")
                            .ruleParameters(eqParameter1));
      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rule,
                        "'name=VALUE1' must be equal to 'VALUE1'",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter1));
      assertThatSuccess(new MyClass(VALUE2),
                        "name",
                        rule,
                        "'name=MyClass[value=VALUE2]' must be equal to 'MyClass[value=VALUE2]'",
                        validatingObject(new MyClass(VALUE2), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter2));
   }

   @Test
   public void testRuleContextWhenOverMap() {
      ParameterValue<Object> eqParameter1 = value(VALUE1);

      InvariantRule<MyClass> rule = isNotNull().andValue(satisfiesValue(v -> v == VALUE1,
                                                                        FastStringFormat.of(
                                                                              "value must be equal to ",
                                                                              parameterValue(0)))
                                                               .map((MyClass myClass) -> myClass == null
                                                                                         ? null
                                                                                         : myClass.value)
                                                               .ruleContext("testRule", eqParameter1));

      assertThatError((MyClass) null,
                      "name",
                      rule,
                      "'name' must not be null",
                      validatingObject((MyClass) null, "name").ruleName("BaseRules.isNotNull"));
      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rule,
                        "value must be equal to VALUE1",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("testRule")
                              .ruleParameters(eqParameter1));
   }

   @Test
   public void testRuleContextWhenIfValue() {
      ParameterValue<Object> eqParameter1 = value(new MyClass(VALUE1));
      InvariantRule<MyClass> rule = isEqualTo(eqParameter1).ifValue(isNotNull()).ifIsSatisfied(() -> true);

      assertThatSuccess(new MyClass(VALUE1),
                        "name",
                        rule,
                        "'name=MyClass[value=VALUE1]' must be equal to 'MyClass[value=VALUE1]'",
                        validatingObject(new MyClass(VALUE1), "name")
                              .ruleName("EqualsRules.isEqualTo")
                              .ruleParameters(eqParameter1));
   }

   public enum MyEnum {
      VALUE1, VALUE2, VALUE3
   }

   public static class MyClass {
      public MyEnum value;

      public MyClass(MyEnum value) {
         this.value = value;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         MyClass myClass = (MyClass) o;
         return value == myClass.value;
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", MyClass.class.getSimpleName() + "[", "]")
               .add("value=" + value)
               .toString();
      }

   }

}