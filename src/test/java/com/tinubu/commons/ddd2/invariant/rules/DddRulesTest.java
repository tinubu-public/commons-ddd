/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.BaseRules.isNotNull;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.IntegerId;
import com.tinubu.commons.ddd2.domain.ids.LongId;
import com.tinubu.commons.ddd2.domain.type.AbstractEntity;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.domain.type.VersionedEntity;
import com.tinubu.commons.ddd2.domain.versions.LongVersion;
import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.MessageValue;

public class DddRulesTest extends BaseInvariantTest {

   @Test
   public void testIsSameValueAs() {
      assertThatSuccess(IntegerId.of(0),
                        DddRules.ValueRules.isSameValueAs(value(IntegerId.of(0))),
                        "'IntegerId[value=0,newObject=false]' must be same value as 'IntegerId[value=0,newObject=false]'");

      assertThatError(null, "field", StringRules.isEmpty(), "'field' must not be null");

      assertThatSuccess(IntegerId.of(0),
                        "field",
                        DddRules.ValueRules.isSameValueAs(value(IntegerId.of(0)),
                                                          "this '%1$s' value have same value as : %2$s",
                                                          MessageValue.validatingObjectName(),
                                                          MessageValue.validatingObjectValue()),
                        "this 'field' value have same value as : IntegerId[value=0,newObject=false]");
   }

   @Test
   public void testId() {
      assertThatSuccess(new MyVersionedEntity("a"),
                        DddRules.EntityRules.id(isEqualTo(value(LongId.of(0L)))),
                        "'LongId[value=0,newObject=false]' must be equal to 'LongId[value=0,newObject=false]'");
      assertThatError(new MyVersionedEntity("a"),
                      "field",
                      DddRules.EntityRules.id(isEqualTo(value(LongId.of(2L)))),
                      "'field=LongId[value=0,newObject=false]' must be equal to 'LongId[value=2,newObject=false]'");
      assertThatError(null,
                      "field",
                      DddRules.EntityRules.id(isEqualTo(value(LongId.of(0L)))),
                      "'field' must not be null");
   }

   @Test
   public void testVersion() {
      assertThatSuccess(new MyVersionedEntity("a"),
                        DddRules.EntityRules.version(isEqualTo(value(LongVersion.of(0L)))),
                        "'LongVersion[value=0]' must be equal to 'LongVersion[value=0]'");
      assertThatError(new MyVersionedEntity("a"),
                      "field",
                      DddRules.EntityRules.version(isEqualTo(value(LongVersion.of(2L)))),
                      "'field=LongVersion[value=0]' must be equal to 'LongVersion[value=2]'");
      assertThatError(null,
                      "field",
                      DddRules.EntityRules.version(isEqualTo(value(LongVersion.of(0L)))),
                      "'field' must not be null");
   }

   public static class MyVersionedEntity extends AbstractEntity<LongId>
         implements VersionedEntity<LongId, LongVersion> {

      private final String value;
      private final int constant = 32;

      public MyVersionedEntity(String value) {
         this.value = value;
      }

      @Override
      public LongId id() {
         return LongId.of(0L);
      }

      @Override
      public LongVersion version() {
         return LongVersion.initialVersion();
      }

      @Override
      @SuppressWarnings("Convert2MethodRef")
      public Fields<MyVersionedEntity> defineDomainFields() {
         return Fields
               .<MyVersionedEntity>builder()
               .field("id", v -> v.id(), isNotNull())
               .field("value", v -> v.value)
               .technicalField("constant", v -> v.constant)
               .build();
      }
   }

}