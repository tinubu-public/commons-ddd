/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.parseMimeType;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class MimeTypeRulesTest {

   @Test
   public void testIsTypeAndSubtypeEqualToWhenNominal() {
      assertThat(MimeTypeRules.isTypeAndSubtypeEqualTo(value(parseMimeType("application/pdf")))).accepts(
            parseMimeType("application/pdf"),
            parseMimeType("application/pdf;charset=UTF-8"),
            parseMimeType("application/pdf;charset=ASCII"));
      assertThat(MimeTypeRules.isTypeAndSubtypeEqualTo(value(parseMimeType("application/pdf;charset=UTF-8")))).accepts(
            parseMimeType("application/pdf"),
            parseMimeType("application/pdf;charset=UTF-8"),
            parseMimeType("application/pdf;charset=ASCII"));
      assertThat(MimeTypeRules.isTypeAndSubtypeEqualTo(value(parseMimeType("application/pdf")))).rejects(
            parseMimeType("application/ftl"),
            parseMimeType("text/pdf"),
            parseMimeType("text/html"));
   }

   @Test
   public void testIsWildcardTypeWhenNominal() {
      assertThat(MimeTypeRules.isWildcardType()).accepts(parseMimeType("*/*"),
                                                         parseMimeType("*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.isWildcardType()).rejects(parseMimeType("application/pdf"));
      assertThat(MimeTypeRules.isWildcardType()).rejects(parseMimeType("application/*"));
   }

   @Test
   public void testIsWildcardSubtypeWhenNominal() {
      assertThat(MimeTypeRules.isWildcardSubtype()).accepts(parseMimeType("*/*"),
                                                            parseMimeType("application/*"),
                                                            parseMimeType("application/*;charset=UTF-8"));
      assertThat(MimeTypeRules.isWildcardSubtype()).rejects(parseMimeType("application/pdf"));
   }

   @Test
   public void testMatchesWhenNominal() {
      assertThat(MimeTypeRules.matches(value(parseMimeType("application/pdf")))).accepts(parseMimeType(
                                                                                               "application/pdf"),
                                                                                         parseMimeType(
                                                                                               "application/*"),
                                                                                         parseMimeType("*/*"),
                                                                                         parseMimeType(
                                                                                               "application/pdf;charset=UTF-8"),
                                                                                         parseMimeType(
                                                                                               "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.matches(value(parseMimeType("application/*")))).accepts(parseMimeType(
                                                                                             "application/pdf"),
                                                                                       parseMimeType(
                                                                                             "application/*"),
                                                                                       parseMimeType("*/*"),
                                                                                       parseMimeType(
                                                                                             "application/pdf;charset=UTF-8"),
                                                                                       parseMimeType(
                                                                                             "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.matches(value(parseMimeType("*/*")))).accepts(parseMimeType("application/pdf"),
                                                                             parseMimeType("application/*"),
                                                                             parseMimeType("*/*"),
                                                                             parseMimeType(
                                                                                   "application/pdf;charset=UTF-8"),
                                                                             parseMimeType("*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.matches(value(parseMimeType("application/pdf")))).rejects(parseMimeType(
                                                                                               "application/ftl"),
                                                                                         parseMimeType(
                                                                                               "text/html"),
                                                                                         parseMimeType(
                                                                                               "application/ftl;charset=UTF-8"),
                                                                                         parseMimeType(
                                                                                               "text/html;charset=UTF-8"));
   }

   @Test
   public void testDoesNotMatchWhenNominal() {
      assertThat(MimeTypeRules.doesNotMatch(value(parseMimeType("application/pdf")))).rejects(parseMimeType(
                                                                                                    "application/pdf"),
                                                                                              parseMimeType(
                                                                                                    "application/*"),
                                                                                              parseMimeType(
                                                                                                    "*/*"),
                                                                                              parseMimeType(
                                                                                                    "application/pdf;charset=UTF-8"),
                                                                                              parseMimeType(
                                                                                                    "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.doesNotMatch(value(parseMimeType("application/*")))).rejects(parseMimeType(
                                                                                                  "application/pdf"),
                                                                                            parseMimeType(
                                                                                                  "application/*"),
                                                                                            parseMimeType(
                                                                                                  "*/*"),
                                                                                            parseMimeType(
                                                                                                  "application/pdf;charset=UTF-8"),
                                                                                            parseMimeType(
                                                                                                  "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.doesNotMatch(value(parseMimeType("*/*")))).rejects(parseMimeType(
                                                                                        "application/pdf"),
                                                                                  parseMimeType(
                                                                                        "application/*"),
                                                                                  parseMimeType("*/*"),
                                                                                  parseMimeType(
                                                                                        "application/pdf;charset=UTF-8"),
                                                                                  parseMimeType(
                                                                                        "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.doesNotMatch(value(parseMimeType("application/pdf")))).accepts(parseMimeType(
                                                                                                    "application/ftl"),
                                                                                              parseMimeType(
                                                                                                    "text/html"),
                                                                                              parseMimeType(
                                                                                                    "application/ftl;charset=UTF-8"),
                                                                                              parseMimeType(
                                                                                                    "text/html;charset=UTF-8"));
   }

   @Test
   public void testIncludesWhenNominal() {
      assertThat(MimeTypeRules.includes(value(parseMimeType("application/pdf")))).accepts(parseMimeType(
                                                                                                "application/pdf"),
                                                                                          parseMimeType(
                                                                                                "application/*"),
                                                                                          parseMimeType("*/*"),
                                                                                          parseMimeType(
                                                                                                "application/pdf;charset=UTF-8"),
                                                                                          parseMimeType(
                                                                                                "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.includes(value(parseMimeType("application/pdf")))).rejects(parseMimeType(
                                                                                                "application/ftl"),
                                                                                          parseMimeType(
                                                                                                "text/html"),
                                                                                          parseMimeType(
                                                                                                "application/ftl;charset=UTF-8"),
                                                                                          parseMimeType(
                                                                                                "text/html;charset=UTF-8"));

      assertThat(MimeTypeRules.includes(value(parseMimeType("application/*")))).accepts(parseMimeType(
                                                                                              "application/*"),
                                                                                        parseMimeType(
                                                                                              "application/*;charset=UTF-8"),
                                                                                        parseMimeType(
                                                                                              "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.includes(value(parseMimeType("application/*")))).rejects(parseMimeType("text/*"));

      assertThat(MimeTypeRules.includes(value(parseMimeType("*/*")))).accepts(parseMimeType("*/*"),
                                                                              parseMimeType(
                                                                                    "*/*;charset=UTF-8"));
      assertThat(MimeTypeRules.includes(value(parseMimeType("*/*")))).rejects(parseMimeType("application/pdf"),
                                                                              parseMimeType(
                                                                                    "application/pdf;charset=UTF-8"),
                                                                              parseMimeType("application/*"),
                                                                              parseMimeType(
                                                                                    "application/*;charset=UTF-8"));
   }

}