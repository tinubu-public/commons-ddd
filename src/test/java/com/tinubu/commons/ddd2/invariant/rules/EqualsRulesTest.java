/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;

import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class EqualsRulesTest extends BaseInvariantTest {

   @Test
   public void testIsEqualTo() {
      assertThatSuccess("value", EqualsRules.isEqualTo(value("value")), "'value' must be equal to 'value'");
      assertThatError("value2",
                      "field",
                      EqualsRules.isEqualTo(value("value1")),
                      "'field=value2' must be equal to 'value1'");
   }

   @Test
   public void testIsEqualToWhenNullValue() {
      assertThatThrown("value", () -> EqualsRules.isEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsNotEqualTo() {
      assertThatError("value",
                      "field",
                      EqualsRules.isNotEqualTo(value("value")),
                      "'field=value' must not be equal to 'value'");
      assertThatSuccess("value2",
                        EqualsRules.isNotEqualTo(value("value1")),
                        "'value2' must not be equal to 'value1'");
   }

   @Test
   public void testIsNotEqualToWhenNullValue() {
      assertThatThrown("value", () -> EqualsRules.isNotEqualTo(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
   }

   @Test
   public void testIsIn() {
      assertThatSuccess("value1",
                        EqualsRules.isIn(value(Arrays.asList("value1", "value2"))),
                        "'value1' must be in [value1,value2]");
      assertThatError("value3",
                      "field",
                      EqualsRules.isIn(value(Arrays.asList("value1", "value2"))),
                      "'field=value3' must be in [value1,value2]");
      assertThatError("value1",
                      "field",
                      EqualsRules.isIn(value(Collections.emptyList())),
                      "'field=value1' must be in []");
      assertThatError(null, "field",
                      EqualsRules.isIn(value(Arrays.asList("value1", null, "value2"))),
                      "'field' must not be null");
      assertThatError("value3",
                      "field",
                      EqualsRules.isIn(value(Arrays.asList("value1", null, "value2"))),
                      "'field=value3' must be in [value1,null,value2]");
   }

   @Test
   public void testIsInWhenNullValue() {
      assertThatThrown("value", () -> EqualsRules.isIn(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'collection' must not be null");
   }

   @Test
   public void testIsNotIn() {
      assertThatError("value1",
                      "field",
                      EqualsRules.isNotIn(value(Arrays.asList("value1", "value2"))),
                      "'field=value1' must not be in [value1,value2]");
      assertThatSuccess("value3",
                        EqualsRules.isNotIn(value(Arrays.asList("value1", "value2"))),
                        "'value3' must not be in [value1,value2]");
      assertThatSuccess("value1",
                        EqualsRules.isNotIn(value(Collections.emptyList())),
                        "'value1' must not be in []");
      assertThatError(null,
                      "field",
                      EqualsRules.isNotIn(value(Arrays.asList("value1", null, "value2"))),
                      "'field' must not be null");
      assertThatSuccess("value3",
                        EqualsRules.isNotIn(value(Arrays.asList("value1", null, "value2"))),
                        "'value3' must not be in [value1,null,value2]");
   }

   @Test
   public void testIsNotInWhenNullValue() {
      assertThatThrown("value", () -> EqualsRules.isNotIn(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'collection' must not be null");
   }

}