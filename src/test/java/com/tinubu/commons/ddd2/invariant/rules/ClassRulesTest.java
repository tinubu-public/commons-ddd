/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;

public class ClassRulesTest extends BaseInvariantTest {

   @Test
   public void testIsInstanceOf() {
      assertThatSuccess("value", isInstanceOf(value(String.class)),
                        "'java.lang.String' must be instance of 'java.lang.String' class");
      assertThatError(null, "field", isInstanceOf(value(Integer.class)), "'field' must not be null");
      assertThatSuccess(new Child(),
                        isInstanceOf(value(Parent.class)),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatError("value",
                      "field",
                      isInstanceOf(value(Parent.class)),
                      "'field=java.lang.String' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatThrown("value", () -> isInstanceOf(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'checkClass' must not be null");
   }

   @Test
   public void testIsNotInstanceOf() {
      assertThatSuccess(Integer.class,
                        ClassRules.isNotInstanceOf(value(String.class)),
                        "'java.lang.Class' must not be instance of 'java.lang.String' class");
      assertThatError(null,
                      "field",
                      ClassRules.isNotInstanceOf(value(Integer.class)),
                      "'field' must not be null");
      assertThatSuccess("value",
                        ClassRules.isNotInstanceOf(value(Parent.class)),
                        "'java.lang.String' must not be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatError(new Child(),
                      "field",
                      ClassRules.isNotInstanceOf(value(Parent.class)),
                      "'field=com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' must not be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatThrown("value", () -> ClassRules.isNotInstanceOf(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'checkClass' must not be null");
   }

   @Test
   public void testIsInstanceOfClass() {
      assertThatSuccess(String.class,
                        ClassRules.isClassOf(value(String.class)),
                        "'java.lang.String' class must be instance of 'java.lang.String' class");
      assertThatError(null, "field", ClassRules.isClassOf(value(Integer.class)), "'field' must not be null");
      assertThatSuccess(Child.class,
                        ClassRules.isClassOf(value(Parent.class)),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' class must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatError(String.class,
                      "field",
                      ClassRules.isClassOf(value(Parent.class)),
                      "'field=java.lang.String' class must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatThrown(String.class, () -> ClassRules.isClassOf(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'checkClass' must not be null");
   }

   @Test
   public void testIsNotInstanceOfClass() {
      assertThatSuccess(Integer.class,
                        ClassRules.isNotClassOf(value(String.class)),
                        "'java.lang.Integer' class must not be instance of 'java.lang.String' class");
      assertThatError(null,
                      "field",
                      ClassRules.isNotClassOf(value(Integer.class)),
                      "'field' must not be null");
      assertThatSuccess(String.class,
                        ClassRules.isNotClassOf(value(Parent.class)),
                        "'java.lang.String' class must not be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatError(Child.class,
                      "field",
                      ClassRules.isNotClassOf(value(Parent.class)),
                      "'field=com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' class must not be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatThrown(String.class, () -> ClassRules.isNotClassOf(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'checkClass' must not be null");
   }

   @Test
   public void testOptionalInstanceOf() {
      assertThatSuccess(new Child(),
                        ClassRules.optionalInstanceOf(Child.class, isInstanceOf(value(Child.class))),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' class");
      assertThatSuccess(new Child(),
                        ClassRules.optionalInstanceOf(Parent.class, isInstanceOf(value(Parent.class))),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatSuccess(new Parent(),
                        ClassRules.optionalInstanceOf(Child.class, isInstanceOf(value(Child.class))),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' must not be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' class");

   }

   @Test
   public void testInstanceOf() {
      assertThatSuccess(new Child(),
                        ClassRules.instanceOf(Child.class, isInstanceOf(value(Child.class))),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' class");
      assertThatSuccess(new Child(),
                        ClassRules.instanceOf(Parent.class, isInstanceOf(value(Parent.class))),
                        "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' class");
      assertThatError(new Parent(),
                      ClassRules.instanceOf(Child.class, isInstanceOf(value(Child.class))),
                      "'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Parent' must be instance of 'com.tinubu.commons.ddd2.invariant.rules.ClassRulesTest$Child' class");

   }

   public static class Parent {}

   public static class Child extends Parent {}
}