/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static com.tinubu.commons.ddd2.invariant.ParameterValue.lazyValue;
import static com.tinubu.commons.ddd2.invariant.ParameterValue.value;
import static com.tinubu.commons.ddd2.invariant.rules.ClassRules.isInstanceOf;
import static com.tinubu.commons.ddd2.invariant.rules.ComparableRules.isEqualTo;

import java.util.function.Supplier;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.rules.EqualsRules;

class ParameterValueTest extends BaseInvariantTest {

   @Test
   public void testValueWhenNominal() {
      assertThatSuccess(42, isEqualTo(value(42)), "'42' must be equal to '42'");
      assertThatSuccess(42,
                        isInstanceOf(value(Integer.class)),
                        "'java.lang.Integer' must be instance of 'java.lang.Integer' class");
   }

   @Test
   public void testLazyValueWhenNominal() {
      assertThatSuccess(42, isEqualTo(lazyValue(() -> 42)), "'42' must be equal to '42'");
      assertThatSuccess(42,
                        isInstanceOf(lazyValue(() -> Integer.class)),
                        "'java.lang.Integer' must be instance of 'java.lang.Integer' class");
   }

   @Test
   public void testValueWhenNull() {
      assertThatError(42, EqualsRules.isEqualTo(value((Integer) null)), "'42' must be equal to 'null'");
   }

   @Test
   public void testLazyValueWhenNull() {
      assertThatThrown(42, () -> EqualsRules.isEqualTo(lazyValue((Supplier<Integer>) null)))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'value' must not be null");
      assertThatError(42,
                      EqualsRules.isEqualTo(lazyValue(() -> (Integer) null)),
                      "'42' must be equal to 'null'");
   }

   @Test
   public void testValueWhenValueMapper() {
      assertThatError(42,
                      isEqualTo(ParameterValue.<Integer, Integer>value(null, v -> v == null ? 0 : v)),
                      "'42' must be equal to '0'");
      assertThatError(42,
                      isEqualTo(ParameterValue.<Integer, Integer>lazyValue(() -> null,
                                                                           v -> v == null ? 0 : v)),
                      "'42' must be equal to '0'");

   }

   @Test
   public void testLazyValueWhenValueMapper() {
      assertThatError(42,
                      isEqualTo(ParameterValue.<Integer, Integer>lazyValue(() -> null,
                                                                           v -> v == null ? 0 : v)),
                      "'42' must be equal to '0'");

   }

}