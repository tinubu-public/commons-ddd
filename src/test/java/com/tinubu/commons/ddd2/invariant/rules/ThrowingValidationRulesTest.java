/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant.rules;

import static com.tinubu.commons.ddd2.invariant.Invariant.validate;
import static com.tinubu.commons.ddd2.invariant.InvariantResult.error;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.BaseInvariantTest;
import com.tinubu.commons.ddd2.invariant.formatter.FixedString;
import com.tinubu.commons.lang.validation.Validate;

public class ThrowingValidationRulesTest extends BaseInvariantTest {

   @Test
   public void testSatisfies() {
      assertThat(validate(null,
                          "name",
                          ThrowingDelegationRules.satisfies(v -> Validate.notNull(v.value(),
                                                                                  v
                                                                                        .name()
                                                                                        .orElse("<object>"))))).isEqualTo(
            error(validatingObject(null, "name"), FixedString.of("'name' must not be null")));
   }

   @Test
   public void testSatisfiesWhenNoExceptionMessage() {
      assertThat(validate(null, "name", ThrowingDelegationRules.satisfies(v -> {
         throw new IllegalArgumentException();
      }))).isEqualTo(error(validatingObject(null, "name"), FixedString.of("IllegalArgumentException")));
   }

   @Test
   public void testSatisfiesValue() {
      assertThat(validate(null,
                          "name",
                          ThrowingDelegationRules.satisfiesValue(v -> Validate.notNull(v,
                                                                                       "validate-name")))).isEqualTo(
            error(validatingObject(null, "name"), FixedString.of("'validate-name' must not be null")));
   }

   @Test
   public void testSatisfiesValueWhenNoExceptionMessage() {
      assertThat(validate(null, "name", ThrowingDelegationRules.satisfiesValue(v -> {
         throw new IllegalArgumentException();
      }))).isEqualTo(error(validatingObject(null, "name"), FixedString.of("IllegalArgumentException")));
   }

}