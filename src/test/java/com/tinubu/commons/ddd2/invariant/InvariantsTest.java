/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.invariant;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.util.Collections;
import java.util.Objects;
import java.util.StringJoiner;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.formatter.FixedString;
import com.tinubu.commons.ddd2.invariant.rules.BaseRules;
import com.tinubu.commons.ddd2.invariant.rules.CollectionRules;
import com.tinubu.commons.ddd2.invariant.rules.StringRules;

public class InvariantsTest extends BaseInvariantTest {

   @Test
   public void testInvariantsValidation() {
      InvariantResults results = Invariants
            .of(Invariant.of(() -> false, BaseRules.isTrue()).objectName("field1"),
                Invariant.of(Collections::emptyList, CollectionRules.isNotEmpty()),
                Invariant.of(() -> "", StringRules.isNotBlank()).objectName("field4"))
            .context(new MyContextObject(33))
            .validate();

      assertThat(results.context()).containsExactly(new MyContextObject(33));
      assertThat(results).containsExactly(InvariantResult.error(validatingObject(false, "field1").ruleName(
                                                "BaseRules.isTrue"), FixedString.of("'field1' must be true")),
                                          InvariantResult.error(validatingObject(emptyList()).ruleName(
                                                                      "CollectionRules.isNotEmpty"),
                                                                FixedString.of("'<object>' must not be empty")),
                                          InvariantResult.error(validatingObject("", "field4").ruleName(
                                                                      "StringRules.isNotBlank"),
                                                                FixedString.of("'field4' must not be blank")));
   }

   @Test
   public void testInvariantsValidationWhenGroups() {
      Invariants invariants = Invariants.of(Invariant
                                                  .of(() -> false, BaseRules.isTrue())
                                                  .groups("field1", "group1")
                                                  .objectName("field1"),
                                            Invariant.of(Collections::emptyList,
                                                         CollectionRules.isNotEmpty()),
                                            Invariant
                                                  .of(() -> "", StringRules.isNotBlank())
                                                  .groups("field4")
                                                  .objectName("field4"));

      assertThat(invariants.validate("field1", "group1")).containsExactly(InvariantResult.error(
            validatingObject(false, "field1").ruleName("BaseRules.isTrue"),
            FixedString.of("'field1' must be true")));

      assertThat(invariants.validate()).containsExactly(InvariantResult.error(validatingObject(false,
                                                                                               "field1").ruleName(
                                                              "BaseRules.isTrue"), FixedString.of("'field1' must be true")),
                                                        InvariantResult.error(validatingObject(emptyList()).ruleName(
                                                                                    "CollectionRules.isNotEmpty"),
                                                                              FixedString.of(
                                                                                    "'<object>' must not be empty")),
                                                        InvariantResult.error(validatingObject("",
                                                                                               "field4").ruleName(
                                                                                    "StringRules.isNotBlank"),
                                                                              FixedString.of(
                                                                                    "'field4' must not be blank")));
   }

   @Test
   public void testInvariantsWhenDuplicatedNames() {
      assertThatThrownBy(() -> Invariants
            .of(Invariant.of(() -> false, BaseRules.isTrue()).name("invariant1"),
                Invariant.of(Collections::emptyList, CollectionRules.isNotEmpty()).name("invariant1"))
            .validate())
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Found invariant name duplicates in results : [invariant1]");
   }

   @Test
   public void testInvariantsValidationOrThrowWhenMultipleErrors() {
      Invariants invariants = Invariants.of(Invariant
                                                  .of(() -> false, BaseRules.isTrue())
                                                  .name("invariant1")
                                                  .objectName("field1"),
                                            Invariant.of(Collections::emptyList,
                                                         CollectionRules.isNotEmpty()),
                                            Invariant
                                                  .of(() -> "", StringRules.isNotBlank())
                                                  .objectName("field4"));

      Throwable validationException = catchThrowable(() -> {
         invariants.context(32, new MyContextObject(33)).validate().orThrow();
      });

      assertThat(validationException)
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > Context [32,MyContextObject[a=33]] > "
                        + "{invariant1} 'field1' must be true | "
                        + "'<object>' must not be empty | "
                        + "'field4' must not be blank");
   }

   @Test
   public void testInvariantsValidationOrThrowWhenOneError() {
      Throwable validationException = catchThrowable(() -> Invariants
            .of(Invariant.of(() -> false, BaseRules.isTrue()).objectName("field1"))
            .context(32, new MyContextObject(33))
            .validate()
            .orThrow());

      assertThat(validationException)
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [32,MyContextObject[a=33]] > 'field1' must be true");
   }

   @Test
   public void testInvariantsValidationOrThrowWhenOneErrorAndNoContext() {
      Throwable validationException = catchThrowable(() -> Invariants
            .of(Invariant.of(() -> false, BaseRules.isTrue()).objectName("field1"))
            .validate()
            .orThrow());

      assertThat(validationException)
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage("Invariant validation error > 'field1' must be true");
   }

   public static class MyContextObject {
      private final int a;

      public MyContextObject(int a) {
         this.a = a;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         MyContextObject that = (MyContextObject) o;
         return a == that.a;
      }

      @Override
      public int hashCode() {
         return Objects.hash(a);
      }

      @Override
      public String toString() {
         return new StringJoiner(",", MyContextObject.class.getSimpleName() + "[", "]")
               .add("a=" + a)
               .toString();
      }
   }

}