/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.criterion.specializations;

import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.CONTAIN;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.MATCH;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.NOT_EQUAL;
import static com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator.START_WITH;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.assertj.core.api.AbstractBooleanAssert;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.criterion.Criterion.CriterionOperator;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

// FIXME remaining operator tests
class PathCriterionTest {

   @Test
   public void testPathCriterionWhenInvalidPath() {
      assertThatNoException().isThrownBy(() -> PathCriterion.of(CriterionBuilder.equal(path(""))).operand(0));
      assertThatNoException().isThrownBy(() -> PathCriterion
            .of(CriterionBuilder.equal(path(".")))
            .operand(0));
      assertThatNoException().isThrownBy(() -> PathCriterion
            .of(CriterionBuilder.equal(path("path/..")))
            .operand(0));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathCriterion.of(CriterionBuilder.equal(path("../path"))).operand(0))
            .withMessage(
                  "Invariant validation error > Context [PathCriterion[operator=EQUAL,operands=[../path],flags=Flags[]]] > {operands} 'operands=[../path]' > 'operands[0]=../path' must not have traversal paths");
   }

   @Test
   public void testPathCriterionWhenNormalizedPath() {
      assertThat(PathCriterion.of(CriterionBuilder.equal(path("path/"))).operand(0)).isEqualTo(path("path"));
      assertThat(PathCriterion.of(CriterionBuilder.equal(path("path/subpath/.."))).operand(0)).isEqualTo(path(
            "path"));
   }

   @Test
   public void testEqualWhenNominal() {
      assertNotSatisfied(null, EQUAL, "path/subpath/file");
      assertSatisfied("path/subpath/file", EQUAL, "path/subpath/file");
      assertSatisfied("/path/subpath/file", EQUAL, "/path/subpath/file");
      assertNotSatisfied("/path/subpath/file", EQUAL, "path/subpath/file");
      assertNotSatisfied("path/subpath/file", EQUAL, "/path/subpath/file");
      assertSatisfied("/", EQUAL, "/");
      assertNotSatisfied("", EQUAL, "path");
      assertNotSatisfied("", EQUAL, "/path");
   }

   @Test
   public void testNotEqualWhenNominal() {
      assertNotSatisfied(null, NOT_EQUAL, "path/subpath/file");
      assertNotSatisfied("path/subpath/file", NOT_EQUAL, "path/subpath/file");
      assertNotSatisfied("/path/subpath/file", NOT_EQUAL, "/path/subpath/file");
      assertSatisfied("/path/subpath/file", NOT_EQUAL, "path/subpath/file");
      assertSatisfied("path/subpath/file", NOT_EQUAL, "/path/subpath/file");
      assertNotSatisfied("/", NOT_EQUAL, "/");
      assertSatisfied("", NOT_EQUAL, "path");
      assertSatisfied("", NOT_EQUAL, "/path");
   }

   @Test
   public void testContainWhenNominal() {
      assertSatisfied("path/subpath/file", CONTAIN, "path");
      assertNotSatisfied(null, CONTAIN, "path");
      assertSatisfied("path/subpath/file", CONTAIN, "subpath");
      assertSatisfied("path/subpath/file", CONTAIN, "file");
      assertSatisfied("path/subpath/file", CONTAIN, "path/subpath");
      assertSatisfied("path/subpath/file", CONTAIN, "subpath/file");
      assertSatisfied("path/subpath/file", CONTAIN, "path/subpath/file");

      assertSatisfied("/path/subpath/file", CONTAIN, "/path/subpath/file");
      assertSatisfied("/path/subpath/file", CONTAIN, "path/subpath/file");
      assertNotSatisfied("path/subpath/file", CONTAIN, "/path/subpath/file");

      assertNotSatisfied("path/subpath/file", CONTAIN, "other");

      assertNotSatisfied("path/subpath/file", CONTAIN, "/");
      assertSatisfied("/path/subpath/file", CONTAIN, "/");

      assertNotSatisfied("", CONTAIN, "path/subpath/file");
      assertNotSatisfied("", CONTAIN, "/path/subpath/file");
   }

   @Test
   public void testStartWithWhenNominal() {
      assertSatisfied("path/subpath/file", START_WITH, "path");
      assertNotSatisfied(null, START_WITH, "path");
      assertNotSatisfied("path/subpath/file", START_WITH, "subpath");
      assertNotSatisfied("path/subpath/file", START_WITH, "file");
      assertSatisfied("path/subpath/file", START_WITH, "path/subpath");
      assertNotSatisfied("path/subpath/file", START_WITH, "subpath/file");
      assertSatisfied("path/subpath/file", START_WITH, "path/subpath/file");

      assertSatisfied("/path/subpath/file", START_WITH, "/path/subpath/file");
      assertNotSatisfied("/path/subpath/file", START_WITH, "path/subpath/file");
      assertNotSatisfied("path/subpath/file", START_WITH, "/path/subpath/file");

      assertNotSatisfied("path/subpath/file", START_WITH, "other");

      assertNotSatisfied("path/subpath/file", START_WITH, "/");
      assertSatisfied("/path/subpath/file", START_WITH, "/");

      assertNotSatisfied("", START_WITH, "path/subpath/file");
      assertNotSatisfied("", START_WITH, "/path/subpath/file");
   }

   @Test
   public void testMatchWhenNominal() {
      assertNotSatisfied("path/subpath/file", MATCH, "path");
      assertNotSatisfied(null, MATCH, "path");
      assertNotSatisfied("path/subpath/file", MATCH, "subpath");
      assertNotSatisfied("path/subpath/file", MATCH, "file");
      assertNotSatisfied("path/subpath/file", MATCH, "path/subpath");
      assertNotSatisfied("path/subpath/file", MATCH, "subpath/file");
      assertSatisfied("path/subpath/file", MATCH, "path/subpath/file");

      assertSatisfied("/path/subpath/file", MATCH, "/path/subpath/file");
      assertNotSatisfied("/path/subpath/file", MATCH, "path/subpath/file");
      assertNotSatisfied("path/subpath/file", MATCH, "/path/subpath/file");

      assertNotSatisfied("path/subpath/file", MATCH, "other");

      assertNotSatisfied("path/subpath/file", MATCH, "/");
      assertNotSatisfied("/path/subpath/file", MATCH, "/");

      assertNotSatisfied("", MATCH, "path/subpath/file");
      assertNotSatisfied("", MATCH, "/path/subpath/file");

   }

   @Test
   public void testMatchWhenGlobMatcher() {
      assertSatisfied("path/subpath/file", MATCH, "glob:**/file");
      assertSatisfied("path/subpath/file", MATCH, "glob:**");
      assertNotSatisfied("path/subpath/file", MATCH, "glob:*");
      assertSatisfied("path/subpath/file", MATCH, "glob:*/subpath/*");

      assertSatisfied("/path/subpath/file", MATCH, "glob:/**/file");
      assertSatisfied("/path/subpath/file", MATCH, "glob:/**");
      assertNotSatisfied("/path/subpath/file", MATCH, "glob:/*");
      assertSatisfied("/path/subpath/file", MATCH, "glob:/*/subpath/*");

      assertNotSatisfied("", MATCH, "glob:**/file");
      assertSatisfied("", MATCH, "glob:**");
      assertSatisfied("", MATCH, "glob:*");
      assertNotSatisfied("", MATCH, "glob:*/subpath/*");
      assertNotSatisfied("", MATCH, "glob:/**/file");
      assertNotSatisfied("", MATCH, "glob:/**");
      assertNotSatisfied("", MATCH, "glob:/*");
      assertNotSatisfied("", MATCH, "glob:/*/subpath/*");

      assertNotSatisfied("path/subpath/file", MATCH, "glob:/**/file");
      assertNotSatisfied("path/subpath/file", MATCH, "glob:/**");
      assertNotSatisfied("path/subpath/file", MATCH, "glob:/*");
      assertNotSatisfied("path/subpath/file", MATCH, "glob:/*/subpath/*");
      assertNotSatisfied("path/subpath/file", MATCH, "glob:/file");

      assertNotSatisfied("/path/file", MATCH, "glob:/path/**/file");
   }

   private AbstractBooleanAssert<?> assertSatisfied(String path,
                                                    CriterionOperator operator,
                                                    String... operands) {
      return assertThat(pathCriterion(operator, operands).satisfiedBy(path(path))).isTrue();
   }

   private AbstractBooleanAssert<?> assertNotSatisfied(String path,
                                                       CriterionOperator operator,
                                                       String... operands) {
      return assertThat(pathCriterion(operator, operands).satisfiedBy(path(path))).isFalse();
   }

   private PathCriterion pathCriterion(CriterionOperator operator, String... operands) {
      notNull(operator);
      notNull(operands);

      return PathCriterion.of(new CriterionBuilder<Path>()
                                    .operator(operator)
                                    .operands(list(stream(operands).map(this::path)))
                                    .build());
   }

   private Path path(String path) {
      return nullable(path).map(Paths::get).orElse(null);
   }

}