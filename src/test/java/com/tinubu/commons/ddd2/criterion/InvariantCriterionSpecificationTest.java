package com.tinubu.commons.ddd2.criterion;

import static com.tinubu.commons.ddd2.criterion.Criterion.Flag.IGNORE_CASE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.criterion.Criterion.CriterionBuilder;
import com.tinubu.commons.ddd2.criterion.Criterion.Flags;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

// FIXME many missing operators
class InvariantCriterionSpecificationTest {

   @Test
   public void testCriterionSpecificationWhenCastError() {
      assertThat(CriterionBuilder.equal(Flags.of(IGNORE_CASE), "AZ").satisfiedBy("az")).isTrue();
      assertThat(CriterionBuilder.equal(Flags.of(IGNORE_CASE), 32).satisfiedBy(32)).isTrue();
      assertThat(CriterionBuilder.equal("AZ").satisfiedBy("az")).isFalse();
      assertThat(CriterionBuilder.equal(32).satisfiedBy(32)).isTrue();
      assertThat(CriterionBuilder.startWith(32).satisfiedBy(32)).isTrue();
   }

   @Test
   public void testCriterionSpecificationWhenNullOperands() {
      assertThatThrownBy(() -> CriterionBuilder.<String>equal(null).satisfiedBy("value"))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=EQUAL,operands=[null],flags=Flags[]]] > {operands} 'operands=[null]' > 'operands[0]' must not be null");
      assertThatThrownBy(() -> CriterionBuilder.equal(null).satisfiedBy(null))
            .isInstanceOf(InvariantValidationException.class)
            .hasMessage(
                  "Invariant validation error > Context [Criterion[operator=EQUAL,operands=[null],flags=Flags[]]] > {operands} 'operands=[null]' > 'operands[0]' must not be null");
   }

   @Test
   public void testDefinedSpecification() {
      assertThat(CriterionBuilder.<String>defined().satisfiedBy("value")).isTrue();
      assertThat(CriterionBuilder.defined().satisfiedBy(null)).isFalse();
   }

   @Test
   public void testUndefinedSpecification() {
      assertThat(CriterionBuilder.<String>undefined().satisfiedBy("value")).isFalse();
      assertThat(CriterionBuilder.undefined().satisfiedBy(null)).isTrue();
   }

   @Test
   public void testEqualSpecification() {
      assertThat(CriterionBuilder.equal("value").satisfiedBy("value")).isTrue();
      assertThat(CriterionBuilder.equal("value1").satisfiedBy("value2")).isFalse();
      assertThat(CriterionBuilder.equal("value").satisfiedBy(null)).isFalse();
   }

   @Test
   public void testNotEqualSpecification() {
      assertThat(CriterionBuilder.notEqual("value").satisfiedBy("value")).isFalse();
      assertThat(CriterionBuilder.notEqual("value1").satisfiedBy("value2")).isTrue();
      assertThat(CriterionBuilder.notEqual("value").satisfiedBy(null)).isFalse();
   }

   @Test
   public void testMatchSpecification() {
      assertThat(CriterionBuilder.match("value").satisfiedBy("value")).isTrue();
      assertThat(CriterionBuilder.match("v?l*32").satisfiedBy("v@l@@32")).isTrue();
      assertThat(CriterionBuilder.match("v?l*32").satisfiedBy("w@l@@32")).isFalse();
      assertThat(CriterionBuilder.match("v?l*32").satisfiedBy("v@l@@e")).isFalse();
      assertThat(CriterionBuilder.match("v\\\\?l\\*e").satisfiedBy("v\\@l*e")).isTrue();
      assertThat(CriterionBuilder.match("?t?u\\\\\\*c.*\\?\\\\*").satisfiedBy("@t@u\\*c.@@?\\@@")).isTrue();
      assertThat(CriterionBuilder.match("value").satisfiedBy(null)).isFalse();
   }

   @Test
   public void testNotMatchSpecification() {
      assertThat(CriterionBuilder.notMatch("value").satisfiedBy("value")).isFalse();
      assertThat(CriterionBuilder.notMatch("v?l*32").satisfiedBy("value32")).isFalse();
      assertThat(CriterionBuilder.notMatch("v?l*32").satisfiedBy("walue32")).isTrue();
      assertThat(CriterionBuilder.notMatch("v?l*32").satisfiedBy("value")).isTrue();
      assertThat(CriterionBuilder.notMatch("v\\\\?l\\*e").satisfiedBy("v\\al*e")).isFalse();
      assertThat(CriterionBuilder
                       .notMatch("?t?u\\\\\\*c.*\\?\\\\*")
                       .satisfiedBy("@t@u\\*c.@@?\\@@")).isFalse();
      assertThat(CriterionBuilder.notMatch("value").satisfiedBy(null)).isFalse();
   }

}