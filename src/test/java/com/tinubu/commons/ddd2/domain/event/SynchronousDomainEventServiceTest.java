/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.event;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.assertj.core.util.Lists.list;

import java.time.ZonedDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.datetime.ApplicationClock;

class SynchronousDomainEventServiceTest {

   private static final ZonedDateTime NOW = ZonedDateTime.now();

   @BeforeAll
   public static void beforeAll() {
      ApplicationClock.setFixedClock(NOW);
   }

   @Test
   public void testRegisterEventListenerWhenNominal() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      List<String> eventPayloads = list();
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener1:" + event.payload));

      eventService.publishEvent(new TestEvent(this, "payload"));

      assertThat(eventPayloads).containsExactly("listener1:payload");
   }

   @Test
   public void testRegisterEventListenerWhenBadParameters() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      assertThatNullPointerException()
            .isThrownBy(() -> eventService.registerEventListener(null, event -> {}))
            .withMessage("'eventClass' must not be null");

      assertThatNullPointerException()
            .isThrownBy(() -> eventService.registerEventListener(TestEvent.class, null))
            .withMessage("'eventListener' must not be null");
   }

   @Test
   public void testRegisterEventListenerWhenSuperType() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      List<String> eventPayloads = list();
      eventService.registerEventListener(DomainEvent.class,
                                         event -> eventPayloads.add("listener1:"
                                                                    + ((TestEvent) event).payload));
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener2:" + event.payload));

      eventService.publishEvent(new TestEvent(this, "payload"));

      assertThat(eventPayloads).containsExactly("listener1:payload", "listener2:payload");
   }

   @Test
   public void testRegisterEventListenerWhenSubType() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      List<String> eventPayloads = list();
      eventService.registerEventListener(ChildTestEvent.class,
                                         event -> eventPayloads.add("listener1:" + event.payload));
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener2:" + event.payload));

      eventService.publishEvent(new TestEvent(this, "payload"));

      assertThat(eventPayloads).containsExactly("listener2:payload");
   }

   @Test
   public void testRegisterEventListenerWhenSameType() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      List<String> eventPayloads = list();
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener1:" + event.payload));
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener2:" + event.payload));

      eventService.publishEvent(new TestEvent(this, "payload"));

      assertThat(eventPayloads).containsExactly("listener1:payload", "listener2:payload");
   }

   @Test
   public void testUnregisterEventListeners() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      List<String> eventPayloads = list();
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener1:" + event.payload));
      eventService.registerEventListener(TestEvent.class,
                                         event -> eventPayloads.add("listener2:" + event.payload));

      eventService.unregisterEventListeners();

      eventService.publishEvent(new TestEvent(this, "payload"));

      assertThat(eventPayloads).isEmpty();
   }

   @Test
   public void testPublishEventWhenBadParameters() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      assertThatNullPointerException()
            .isThrownBy(() -> eventService.publishEvent(null))
            .withMessage("'event' must not be null");

   }

   @Test
   public void testPublishEventWhenUnregisteredEvent() {
      SynchronousDomainEventService eventService = new SynchronousDomainEventService();

      eventService.publishEvent(new TestEvent(this, "payload"));
   }

   private static class TestEvent extends AbstractDomainEventValue {

      protected final String payload;

      public TestEvent(Object source, String payload) {
         super(source);
         this.payload = payload;
      }
   }

   private static class ChildTestEvent extends TestEvent {

      public ChildTestEvent(Object source, String payload) {
         super(source, payload);
      }
   }

}