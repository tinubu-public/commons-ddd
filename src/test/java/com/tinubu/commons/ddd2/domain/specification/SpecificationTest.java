package com.tinubu.commons.ddd2.domain.specification;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class SpecificationTest {

   @Test
   public void testSpecification() {
      Specification<MyClass> spec = new MyClassSpecification("value1");

      assertThat(spec.satisfiedBy(new MyClass("value1"))).isTrue();
      assertThat(spec.satisfiedBy(new MyClass("value2"))).isFalse();
   }

   @Test
   public void testSpecificationAsPredicate() {

      MyClassSpecification specification = new MyClassSpecification("string1");

      Specification<MyClass> orSpecification =
            specification.or(myClass -> myClass.getMyString().equals("string2"));
      Specification<MyClass> andSpecification =
            specification.and(myClass -> myClass.getMyString().equals("string2"));
      Specification<MyClass> negateSpecification = specification.negate();

      assertThat(specification.satisfiedBy(new MyClass("string1"))).isTrue();
      assertThat(specification.test(new MyClass("string1"))).isTrue();

      assertThat(orSpecification.satisfiedBy(new MyClass("string2"))).isTrue();
      assertThat(orSpecification.test(new MyClass("string2"))).isTrue();

      assertThat(andSpecification.satisfiedBy(new MyClass("string2"))).isFalse();
      assertThat(andSpecification.test(new MyClass("string2"))).isFalse();

      assertThat(negateSpecification.satisfiedBy(new MyClass("string2"))).isTrue();
      assertThat(negateSpecification.test(new MyClass("string2"))).isTrue();

      assertThat(Specification.not(specification).satisfiedBy(new MyClass("string2"))).isTrue();
      assertThat(Specification.not(specification).test(new MyClass("string2"))).isTrue();
   }

   public static class MyClass {

      private final String myString;

      public MyClass(String myString) {
         this.myString = myString;
      }

      public String getMyString() {
         return myString;
      }
   }

   public static class MyClassSpecification implements Specification<MyClass> {

      private final String value;

      public MyClassSpecification(String value) {
         this.value = value;
      }

      @Override
      public boolean satisfiedBy(MyClass myClass) {
         return value != null && value.equals(myClass.getMyString());
      }

      @Override
      public <V> V accept(CompositeSpecificationVisitor<MyClass, V> visitor) {
         return visitor.visit(this);
      }

   }
}
