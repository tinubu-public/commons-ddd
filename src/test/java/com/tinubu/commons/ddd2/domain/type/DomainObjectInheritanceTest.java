/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.type.DomainObjectInheritanceTest.MyChildValue.MyChildValueBuilder;

/**
 * Modeling a complete domain object inheritance.
 */
public class DomainObjectInheritanceTest {

   @Test
   public void testDomainObjectInheritanceWhenNominal() {
      MyChildValue myChildValue = new MyChildValueBuilder().value("value").childValue("childValue").build();

      assertThat(myChildValue.value()).isEqualTo("value");
      assertThat(myChildValue.childValue()).isEqualTo("childValue");
   }

   /**
    * MyValue parent class.
    */
   public static class MyValue extends AbstractValue {
      private final String value;

      public MyValue(MyValueBuilder<? extends MyValue> builder) {
         this.value = builder.value;
      }

      @Override
      protected Fields<? extends MyValue> defineDomainFields() {
         return Fields.<MyValue>builder().field("value", v -> v.value, isNotBlank()).build();
      }

      public String value() {
         return value;
      }

      public static class MyValueBuilder<T extends MyValue> extends DomainBuilder<T> {
         private String value;

         public MyValueBuilder<T> value(String value) {
            this.value = value;
            return this;
         }

         @Override
         @SuppressWarnings("unchecked")
         public T buildDomainObject() {
            return (T) new MyValue(this);
         }
      }
   }

   /**
    * MyValue inheriting implementation.
    */
   public static class MyChildValue extends MyValue {
      private final String childValue;

      public MyChildValue(MyChildValueBuilder builder) {
         super(builder);
         this.childValue = builder.childValue;
      }

      @Override
      @SuppressWarnings("unchecked")
      protected Fields<? extends MyChildValue> defineDomainFields() {
         return Fields
               .<MyChildValue>builder()
               .superFields((Fields<MyChildValue>) super.defineDomainFields())
               .field("childValue", v -> v.childValue, isNotBlank())
               .build();
      }

      public String childValue() {
         return childValue;
      }

      public static class MyChildValueBuilder extends MyValueBuilder<MyChildValue> {
         private String childValue;

         @Override
         public MyChildValueBuilder value(String value) {
            return (MyChildValueBuilder) super.value(value);
         }

         public MyChildValueBuilder childValue(String childValue) {
            this.childValue = childValue;
            return this;
         }

         @Override
         public MyChildValue buildDomainObject() {
            return new MyChildValue(this);
         }

         @Override
         public MyChildValue build() {
            return (MyChildValue) super.build();
         }
      }
   }

}


