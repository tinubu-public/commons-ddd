/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type.collection;

import static com.tinubu.commons.ddd2.invariant.rules.StringRules.isNotBlank;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

class ImmutableValueListTest {

   @Test
   public void testOfWhenNominal() {
      ImmutableValueList<MyValue> list =
            ImmutableValueList.ofValues(asList(new MyValue("a"), new MyValue("b")));

      assertThat(list).containsExactly(new MyValue("a"), new MyValue("b"));
   }

   @Test
   public void testOfWhenEmptyList() {
      ImmutableValueList<MyValue> list = ImmutableValueList.ofValues(emptyList());

      assertThat(list).isEmpty();
   }

   @Test
   public void testOfWhenVarargs() {
      ImmutableValueList<MyValue> list = ImmutableValueList.ofValues(new MyValue("a"), new MyValue("b"));

      assertThat(list).containsExactly(new MyValue("a"), new MyValue("b"));
   }

   @Test
   public void testOfWhenVarargsNoElements() {
      ImmutableValueList<MyValue> list = ImmutableValueList.ofValues();

      assertThat(list).isEmpty();
   }

   @Test
   public void testOfWhenNullList() {
      ImmutableValueList<MyValue> list = ImmutableValueList.ofValues((List<MyValue>) null);

      assertThat(list).isEmpty();
   }

   @Test
   public void testOfWhenNullElements() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> ImmutableValueList.ofValues(singletonList(null)))
            .withMessage(
                  "Invariant validation error > Context [ImmutableValueList[elements=[null]]] > {elements} 'elements=[null]' > 'elements[0]' must not be null");
   }

   @Test
   public void testOfWhenVarargsNullElements() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> ImmutableValueList.ofValues((MyValue) null))
            .withMessage(
                  "Invariant validation error > Context [ImmutableValueList[elements=[null]]] > {elements} 'elements=[null]' > 'elements[0]' must not be null");
   }

   @Test
   public void testEqualsWhenNominal() {
      ImmutableValueList<MyValue> list1 =
            ImmutableValueList.ofValues(Arrays.asList(new MyValue("a"), new MyValue("b")));
      ImmutableValueList<MyValue> list2 =
            ImmutableValueList.ofValues(Arrays.asList(new MyValue("a"), new MyValue("b")));
      ImmutableValueList<MyValue> list3 =
            ImmutableValueList.ofValues(Arrays.asList(new MyValue("b"), new MyValue("a")));

      assertThat(list1).isEqualTo(list2);
      assertThat(list1).isNotEqualTo(list3);
   }

   @Test
   public void testToStringWhenNominal() {
      ImmutableValueList<MyValue> list =
            ImmutableValueList.ofValues(Arrays.asList(new MyValue("a"), new MyValue("b")));

      assertThat(list).hasToString("ImmutableValueList[elements=[MyValue[value=a],MyValue[value=b]]]");
   }

   public static class MyValue extends AbstractValue {
      private final String value;

      public MyValue(String value) {
         this.value = value;
      }

      @Override
      protected Fields<? extends MyValue> defineDomainFields() {
         return Fields.<MyValue>builder().field("value", v -> v.value, isNotBlank()).build();
      }

      public String value() {
         return value;
      }
   }

}