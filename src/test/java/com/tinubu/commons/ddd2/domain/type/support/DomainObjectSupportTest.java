package com.tinubu.commons.ddd2.domain.type.support;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.LongId;
import com.tinubu.commons.ddd2.domain.type.AbstractEntity;
import com.tinubu.commons.ddd2.domain.type.AbstractValue;
import com.tinubu.commons.ddd2.domain.type.Fields;
import com.tinubu.commons.ddd2.valueformatter.StringValueFormatter;

class DomainObjectSupportTest {

   @Test
   void valueEquals() {
      assertThat(DomainObjectSupport.valueEquals(null, null)).isTrue();
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), new SimpleValue(0))).isTrue();
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), null)).isFalse();
      assertThat(DomainObjectSupport.valueEquals(null, "string")).isFalse();
      assertThat(DomainObjectSupport.valueEquals(null, new SimpleValue(0))).isFalse();
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), new SimpleValue(1))).isFalse();
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), new OtherValue(0))).isFalse();
   }

   @Test
   @SuppressWarnings({ "ConstantConditions", "EqualsBetweenInconvertibleTypes" })
   void valueEqualsJavaSpecification() {
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), new SimpleValue(0)))
            .as("equals is reflexive")
            .isTrue();
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), new SimpleValue(1))
                 == DomainObjectSupport.valueEquals(
            new SimpleValue(1),
            new SimpleValue(0))).as("equals is symmetric").isTrue();
      assertThat(DomainObjectSupport.valueEquals(new SimpleValue(0), "string")
                 == "string".equals(new SimpleValue(0)))
            .as("equals is symmetric")
            .isTrue();
   }

   @Test
   void valueToString() {
      assertThat(DomainObjectSupport.valueToString(new SimpleValue(0))).isEqualTo("SimpleValue[value=0]");
      assertThat(DomainObjectSupport.valueToString(new SimpleValue(null))).isEqualTo(
            "SimpleValue[value=<null>]");
      assertThat(DomainObjectSupport.valueToString(new SimpleValue(123456789))).isEqualTo(
            "SimpleValue[value=123...]");
      assertThat(DomainObjectSupport.valueToString(new SimpleValue(0),
                                                   Fields.<SimpleValue>builder().build())).isEqualTo(
            "SimpleValue[value=0]");
      assertThat(DomainObjectSupport.valueToString(new SimpleValue(0), Fields
                                                 .<SimpleValue>builder()
                                                 .field("zero", __ -> 0)
                                                 .field("null", __ -> null)
                                                 .build())).isEqualTo(
            "SimpleValue[value=0,zero=0,null=<null>]");
   }

   @Test
   void entityEquals() {
      assertThat(DomainObjectSupport.entityEquals(null, null)).isTrue();
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), new SimpleEntity(0))).isTrue();
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), null)).isFalse();
      assertThat(DomainObjectSupport.entityEquals(null, "string")).isFalse();
      assertThat(DomainObjectSupport.entityEquals(null, new SimpleEntity(0))).isFalse();
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), new SimpleEntity(1))).isFalse();
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), new OtherEntity(0))).isFalse();
   }

   @Test
   @SuppressWarnings({ "ConstantConditions", "EqualsBetweenInconvertibleTypes" })
   void entityEqualsJavaSpecification() {
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), new SimpleEntity(0)))
            .as("equals is reflexive")
            .isTrue();
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), new SimpleEntity(1))
                 == DomainObjectSupport.entityEquals(new SimpleEntity(1), new SimpleEntity(0)))
            .as("equals is symmetric")
            .isTrue();
      assertThat(DomainObjectSupport.entityEquals(new SimpleEntity(0), "string")
                 == "string".equals(new SimpleEntity(0))).as("equals is symmetric").isTrue();
   }

   @Test
   void entityToString() {
      assertThat(DomainObjectSupport.entityToString(new SimpleEntity(0))).isEqualTo(
            "SimpleEntity[id=LongId[value=0,newObject=false]]");
      assertThat(DomainObjectSupport.entityToString(new SimpleEntity(123456789))).isEqualTo(
            "SimpleEntity[id=LongId[value=123456789,newObject=false]]");
      assertThat(DomainObjectSupport.entityToString(new SimpleEntity(0),
                                                    Fields.<SimpleEntity>builder().build())).isEqualTo(
            "SimpleEntity[id=LongId[value=0,newObject=false]]");
      assertThat(DomainObjectSupport.entityToString(new SimpleEntity(0), Fields
                                                  .<SimpleEntity>builder()
                                                  .field("zero", __ -> 0)
                                                  .field("null", __ -> null)
                                                  .build())).isEqualTo(
            "SimpleEntity[id=LongId[value=0,newObject=false],zero=0,null=<null>]");
   }

   static class SimpleValue extends AbstractValue {

      private final Integer value;

      public SimpleValue(Integer value) {
         this.value = value;
      }

      @Override
      public Fields<SimpleValue> domainFields() {
         return Fields
               .<SimpleValue>builder()
               .field("value", v -> v.value, new StringValueFormatter(3))
               .build();
      }
   }

   static class OtherValue extends AbstractValue {

      private final Integer value;

      public OtherValue(Integer value) {
         this.value = value;
      }

      @Override
      public Fields<OtherValue> domainFields() {
         return Fields.<OtherValue>builder().field("value", v -> v.value).build();
      }
   }

   static class SimpleEntity extends AbstractEntity<LongId> {

      private final LongId id;

      public SimpleEntity(long id) {
         this.id = LongId.of(id);
      }

      @Override
      public LongId id() {
         return id;
      }
   }

   static class OtherEntity extends AbstractEntity<LongId> {

      private final LongId id;

      public OtherEntity(long id) {
         this.id = LongId.of(id);
      }

      @Override
      public LongId id() {
         return id;
      }
   }

}