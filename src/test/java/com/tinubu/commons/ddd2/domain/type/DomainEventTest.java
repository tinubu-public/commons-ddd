/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.ids.StringId;

class DomainEventTest {

   @Test
   public void testSameEventAsWhenNominal() {
      assertThat(new Event1().sameEventAs(new Event1())).isTrue();
      assertThat(new Event1().sameEventAs(new Event2())).isFalse();
   }

   @Test
   public void testSameEventAsWhenNullEvent() {
      assertThat(new Event1().sameEventAs(null)).isFalse();
   }

   @Test
   public void testSameEventAsWhenValueEvent() {
      assertThat(new ValueEvent1("a").sameEventAs(new ValueEvent1("a"))).isTrue();
      assertThat(new ValueEvent1("a").sameEventAs(new ValueEvent1("b"))).isFalse();
      assertThat(new ValueEvent1("a").sameEventAs(new ValueEvent2("b"))).isFalse();
      assertThat(new ValueEvent1("a").sameEventAs(new EntityEvent1(StringId.of("a")))).isFalse();
   }

   @Test
   public void testSameEventAsWhenEntityEvent() {
      assertThat(new EntityEvent1(StringId.of("a")).sameEventAs(new EntityEvent1(StringId.of("a")))).isTrue();
      assertThat(new EntityEvent1(StringId.of("a")).sameEventAs(new EntityEvent1(StringId.of("b")))).isFalse();
      assertThat(new EntityEvent1(StringId.of("a")).sameEventAs(new EntityEvent2(StringId.of("b")))).isFalse();
      assertThat(new EntityEvent1(StringId.of("a")).sameEventAs(new ValueEvent1("a"))).isFalse();
   }

   public static class Event1 implements DomainEvent {}

   public static class Event2 implements DomainEvent {}

   public static class ValueEvent1 implements Value, DomainEvent {
      final String value;

      public ValueEvent1(String value) {
         this.value = value;
      }

      @Override
      public boolean sameValueAs(Value value) {
         return value instanceof ValueEvent1 && ((ValueEvent1) value).value.equals(this.value);
      }
   }

   public static class ValueEvent2 implements Value, DomainEvent {
      final String value;

      public ValueEvent2(String value) {
         this.value = value;
      }

      @Override
      public boolean sameValueAs(Value value) {
         return value instanceof ValueEvent2 && ((ValueEvent2) value).value.equals(this.value);
      }
   }

   public static class EntityEvent1 implements Entity<StringId>, DomainEvent {
      final StringId id;

      public EntityEvent1(StringId id) {
         this.id = id;
      }

      @Override
      public StringId id() {
         return id;
      }
   }

   public static class EntityEvent2 implements Entity<StringId>, DomainEvent {
      final StringId id;

      public EntityEvent2(StringId id) {
         this.id = id;
      }

      @Override
      public StringId id() {
         return id;
      }
   }

}