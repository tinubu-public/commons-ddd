/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.ids;

import java.math.BigInteger;
import java.net.URI;

class BigIntegerIdTest extends BaseSimpleIdTest<BigIntegerId, BigInteger> {

   @Override
   protected BigIntegerId newInstanceForNewObject(BigInteger value) {
      return BigIntegerId.ofNewObject(value);
   }

   @Override
   protected BigIntegerId newInstanceForNewObject(URI urn) {
      return BigIntegerId.ofNewObject(urn);
   }

   @Override
   protected BigIntegerId newInstanceForNewObject() {
      return BigIntegerId.ofNewObject();
   }

   @Override
   protected BigIntegerId newInstance(BigInteger value) {
      return BigIntegerId.of(value);
   }

   @Override
   protected BigIntegerId newInstance(URI urn) {
      return BigIntegerId.of(urn);
   }

   @Override
   protected BigInteger neutralValue() {
      return BigInteger.ZERO;
   }

   @Override
   protected String urnIdType() {
      return "bigint";
   }
}