package com.tinubu.commons.ddd2.domain.ids;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.invariant.InvariantValidationException;

public class PathIdTest extends BaseSimpleIdTest<PathId, Path> {

   @Override
   protected PathId newInstanceForNewObject(Path value) {
      return PathId.ofNewObject(value, false);
   }

   @Override
   protected PathId newInstanceForNewObject(URI urn) {
      return PathId.ofNewObject(urn, false);
   }

   @Override
   protected PathId newInstanceForNewObject() {
      return PathId.ofNewObject(false);
   }

   @Override
   protected PathId newInstance(Path value) {
      return PathId.of(value, false);
   }

   @Override
   protected PathId newInstance(URI urn) {
      return PathId.of(urn, false);
   }

   @Override
   protected Path neutralValue() {
      return Paths.get("filename.ext");
   }

   @Override
   protected String urnIdType() {
      return "path";
   }

   @Test
   public void testPathIdWhenNormalize() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get("."), false))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=,newObject=false,relativePath=false]] > {value} 'value' must not be empty");
      assertThat(PathId.of(Paths.get("././filename.ext"),
                           false)).isEqualTo(PathId.of(Paths.get("filename.ext"), false));
      assertThat(PathId.of(Paths.get("path/../filename.ext"), false)).isEqualTo(PathId.of(Paths.get(
            "filename.ext"), false));
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get("path/../../filename.ext"), false))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=../filename.ext,newObject=false,relativePath=false]] > {value} 'value=../filename.ext' must not have traversal paths");
   }

   @Test
   public void testPathIdWhenNotRelativePath() {
      assertThat(PathId.of(Paths.get("filename.ext"), false)).isNotNull();
      assertThat(PathId.of(Paths.get("/filename.ext"), false)).isNotNull();
      assertThat(PathId.of(Paths.get("/path/filename.ext"), false)).isNotNull();
   }

   @Test
   public void testPathIdWhenRelativePath() {
      assertThat(PathId.of(Paths.get("filename.ext"), true)).isNotNull();
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get("/filename.ext"), true))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=/filename.ext,newObject=false,relativePath=true]] > {value} 'value=/filename.ext' must not be absolute path");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get("/path/filename.ext"), true))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=/path/filename.ext,newObject=false,relativePath=true]] > {value} 'value=/path/filename.ext' must not be absolute path");
   }

   @Test
   public void testPathIdWhenInvalidPath() {
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get(""), false))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=,newObject=false,relativePath=false]] > {value} 'value' must not be empty");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get(" "), false))
            .withMessage(
                  "Invariant validation error > Context [PathId[value= ,newObject=false,relativePath=false]] > {value} 'value.fileName' must not be blank");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get("/"), false))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=/,newObject=false,relativePath=false]] > {value} 'value=/' must have a filename component");
      assertThatExceptionOfType(InvariantValidationException.class)
            .isThrownBy(() -> PathId.of(Paths.get(".."), false))
            .withMessage(
                  "Invariant validation error > Context [PathId[value=..,newObject=false,relativePath=false]] > {value} 'value=..' must not have traversal paths");
   }

   @Test
   public void testPathIdEquality() {
      assertThat(PathId.of(Paths.get("filename.ext"), false)).isEqualTo(PathId.of(Paths.get("filename.ext"),
                                                                                  false));
      assertThat(PathId.of(Paths.get("filename.ext"), false)).isNotEqualTo(PathId.of(Paths.get(
            "otherfilename.ext"), false));
      assertThat(PathId.of(Paths.get("/filename.ext"),
                           false)).isEqualTo(new PathId(Paths.get("/filename.ext"), false));
      assertThat(PathId.of(Paths.get("/filename.ext"),
                           false)).isNotEqualTo(PathId.of(Paths.get("filename.ext"), false));
   }

}
