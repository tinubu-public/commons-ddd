/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.ids;

import java.net.URI;

class IntegerIdTest extends BaseSimpleIdTest<IntegerId, Integer> {

   @Override
   protected IntegerId newInstanceForNewObject(Integer value) {
      return IntegerId.ofNewObject(value);
   }

   @Override
   protected IntegerId newInstanceForNewObject(URI urn) {
      return IntegerId.ofNewObject(urn);
   }

   @Override
   protected IntegerId newInstanceForNewObject() {
      return IntegerId.ofNewObject();
   }

   @Override
   protected IntegerId newInstance(Integer value) {
      return IntegerId.of(value);
   }

   @Override
   protected IntegerId newInstance(URI urn) {
      return IntegerId.of(urn);
   }

   @Override
   protected Integer neutralValue() {
      return 0;
   }

   @Override
   protected String urnIdType() {
      return "int";
   }

}