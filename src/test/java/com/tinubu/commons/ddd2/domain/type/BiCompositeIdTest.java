/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.ddd2.domain.type;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;

import org.junit.jupiter.api.Test;

class BiCompositeIdTest {

   @Test
   public void testIdStringWhenNominal() {
      assertThat(new TestBiCompositeId("a", 0).stringValue()).isEqualTo("a/0");
   }

   @Test
   public void testIdStringWhenUninitialized() {
      assertThat(new TestBiCompositeId("a", null).stringValue()).isNull();
      assertThat(new TestBiCompositeId(null, 0).stringValue()).isNull();
      assertThat(new TestBiCompositeId(null, null).stringValue()).isNull();
   }

   public static class TestBiCompositeId implements BiCompositeId<String, Integer> {

      private final String value1;
      private final Integer value2;

      public TestBiCompositeId(String value1, Integer value2) {
         this.value1 = value1;
         this.value2 = value2;
      }

      @Override
      public String value1() {
         return value1;
      }

      @Override
      public Integer value2() {
         return value2;
      }

      @Override
      public int compareTo(Id o) {
         throw new UnsupportedOperationException();
      }

      @Override
      public URI urnValue() {
         throw new UnsupportedOperationException();
      }
   }

}